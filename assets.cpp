#include "assets.h"

//---------------
// Local globals
//---------------

bool MUTE_MUSIC_FOR_DEBUG = true;
const int SOUND_QUEUE_SIZE = 50;

std::unordered_map<SoundId, sf::SoundBuffer*> allSoundBuffers;

sf::Sound* unorderedSoundQueue[SOUND_QUEUE_SIZE];

std::unordered_map<std::string, sf::Music*> allMusics;
std::unordered_map<TextureId, sf::Texture*> allTextures;
std::unordered_map<TextureId, sf::Texture*> allTextureMasks;
std::unordered_map<std::string, sf::Image*> allImages;
std::unordered_map<TextureId, int> framesPerRow;
std::unordered_map<std::string, GameMap*> allGameMaps;
std::unordered_map<std::string, SpawnMap*> allSpawnMaps;
std::unordered_map<std::string, DialogLine*> allDialogPacks;    // A dialog pack is the first line of dialog in a tree, which forms a one-way linked list

sf::Font largeFont;
sf::Font font;
sf::Font smallFont;
sf::Font tinyFont;
sf::Music* currentPlayingMusic = 0;
sf::Music* currentPlayingAmbient = 0;

void preloadSound(std::string filename, SoundId soundId)
{
    sf::SoundBuffer* sb = new sf::SoundBuffer();
    sb->loadFromFile(filename);
    allSoundBuffers[soundId] = sb;
}

void preloadMusic(std::string filename, std::string name)
{
    sf::Music* music = new sf::Music();
    music->openFromFile(filename);
    allMusics[name] = music;
}

void preloadTexture(std::string filename, TextureId textureId, int frameWidth, int frameHeight)
{
    if(frameHeight == -1)
    {
        frameHeight = frameWidth;
    }

    sf::Texture* tex = new sf::Texture();
    bool loadSucceeded = tex->loadFromFile(filename);
    allTextures[textureId] = tex;

    if(!loadSucceeded)
    {
        sf::Image debugImage;
        debugImage.create(frameWidth, frameHeight, sf::Color(255,0,255));

        tex->loadFromImage(debugImage);
    }

    sf::Image maskImage = tex->copyToImage();
    int width = maskImage.getSize().x;
    int height = maskImage.getSize().y;

    framesPerRow[textureId] = width / frameWidth;

    for(int x = 0; x < width; x++)
    {
        for(int y = 0; y < height; y++)
        {
            sf::Color p = maskImage.getPixel(x, y);
            int a = 0; if(p.a) a = 255;
            sf::Color pn(255, 255, 255, a);
            maskImage.setPixel(x, y, pn);
        }
    }

    sf::Texture* maskTex = new sf::Texture();
    maskTex->loadFromImage(maskImage);
    allTextureMasks[textureId] = maskTex;
}

void preloadImage(std::string filename, std::string name, int frameWidth)
{
    sf::Image* img = new sf::Image();
    img->loadFromFile(filename);
    allImages[name] = img;
}

void preloadMap(std::string filename, std::string name)
{
    sf::Clock mapTimer;
    PRINT_DEBUG("Timer sanity check (seconds): " << mapTimer.getElapsedTime().asMilliseconds() / 1000.0 << "\n");

    GameMap* gameMap = new GameMap();

    gameMap->filename = filename;
    gameMap->lookupName = name;
    gameMap->hashValue = 0;

    if(filename.find("_battle_map") != std::string::npos) gameMap->mapType = MAP_TYPE_BATTLE_MAP;
    if(filename.find("_explore_map") != std::string::npos) gameMap->mapType = MAP_TYPE_EXPLORE_MAP;

    std::ifstream file(filename);
    std::string line;

    int currentLayer = 0;
    int currentRow = 0;
    GameMapTileLayer* tileLayer = 0;
    Tileset* tileset = 0;
    int tileThatHasCollision = 0;
    bool currentlyInTileBlock = false;

    UnpinnedEntityId unpinnedEntityIds[1000];
    int nUnpinnedEntityIds = 0;

    while(std::getline(file, line)) // Go through every line of the file
    {
        if(line.length() == 0)  // It's a blank line
            continue;

        dirtyHash(&(gameMap->hashValue), line);

        if(line.find("<") != std::string::npos) // It's a metadata line
        {
            if(line.find("<map") != std::string::npos)
            {
                gameMap->nTilesX = atoi(getStringBetween(line, "width=\"", "\" height").c_str());
                gameMap->nTilesY = atoi(getStringBetween(line, "height=\"", "\" tilewidth").c_str());
                gameMap->tileWidth = atoi(getStringBetween(line, "tilewidth=\"", "\" tileheight").c_str());
            }

            if(line.find("<property") != std::string::npos)
            {
                if(line.find("BACKGROUND_X_VELOCITY") != std::string::npos)
                    gameMap->backgroundXVelocity = atof(getStringBetween(line, "value=\"", "\"").c_str());

                if(line.find("FOREGROUND_X_VELOCITY") != std::string::npos)
                    gameMap->foregroundXVelocity = atof(getStringBetween(line, "value=\"", "\"").c_str());

                if(line.find("WORLD_BOUND_LEFT") != std::string::npos)
                    gameMap->worldBoundLeft = atoi(getStringBetween(line, "value=\"", "\"").c_str());

                if(line.find("WORLD_BOUND_RIGHT") != std::string::npos)
                    gameMap->worldBoundRight = atoi(getStringBetween(line, "value=\"", "\"").c_str());

                if(line.find("WORLD_BOUND_TOP") != std::string::npos)
                    gameMap->worldBoundTop = atoi(getStringBetween(line, "value=\"", "\"").c_str());

                if(line.find("WORLD_BOUND_BOTTOM") != std::string::npos)
                    gameMap->worldBoundBottom = atoi(getStringBetween(line, "value=\"", "\"").c_str());
            }

            if(line.find("<layer") != std::string::npos)
            {
                tileLayer = new GameMapTileLayer();
                currentLayer++;
                gameMap->tileLayers[currentLayer] = tileLayer;
                gameMap->nLayers++;

                if(line.find("visible") != std::string::npos)
                {
                    tileLayer->isEmpty = true;
                }

                currentRow = 0;
            }

            if(line.find("<tileset") != std::string::npos)
            {
                tileset = new Tileset();

                tileset->firstID = atoi(getStringBetween(line, "firstgid=\"", "\" name").c_str());

                std::string parsedName = getStringBetween(line, "name=\"", "\" tilewidth");
                tileset->name = parsedName += "_TILESET";

                tileset->nTiles = atoi(getStringBetween(line, "tilecount=\"", "\" columns").c_str());
                tileset->nColumns = atoi(getStringBetween(line, "columns=\"", "\">").c_str());

                gameMap->tilesets[gameMap->nTilesets] = tileset;
                gameMap->nTilesets++;
            }

            if(line.find("<image") != std::string::npos)
            {
                if(allImages.find(tileset->name) != allImages.end())  // check if another map has already loaded tileset
                {
                    tileset->imageData = retrieveImage(tileset->name);
                }
                else
                {
                    std::string filename = getStringBetween(line, "source=\"..", "\" width");
                    filename.insert(0, "assets");

                    preloadImage(filename, tileset->name, gameMap->tileWidth);

                    tileset->imageData = retrieveImage(tileset->name);
                }
            }

            if(line.find("<tile ") != std::string::npos)
            {
                tileThatHasCollision = atoi(getStringBetween(line, "id=\"", "\">").c_str());
                currentlyInTileBlock = true;
            }

            if(line.find("<object ") != std::string::npos)
            {
                if(currentlyInTileBlock)
                {
                    CollisionBox* cb = new CollisionBox();

                    float leftf = 0;
                    float topf = 0;
                    float widthf = 0;
                    float heightf = 0;

                    leftf = atof(getStringBetween(line, "x=\"", "\" y=").c_str());
                    topf = atof(getStringBetween(line, "y=\"", "\" width").c_str());
                    widthf = atof(getStringBetween(line, "width=\"", "\" height").c_str());
                    heightf = atof(getStringBetween(line, "height=\"", "\"/>").c_str());

                    cb->x = roundf(leftf);
                    cb->y = roundf(topf);
                    cb->width = roundf(widthf);
                    cb->height = roundf(heightf);
                    cb->layer = currentLayer;

                    tileset->tileCollisionBoxes[tileThatHasCollision] = cb;
                }
                else
                {
                    float x = atof(getStringBetween(line, "x=\"", "\" y=").c_str());
                    float y = atof(getStringBetween(line, "y=\"", "\" width").c_str());
                    std::string name = getStringBetween(line, "name=\"", "\" x");

                    int inferredX = (int)(x / gameMap->tileWidth);
                    int inferredY = (int)(y / gameMap->tileWidth);

                    unpinnedEntityIds[nUnpinnedEntityIds].inferredX = inferredX;
                    unpinnedEntityIds[nUnpinnedEntityIds].inferredY = inferredY;
                    unpinnedEntityIds[nUnpinnedEntityIds].mapEntityId = name;
                    nUnpinnedEntityIds++;
                }
            }

            if(line.find("</tile>") != std::string::npos)
            {
                currentlyInTileBlock = false;
            }
        }
        else    // It's a tile data line
        {
            int currentColumn = 0;

            std::string charBuffer = "";
            for(unsigned int j = 0; j < line.length(); j++)
            {
                char c = line[j];
                if(c == ',' || j == line.length() - 1)
                {
                    if(j == line.length() - 1)
                    {
                        charBuffer += c;
                    }

                    int tile = atoi(charBuffer.c_str());
                    tileLayer->tiles[currentColumn][currentRow] = tile;

                    charBuffer.erase();
                    currentColumn++;
                }
                else
                {
                    charBuffer += c;
                }
            }

            currentRow++;
        }
    }

    PRINT_DEBUG("File parse complete at (seconds): " << mapTimer.getElapsedTime().asMilliseconds() / 1000.0 << "\n");

    // Precompile map; includes rendering the map texture and the map collision objects and adding placeholder entities

    unsigned int pixelWidth = gameMap->tileWidth * gameMap->nTilesX;
    unsigned int pixelHeight = gameMap->tileWidth * gameMap->nTilesY;

    for(int lay = 1; lay <= gameMap->nLayers; lay++)
    {
        tileLayer = gameMap->tileLayers[lay];

        sf::Image scratchImage;
        scratchImage.create(pixelWidth, pixelHeight, sf::Color(0,0,0,0)); // Every layer texture starts transparent

        if(tileLayer->isEmpty)  // If the layer is empty (hidden), the layer texture should be totally transparent
            goto DONE_RENDERING_LAYER;

        for(int y = 0; y < gameMap->nTilesY; y++)
        {
            for(int x = 0; x < gameMap->nTilesX; x++)
            {
                int tile = tileLayer->tiles[x][y];
                Tileset* thisTilesTileset = 0;

                for(int i = 0; i < gameMap->nTilesets; i++)   // Determine the tileset in use
                {
                    int fid = gameMap->tilesets[i]->firstID;
                    if(tile >= fid && tile < fid + gameMap->tilesets[i]->nTiles)
                    {
                        thisTilesTileset = gameMap->tilesets[i];
                    }
                }

                if(thisTilesTileset != 0)
                {
                    int rtn = tile - thisTilesTileset->firstID;
                    int rtx = rtn % thisTilesTileset->nColumns;
                    int rty = rtn / thisTilesTileset->nColumns;
                    int tw = gameMap->tileWidth;

                    if(!isTileEntity(tile)) // don't draw if the tile is a placholder for a real entity
                    {
                        scratchImage.copy(*thisTilesTileset->imageData, x * tw, y * tw, sf::IntRect(rtx * tw, rty * tw, tw, tw)); // draw tile if it's not an entity
                    }
                    else // Store entity placeholder if it's a real entity
                    {
                        EntityPlaceholder* e = new EntityPlaceholder();
                        e->tileId = tile;
                        e->x = x * tw;
                        e->y = y * tw;
                        e->layer = lay;

                        gameMap->entityPlaceholders[gameMap->nEntityPlaceholders++] = e;
                    }


                    if(thisTilesTileset->tileCollisionBoxes[rtn] != 0)
                    {
                        CollisionBox* cb = new CollisionBox();
                        CollisionBox* tcb = thisTilesTileset->tileCollisionBoxes[rtn];

                        cb->x = x * tw + tcb->x;
                        cb->y = y * tw + tcb->y;
                        cb->width = tcb->width;
                        cb->height = tcb->height;
                        cb->layer = lay;

                        gameMap->staticCollisionBoxes[gameMap->nStaticCollisionBoxes] = cb;
                        gameMap->nStaticCollisionBoxes++;
                    }
                }
            }
        }

        // Attach the previously parsed entity IDs to the now existing EntityPlaceholders
        for(int i = 0; i < nUnpinnedEntityIds; i++)
        {
            int inferredX = unpinnedEntityIds[i].inferredX;
            int inferredY = unpinnedEntityIds[i].inferredY;
            std::string mapEntityId = unpinnedEntityIds[i].mapEntityId;

            for(int i = 0; i < gameMap->nEntityPlaceholders; i++)
            {
                EntityPlaceholder* e = gameMap->entityPlaceholders[i];

                if(e->x / gameMap->tileWidth == inferredX && e->y / gameMap->tileWidth == inferredY)
                {
                    e->mapEntityId = mapEntityId;
                    break;
                }
            }

        }

        combineCollisionBoxesOnMapAlongRows(gameMap);
        combineCollisionBoxesOnMapAlongColumns(gameMap);

        DONE_RENDERING_LAYER:

        sf::Texture* texture = new sf::Texture();
        texture->loadFromImage(scratchImage);

        std::string textureName = name;
        textureName += "_";
        textureName += std::to_string(lay);
        gameMap->layerTextures[lay] = texture;
    }

    PRINT_DEBUG("All layers rendered complete at (seconds): " << mapTimer.getElapsedTime().asMilliseconds() / 1000.0 << "\n");

    PRINT_DEBUG("Collision box count: " << gameMap->nStaticCollisionBoxes << "\n");

    PRINT_DEBUG("Calculating pathfinding grid nodes.\n");

    for(int n = 1; n <= 2; n++)
    {
        for(int y = 0; y < gameMap->nTilesY; y += n)
        {
            for(int x = 0; x < gameMap->nTilesX; x += n)
            {
                int topLeftX = x * gameMap->tileWidth;
                int topLeftY = y * gameMap->tileWidth;

                if(topLeftX < gameMap->worldBoundLeft || topLeftX > gameMap->worldBoundRight|| topLeftY < gameMap->worldBoundTop || topLeftY > gameMap->worldBoundBottom)
                    continue;

                GridNode* newGridNode = new GridNode();
                newGridNode->x = x;
                newGridNode->y = y;

                for(int i = 0; i < gameMap->nStaticCollisionBoxes; i++)
                {
                    CollisionBox* cb = gameMap->staticCollisionBoxes[i];
                    if(isCollidingOnLine(topLeftX, topLeftX + gameMap->tileWidth * n - 1, cb->x, cb->x + cb->width - 1) && isCollidingOnLine(topLeftY, topLeftY + gameMap->tileWidth * n - 1, cb->y, cb->y + cb->height - 1))
                    {
                        delete newGridNode;
                        goto skip_tile;
                    }
                }

                gameMap->pathfindingGridNodes[n - 1][gameMap->nPathfindingGridNodes[n - 1]] = newGridNode;
                gameMap->nPathfindingGridNodes[n - 1]++;

                skip_tile:
                    ;
            }
        }
    }

    PRINT_DEBUG("Calculating pathfinding grid edges.\n");

    for(int n = 1; n <= 2; n++)
    {
        for(int z = 0; z < gameMap->nPathfindingGridNodes[n - 1]; z++)
        {
            GridNode* thisNode = gameMap->pathfindingGridNodes[n - 1][z];

            for(int k = 0; k < gameMap->nPathfindingGridNodes[n - 1]; k++)
            {
                GridNode* possibleNeighbour = gameMap->pathfindingGridNodes[n - 1][k];

                if(possibleNeighbour->x != thisNode->x && possibleNeighbour->y != thisNode->y) continue;

                if(possibleNeighbour->x - thisNode->x == n) thisNode->adjacentNodes[4] = possibleNeighbour;
                if(possibleNeighbour->x - thisNode->x == -n) thisNode->adjacentNodes[2] = possibleNeighbour;
                if(possibleNeighbour->y - thisNode->y == n) thisNode->adjacentNodes[3] = possibleNeighbour;
                if(possibleNeighbour->y - thisNode->y == -n) thisNode->adjacentNodes[1] = possibleNeighbour;
            }
        }
    }

    PRINT_DEBUG("Pathfinding grid calculations complete at (seconds): " << mapTimer.getElapsedTime().asMilliseconds() / 1000.0 << "\n");

    if(gameMap->nTilesX == 0 || gameMap->nTilesY == 0 || gameMap->tileLayers == 0)
    {
        PRINT_DEBUG("Failed to load the map.");
        return;
    }
    else
    {
        PRINT_DEBUG("Successfully loaded map " << gameMap->lookupName << " with dimensions: " << gameMap->nTilesX << ", " << gameMap->nTilesY << " and " << gameMap->nLayers << " layers and " << gameMap->nTilesets << " tilesets");
        PRINT_DEBUG("World bounds:" << gameMap->worldBoundTop << "," << gameMap->worldBoundBottom << "," << gameMap->worldBoundLeft << "," << gameMap->worldBoundRight);
        allGameMaps[name] = gameMap;
    }

    PRINT_DEBUG("Map time (seconds): " << mapTimer.getElapsedTime().asMilliseconds() / 1000.0 << "\n");
}

void preloadSpawnMap(std::string filename, std::string name)
{
    sf::Clock mapTimer;
    mapTimer.restart();

    SpawnMap* spawnMap = new SpawnMap();

    spawnMap->filename = filename;
    spawnMap->lookupName = name;

    std::ifstream file(filename);
    std::string line;

    int currentRow = 0;
    bool currentlyInSpawnLayer = 0;

    while(std::getline(file, line)) // Go through every line of the file
    {
        if(line.length() == 0)  // It's a blank line
            continue;

        if(!currentlyInSpawnLayer)
        {
            if(line.find("Spawns") != std::string::npos) currentlyInSpawnLayer = true;
            else continue;
        }

        // We are now in spawn layer

        if(line.find("<") != std::string::npos) // metadata lines, to be ignored
            continue;

        // We are now at the data lines
        {
            int currentColumn = 0;

            std::string charBuffer = "";
            for(unsigned int j = 0; j < line.length(); j++)
            {
                char c = line[j];
                if(c == ',' || j == line.length() - 1)
                {
                    if(j == line.length() - 1)
                    {
                        charBuffer += c;
                    }

                    int tile = atoi(charBuffer.c_str());

                    EntitySubType subType = SUBTYPE_NONE;
                    if(tile == SHAMBLER_TILE) subType = SUBTYPE_SHAMBLER;
                    if(tile == SPRINTER_TILE) subType = SUBTYPE_SPRINTER;
                    if(tile == BRUTE_TILE) subType = SUBTYPE_BRUTE;
                    if(tile == WIZARD_TILE) subType = SUBTYPE_WIZARD;
                    if(tile == GHOST_TILE) subType = SUBTYPE_GHOST;
                    if(tile == SKELETON_MAGE_TILE) subType = SUBTYPE_SKELETON_MAGE;

                    // Spawn enemy
                    if(subType != SUBTYPE_NONE)
                    {
                        EntitySpawn* spawn = &(spawnMap->entitySpawns[currentColumn][currentRow - 10]);
                        spawn->timeIndex = currentColumn;
                        spawn->y = currentRow;
                        spawn->entitySubType = subType;

                        spawnMap->totalSpawns++;
                    }

                    // Set start position
                    if(tile == START_TILE)
                    {
                        spawnMap->startSkip = currentColumn;
                    }

                    // Set army stage positions
                    if(tile == SCOUTS_TRIGGER_TILE)
                    {
                        spawnMap->scoutsAt = currentColumn;
                    }
                    if(tile == VANGUARD_TRIGGER_TILE)
                    {
                        spawnMap->vanguardAt = currentColumn;
                    }
                    if(tile == HORDE_TRIGGER_TILE)
                    {
                        spawnMap->hordeAt = currentColumn;
                    }
                    if(tile == ELITES_TRIGGER_TILE)
                    {
                        spawnMap->elitesAt = currentColumn;
                    }
                    if(tile == BOSS_TRIGGER_TILE)
                    {
                        spawnMap->bossAt = currentColumn;
                    }

                    charBuffer.erase();
                    currentColumn++;
                }
                else
                {
                    charBuffer += c;
                }
            }

            currentRow++;
        }
    }

    if(spawnMap->totalSpawns == 0)
    {
        PRINT_DEBUG("Warning: Spawn map has no spawns. Not loaded.");
        return;
    }
    else
    {
        PRINT_DEBUG("Successfully loaded spawn map " << spawnMap->lookupName << " with total spawns: " << spawnMap->totalSpawns << ", start skip to: " << spawnMap->startSkip << ", boss at: " << spawnMap->bossAt << ", vanguard at: " << spawnMap->vanguardAt);
        allSpawnMaps[name] = spawnMap;
    }

    PRINT_DEBUG("Spawn map parse time (seconds): " << mapTimer.getElapsedTime().asMilliseconds() / 1000.0 << "\n");
}

void combineCollisionBoxesOnMapAlongRows(GameMap* gameMap)
{
    const int COLLISION_BOX_TOLERANCE = 6;

    CollisionBox* collisionBoxesBuffer[1000];
    int newCollisionBoxCount = 0;

    // Combine along rows
    for(int i = 0; i < gameMap->nStaticCollisionBoxes; i++)
    {
        CollisionBox* thisCollisionBox = gameMap->staticCollisionBoxes[i];

        if(thisCollisionBox == 0) continue; // Null pointer means it's already been removed/considered

        int thisCollisionBoxTop = topEdge(thisCollisionBox);
        int thisCollisionBoxBottom = bottomEdge(thisCollisionBox);
        int thisCollisionBoxLeft = leftEdge(thisCollisionBox);
        int thisCollisionBoxRight = rightEdge(thisCollisionBox);

        // Remove old "this" collision box from array (but don't delete it)
        gameMap->staticCollisionBoxes[i] = 0;

        for(int j = 0; j < gameMap->nStaticCollisionBoxes; j++)
        {
            CollisionBox* thatCollisionBox = gameMap->staticCollisionBoxes[j];

            if(thatCollisionBox == 0) continue; // Null pointer means it's already been removed/considered
            if(thatCollisionBox->layer != thisCollisionBox->layer) continue; // Don't try to combine collision boxes on different layers

            int thatCollisionBoxTop = topEdge(thatCollisionBox);
            int thatCollisionBoxBottom = bottomEdge(thatCollisionBox);
            int thatCollisionBoxLeft = leftEdge(thatCollisionBox);
            int thatCollisionBoxRight = rightEdge(thatCollisionBox);

            if(thisCollisionBoxTop != thatCollisionBoxTop || thisCollisionBoxBottom != thatCollisionBoxBottom) continue; // They don't line up across row

            // Try to join row
            int collisionCheck = isCollidingOnLine(thisCollisionBoxLeft - COLLISION_BOX_TOLERANCE, thisCollisionBoxRight + COLLISION_BOX_TOLERANCE, thatCollisionBoxLeft, thatCollisionBoxRight);

            if(!collisionCheck) continue; // They don't overlap

            if(collisionCheck > 0)
            {
                thisCollisionBox->width = (thatCollisionBoxRight - thisCollisionBoxLeft) + 1;
            }

            if(collisionCheck < 0)
            {
                thisCollisionBox->width = thisCollisionBoxRight - thatCollisionBoxLeft + 1;
                thisCollisionBox->x = thatCollisionBoxLeft;
            }

            delete gameMap->staticCollisionBoxes[j];
            gameMap->staticCollisionBoxes[j] = 0;
        }

        collisionBoxesBuffer[newCollisionBoxCount] = thisCollisionBox; // Put "this" collision box back down, whether unmodified or modified
        newCollisionBoxCount++;
    }

    gameMap->nStaticCollisionBoxes = newCollisionBoxCount;
    for(int i = 0; i < newCollisionBoxCount; i++)
    {
        gameMap->staticCollisionBoxes[i] = collisionBoxesBuffer[i];
    }
}

void combineCollisionBoxesOnMapAlongColumns(GameMap* gameMap)
{
    const int COLLISION_BOX_TOLERANCE = 6;

    CollisionBox* collisionBoxesBuffer[1000];
    int newCollisionBoxCount = 0;

    // Combine along columns
    for(int i = 0; i < gameMap->nStaticCollisionBoxes; i++)
    {
        CollisionBox* thisCollisionBox = gameMap->staticCollisionBoxes[i];

        if(thisCollisionBox == 0) continue; // Null pointer means it's already been removed/considered

        int thisCollisionBoxTop = topEdge(thisCollisionBox);
        int thisCollisionBoxBottom = bottomEdge(thisCollisionBox);
        int thisCollisionBoxLeft = leftEdge(thisCollisionBox);
        int thisCollisionBoxRight = rightEdge(thisCollisionBox);

        // Remove old "this" collision box from array (but don't delete it)
        gameMap->staticCollisionBoxes[i] = 0;

        for(int j = 0; j < gameMap->nStaticCollisionBoxes; j++)
        {
            CollisionBox* thatCollisionBox = gameMap->staticCollisionBoxes[j];

            if(thatCollisionBox == 0) continue; // Null pointer means it's already been removed/considered
            if(thatCollisionBox->layer != thisCollisionBox->layer) continue; // Don't try to combine collision boxes on different layers

            int thatCollisionBoxTop = topEdge(thatCollisionBox);
            int thatCollisionBoxBottom = bottomEdge(thatCollisionBox);
            int thatCollisionBoxLeft = leftEdge(thatCollisionBox);
            int thatCollisionBoxRight = rightEdge(thatCollisionBox);

            if(thisCollisionBoxLeft != thatCollisionBoxLeft || thisCollisionBoxRight != thatCollisionBoxRight) continue; // They don't line up across column

            // Try to join row
            int collisionCheck = isCollidingOnLine(thisCollisionBoxTop - COLLISION_BOX_TOLERANCE, thisCollisionBoxBottom + COLLISION_BOX_TOLERANCE, thatCollisionBoxTop, thatCollisionBoxBottom);

            if(!collisionCheck) continue; // They don't overlap

            if(collisionCheck > 0)
            {
                thisCollisionBox->height = (thatCollisionBoxBottom - thisCollisionBoxTop) + 1;
            }

            if(collisionCheck < 0)
            {
                thisCollisionBox->height = thisCollisionBoxBottom - thatCollisionBoxTop + 1;
                thisCollisionBox->y = thatCollisionBoxTop;
            }

            delete gameMap->staticCollisionBoxes[j];
            gameMap->staticCollisionBoxes[j] = 0;
        }

        collisionBoxesBuffer[newCollisionBoxCount] = thisCollisionBox; // Put "this" collision box back down, whether unmodified or modified
        newCollisionBoxCount++;
    }

    gameMap->nStaticCollisionBoxes = newCollisionBoxCount;
    for(int i = 0; i < newCollisionBoxCount; i++)
    {
        gameMap->staticCollisionBoxes[i] = collisionBoxesBuffer[i];
    }
}

void preloadAssets()
{
    preloadSound("assets/Audio/SE/Thunder.ogg", SOUND_THUNDER);
    preloadSound("assets/Audio/SE/Fireball.ogg", SOUND_FIREBALL);
    preloadSound("assets/Audio/SE/Warp.ogg", SOUND_WARP);
    preloadSound("assets/Audio/SE/Shield.ogg", SOUND_SHIELD);
    preloadSound("assets/Audio/SE/Warmth.ogg", SOUND_WARMTH);
    preloadSound("assets/Audio/SE/FireExplosion.ogg", SOUND_FIREBALL_EXPLODE);
    preloadSound("assets/Audio/SE/JesterLand.ogg", SOUND_JESTER_LAND);
    preloadSound("assets/Audio/SE/Blow.ogg", SOUND_SHAMBLER_HIT);
    preloadSound("assets/Audio/SE/Blow.ogg", SOUND_SPRINTER_HIT);
    preloadSound("assets/Audio/SE/CastFail.ogg", SOUND_CAST_FAIL);
    preloadSound("assets/Audio/SE/ReleaseSpell.ogg", SOUND_RELEASE_SPELL);
    preloadSound("assets/Audio/SE/BloodPact.ogg", SOUND_BLOOD_PACT);
    preloadSound("assets/Audio/SE/ThornShoot.ogg", SOUND_THORN_SHOOT);
    preloadSound("assets/Audio/SE/ThornHit.ogg", SOUND_THORN_HIT);
    preloadSound("assets/Audio/SE/SpellBank.ogg", SOUND_SPELL_BANK);
    preloadSound("assets/Audio/SE/BladeStrike.ogg", SOUND_BLADE_STRIKE);
    preloadSound("assets/Audio/SE/BladeSwoosh.ogg", SOUND_BLADE_SWOOSH);
    preloadSound("assets/Audio/SE/CastGlow.ogg", SOUND_CAST_GLOW);
    preloadSound("assets/Audio/SE/JestersBladeBounce.ogg", SOUND_JESTERS_BLADE_BOUNCE);
    preloadSound("assets/Audio/SE/Gust.ogg", SOUND_GUST);
    preloadSound("assets/Audio/SE/ReverseGust.ogg", SOUND_REVERSE_GUST);
    preloadSound("assets/Audio/SE/IceShard.ogg", SOUND_ICE_SHARD);
    preloadSound("assets/Audio/SE/IceShardSmash.ogg", SOUND_ICE_SHARD_SMASH);
    preloadSound("assets/Audio/SE/LuxCaecus.ogg", SOUND_LUX_CAECUS);
    preloadSound("assets/Audio/SE/MenuOpen.ogg", SOUND_MENU_OPEN);
    preloadSound("assets/Audio/SE/Meditate.ogg", SOUND_MEDITATE);
    preloadSound("assets/Audio/SE/ExpTink.ogg", SOUND_EXP_TINK);
    preloadSound("assets/Audio/SE/storm_thunder.ogg", SOUND_STORM_THUNDER);
    preloadSound("assets/Audio/SE/BruteStrike.ogg", SOUND_BRUTE_STRIKE);
    preloadSound("assets/Audio/SE/FallDamage.ogg", SOUND_FALL_DAMAGE);
    preloadSound("assets/Audio/SE/SoftFall.ogg", SOUND_SOFT_FALL);
    preloadSound("assets/Audio/SE/EnemyCastPrepare.ogg", SOUND_ENEMY_CAST_PREPARE);
    preloadSound("assets/Audio/SE/SlowDown.ogg", SOUND_SLOWDOWN);
    preloadSound("assets/Audio/SE/SpeedUp.ogg", SOUND_SPEEDUP);
    preloadSound("assets/Audio/SE/LowTone.ogg", SOUND_LOW_TONE);
    preloadSound("assets/Audio/SE/LightBurn.ogg", SOUND_LIGHT_BURN);

    std::string voiceBaseFilename = "assets/Audio/Voice/";
    for(int i = 1; i <= 20; i++)
    {
        preloadSound(voiceBaseFilename + std::to_string(i) + ".ogg", (SoundId)(200 + i));
    }

//    preloadMusic("assets/Audio/Music/peaceful/Clean Soul (Title Screen).ogg", "MUSIC_TITLE_SCREEN");
//    preloadMusic("assets/Audio/Music/peaceful/Skye Cuillin (Intrigue at the Tavern).ogg", "MUSIC_WORLD_MAP");
//    preloadMusic("assets/Audio/Music/battle/Black vortex (Dark).ogg", "MUSIC_BATTLE_1");

    preloadMusic("assets/Audio/BGS/rain.ogg", "BGS_RAIN");
    preloadMusic("assets/Audio/BGS/storm.ogg", "BGS_STORM");
    preloadMusic("assets/Audio/BGS/blizzard.ogg", "BGS_BLIZZARD");

    preloadTexture("assets/Graphics/Sprites/bright_one.png", TEXTURE_BRIGHT_ONE_SPRITESHEET, 38);
    preloadTexture("assets/Graphics/Sprites/shambler.png", TEXTURE_SHAMBLER_SPRITESHEET, 18, 24);
    preloadTexture("assets/Graphics/Sprites/sprinter.png", TEXTURE_SPRINTER_SPRITESHEET, 20);
    preloadTexture("assets/Graphics/Sprites/brute.png", TEXTURE_BRUTE_SPRITESHEET, 33, 42);
    preloadTexture("assets/Graphics/Sprites/wizard.png", TEXTURE_WIZARD_SPRITESHEET, 24, 32);
    preloadTexture("assets/Graphics/Sprites/ghost.png", TEXTURE_GHOST_SPRITESHEET, 21, 29);
    preloadTexture("assets/Graphics/Sprites/skeleton_mage.png", TEXTURE_SKELETON_MAGE_SPRITESHEET, 33, 29);

    preloadTexture("assets/Graphics/Sprites/generic_guy.png", TEXTURE_GENERIC_GUY_SPRITESHEET, 16, 24);
    preloadTexture("assets/Graphics/Sprites/innkeeper.png", TEXTURE_INNKEEPER_SPRITESHEET, 15, 31);
    preloadTexture("assets/Graphics/Sprites/farmer_wife.png", TEXTURE_FARMER_WIFE_SPRITESHEET, 15, 29);

    preloadTexture("assets/Graphics/Sprites/woman_1.png", TEXTURE_WOMAN_1_SPRITESHEET, 15, 25);
    preloadTexture("assets/Graphics/Sprites/woman_2.png", TEXTURE_WOMAN_2_SPRITESHEET, 15, 25);
    preloadTexture("assets/Graphics/Sprites/woman_3.png", TEXTURE_WOMAN_3_SPRITESHEET, 15, 28);
    preloadTexture("assets/Graphics/Sprites/woman_4.png", TEXTURE_WOMAN_4_SPRITESHEET, 15, 29);
    preloadTexture("assets/Graphics/Sprites/woman_5.png", TEXTURE_WOMAN_5_SPRITESHEET, 15, 28);

    preloadTexture("assets/Graphics/Sprites/crate.png", TEXTURE_CRATE_SPRITESHEET, 32);
    preloadTexture("assets/Graphics/Sprites/rock.png", TEXTURE_ROCK_SPRITESHEET, 32);
    preloadTexture("assets/Graphics/Sprites/lantern.png", TEXTURE_LANTERN_SPRITESHEET, 14, 17);

    preloadTexture("assets/Graphics/Sprites/debug.png", TEXTURE_DEBUG_SPRITESHEET, 32);
    preloadTexture("assets/Graphics/Sprites/IceShardLarge.png", TEXTURE_ICE_SHARD_LARGE_SPRITESHEET, 9);
    preloadTexture("assets/Graphics/Sprites/IceShardMedium.png", TEXTURE_ICE_SHARD_MEDIUM_SPRITESHEET, 6);
    preloadTexture("assets/Graphics/Sprites/IceShardSmall.png", TEXTURE_ICE_SHARD_SMALL_SPRITESHEET, 3);

    preloadTexture("assets/Graphics/Animations/Fireball.png", TEXTURE_FIREBALL_ANIMATION, 5);
    preloadTexture("assets/Graphics/Animations/Warp.png", TEXTURE_TELEPORT_ANIMATION, 32);
    preloadTexture("assets/Graphics/Animations/Explosion.png", TEXTURE_EXPLOSION_ANIMATION, 48);
    preloadTexture("assets/Graphics/Animations/Warmth.png", TEXTURE_WARMTH_ANIMATION, 48);
    preloadTexture("assets/Graphics/Animations/JestersBlade.png", TEXTURE_JESTERS_BLADE_ANIMATION, 16);

    preloadTexture("assets/Graphics/Sprites/SpellIcons/ChaseAspectIcon.png", TEXTURE_CHASE_ASPECT_ICON, 9);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/ScatterAspectIcon.png", TEXTURE_SCATTER_ASPECT_ICON, 9);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/EmpowerAspectIcon.png", TEXTURE_EMPOWER_ASPECT_ICON, 9);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/PierceAspectIcon.png", TEXTURE_PIERCE_ASPECT_ICON, 9);

    preloadTexture("assets/Graphics/Sprites/SpellIcons/GenericSpellIcon.png", TEXTURE_GENERIC_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/FireballSpellIcon.png", TEXTURE_FIREBALL_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/ChainLightningSpellIcon.png", TEXTURE_CHAIN_LIGHTNING_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/BlizzardSpellIcon.png", TEXTURE_BLIZZARD_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/TeleportSpellIcon.png", TEXTURE_TELEPORT_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/ScorchingLightSpellIcon.png", TEXTURE_SCORCHING_LIGHT_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/JestersBladeSpellIcon.png", TEXTURE_JESTERS_BLADE_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/HomingFireballsSpellIcon.png", TEXTURE_HOMING_FIREBALLS_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/WarmthSpellIcon.png", TEXTURE_WARMTH_SPELL_ICON, 16);
    preloadTexture("assets/Graphics/Sprites/SpellIcons/ShockwaveSpellIcon.png", TEXTURE_SHOCKWAVE_SPELL_ICON, 16);

    preloadTexture("assets/Graphics/Backgrounds/CloudScape.png", TEXTURE_CLOUD_BACKGROUND, 1280);
    preloadTexture("assets/Graphics/Backgrounds/WorldMap.png", TEXTURE_WORLD_MAP_BACKGROUND, 1280);
    preloadTexture("assets/Graphics/Backgrounds/TitleScreen.png", TEXTURE_TITLE_SCREEN_BACKGROUND, 640);

    preloadTexture("assets/Graphics/Sprites/upArrow.png", TEXTURE_UP_ARROW, 5);
    preloadTexture("assets/Graphics/Sprites/leftArrow.png", TEXTURE_LEFT_ARROW, 5);
    preloadTexture("assets/Graphics/Sprites/downArrow.png", TEXTURE_DOWN_ARROW, 5);
    preloadTexture("assets/Graphics/Sprites/rightArrow.png", TEXTURE_RIGHT_ARROW, 5);
    preloadTexture("assets/Graphics/Sprites/upArrowLarge.png", TEXTURE_UP_ARROW_LARGE, 10);
    preloadTexture("assets/Graphics/Sprites/leftArrowLarge.png", TEXTURE_LEFT_ARROW_LARGE, 10);
    preloadTexture("assets/Graphics/Sprites/downArrowLarge.png", TEXTURE_DOWN_ARROW_LARGE, 10);
    preloadTexture("assets/Graphics/Sprites/rightArrowLarge.png", TEXTURE_RIGHT_ARROW_LARGE, 10);


    largeFont.loadFromFile("assets/Graphics/Fonts/elv-pixels-06-long-l.ttf");
    font.loadFromFile("assets/Graphics/Fonts/monogram.ttf");
    smallFont.loadFromFile("assets/Graphics/Fonts/quinque-five.ttf");
    tinyFont.loadFromFile("assets/Graphics/Fonts/smoller-mono.ttf");

    preloadMap("assets/Maps/sky_bridge_battle_map.tmx", "SKY_BRIDGE_BATTLE_MAP");
    preloadMap("assets/Maps/storm_coast_battle_map.tmx", "STORM_COAST_BATTLE_MAP");
    preloadMap("assets/Maps/the_tavern_explore_map.tmx", "THE_TAVERN_EXPLORE_MAP");
    preloadMap("assets/Maps/slid_boss_battle_map.tmx", "SLID_BOSS_BATTLE_MAP");
    preloadMap("assets/Maps/inside_outside_battle_map.tmx", "INSIDE_OUTSIDE_BATTLE_MAP");
    preloadMap("assets/Maps/tileset_demo_explore_map.tmx", "TILESET_DEMO_EXPLORE_MAP");

    preloadSpawnMap("assets/Maps/army_of_mung_spawn_map.tmx", "ARMY_OF_MUNG_SPAWN_MAP");

    for(int i = 0; i < SOUND_QUEUE_SIZE; i++)
    {
        sf::Sound* s = new sf::Sound();
        s->setVolume(100);
        unorderedSoundQueue[i] = s;
    }


    preloadDialogs();
}

std::string getStringBetween(std::string str, std::string s1, std::string s2)
{
    int startChar = str.find(s1) + s1.length();
    int endChar = str.find(s2);
    return str.substr(startChar, endChar - startChar);
}

void playSound(SoundId soundId, float volumeAdjustment, bool isDialogue)
{
    int baseVolume = modulateVolumeWithMasterVolume(globals()->systemOptions[OPTION_SOUND_VOLUME]);

    if(isDialogue)
        baseVolume = modulateVolumeWithMasterVolume(globals()->systemOptions[OPTION_VOICE_VOLUME]);

    if(allSoundBuffers.find(soundId) == allSoundBuffers.end())
    {
        PRINT_DEBUG("Play " << soundId << ", no sound loaded.");
        return;
    }

    for(int i = 0; i < SOUND_QUEUE_SIZE; i++)
    {
        sf::Sound* sound = unorderedSoundQueue[i];

        if(sound->getBuffer() != 0 && sound->getStatus() == sf::SoundSource::Playing)
            continue;   // Keep skipping until we find a sound instance that isn't in use (playing)

        sound->setBuffer(*allSoundBuffers[soundId]);
        sound->setVolume(baseVolume * volumeAdjustment);
        sound->play();
        limitInstancesOfSoundToN(soundId, 10); // Max 5 instances, so e.g. exp diamonds don't overwhelm sound buffer

        return; // Success
    }

    PRINT_DEBUG("Warning: all sound instances in use, while trying to play " << soundId);
}

void limitInstancesOfSoundToN(SoundId soundId, int n)
{
    std::unordered_map<SoundId, sf::SoundBuffer*>::iterator it = allSoundBuffers.find(soundId);

    if(it != allSoundBuffers.end())
    {
        sf::SoundBuffer* soundBuffer = it->second;

        int soundCount = 0;

        for(int i = 0; i < SOUND_QUEUE_SIZE; i++)
        {
            sf::Sound* sound = unorderedSoundQueue[i];

            if(sound->getBuffer() == soundBuffer)
            {
                soundCount++;

                if(soundCount > n)
                {
                    sound->stop(); // TODO this is possibly stopping the wrong one.
                }
            }
        }
    }
    else
    {
        PRINT_DEBUG("Trying to limit instances of " << soundId << ", no sound loaded.");
    }
}

bool isSoundPlaying(SoundId soundId)
{
    if(allSoundBuffers.find(soundId) != allSoundBuffers.end())
    {
        sf::SoundBuffer* sb = allSoundBuffers[soundId];

        for(int i = 0; i < SOUND_QUEUE_SIZE; i++)
        {
            if(unorderedSoundQueue[i]->getBuffer() == sb && unorderedSoundQueue[i]->getStatus() == sf::SoundSource::Playing)
                return true;
        }
    }

    return false;
}

void playDialogWordSound()
{
    SoundId voiceSound = (SoundId)(rand() % 20 + 201);
    playSound(voiceSound, 1.0, true);
}

void playMusic(std::string musicName, float volumeAdjustment)
{
    if(MUTE_MUSIC_FOR_DEBUG) return;

    stopMusic();

    int baseVolume = modulateVolumeWithMasterVolume(globals()->systemOptions[OPTION_MUSIC_VOLUME]);

    if(allMusics.find(musicName) != allMusics.end())
    {
        sf::Music* music = allMusics[musicName];

        music->setVolume(baseVolume * volumeAdjustment);
        music->play();
        music->setLoop(true);
        currentPlayingMusic = music;
    }
    else
    {
        PRINT_DEBUG("Play " << musicName << ", no music loaded.");
    }
}

void stopMusic()
{
    if(currentPlayingMusic != 0)
    {
        currentPlayingMusic->stop();
    }
}

void playAmbient(std::string ambientName, float volumeAdjustment)
{
    stopAmbient();

    int baseVolume = modulateVolumeWithMasterVolume(globals()->systemOptions[OPTION_SOUND_VOLUME]);

    if(allMusics.find(ambientName) != allMusics.end())
    {
        sf::Music* music = allMusics[ambientName];

        music->setVolume(baseVolume * volumeAdjustment);
        music->play();
        music->setLoop(true);
        currentPlayingAmbient = music;
    }
    else
    {
        PRINT_DEBUG("Play " << ambientName << ", no music loaded.");
    }
}

void stopAmbient()
{
    if(currentPlayingAmbient != 0)
    {
        currentPlayingAmbient->stop();
    }
}

void adjustCurrentMusicVolume(float multiplier)
{
    if(currentPlayingMusic == 0)
        return;

    currentPlayingMusic->setVolume(currentPlayingMusic->getVolume() * multiplier);
}

void adjustCurrentAmbientVolume(float multiplier)
{
    if(currentPlayingAmbient == 0)
        return;

    currentPlayingAmbient->setVolume(currentPlayingAmbient->getVolume() * multiplier);
}

void resetSoundsToVolumes()
{
    if(currentPlayingMusic != 0)
        currentPlayingMusic->setVolume(modulateVolumeWithMasterVolume(globals()->systemOptions[OPTION_MUSIC_VOLUME]));
    if(currentPlayingAmbient != 0)
        currentPlayingAmbient->setVolume(modulateVolumeWithMasterVolume(globals()->systemOptions[OPTION_SOUND_VOLUME]));

    for(int i = 0; i < SOUND_QUEUE_SIZE; i++)
    {
        sf::Sound* s = unorderedSoundQueue[i];
        s->setVolume(modulateVolumeWithMasterVolume(globals()->systemOptions[OPTION_SOUND_VOLUME]));
    }
}

int modulateVolumeWithMasterVolume(int volume)
{
    return (int)((float)volume * (float)globals()->systemOptions[OPTION_MASTER_VOLUME] / 100.0);
}

sf::Texture* retrieveTexture(TextureId textureId)
{
    if(allTextures.find(textureId) != allTextures.end())
    {
        return allTextures[textureId];
    }
    else
    {
        PRINT_DEBUG("ERROR: Failed to retrieve texture: " << textureId);
        return 0;
    }
}

sf::Texture* retrieveTextureMask(TextureId textureId)
{
    if(allTextureMasks.find(textureId) != allTextureMasks.end())
    {
        return allTextureMasks[textureId];
    }
    else
    {
        PRINT_DEBUG("ERROR: Failed to retrieve texture mask: " << textureId);
        return 0;
    }
}

sf::Image* retrieveImage(std::string name)
{
    if(allImages.find(name) != allImages.end())
    {
        return allImages[name];
    }
    else
    {
        PRINT_DEBUG("Warning: Failed to retrieve image: " << name);
        return 0;
    }
}

int retrieveNumberOfFrames(TextureId textureId)
{
    if(allTextures.find(textureId) != allTextures.end())
    {
        return framesPerRow[textureId];
    }
    else
    {
        return 0;
    }
}

GameMap* retrieveGameMap(std::string name)
{
    if(allGameMaps.find(name) != allGameMaps.end())
    {
        return allGameMaps[name];
    }
    else
    {
        PRINT_DEBUG("Map " << name << " requested, but was not loaded.");
        return 0;
    }
}

SpawnMap* retrieveSpawnMap(std::string name)
{
    if(allSpawnMaps.find(name) != allSpawnMaps.end())
    {
        return allSpawnMaps[name];
    }
    else
    {
        PRINT_DEBUG("Spawn Map " << name << " requested, but was not loaded.");
        return 0;
    }
}

DialogLine* retrieveDialogPack(std::string name)
{
    if(allDialogPacks.find(name) != allDialogPacks.end())
    {
        return allDialogPacks[name];
    }
    else
    {
        PRINT_DEBUG("Dialogue Pack " << name << " requested, but was not loaded.");
        return 0;
    }
}

void drawText(sf::RenderTarget* target, int x, int y, std::string str, int fontSize, sf::Color color)
{
    sf::Text text;

    if(fontSize == LARGE_FONT)
    {
        text.setFont(largeFont);
        text.setCharacterSize(18);
    }
    else if(fontSize == REGULAR_FONT)
    {
        text.setFont(font);
        text.setCharacterSize(16);
    }
    else if(fontSize == SMALL_FONT)
    {
        text.setFont(smallFont);
        text.setCharacterSize(5);
    }
    else
    {
        text.setFont(tinyFont);
        text.setCharacterSize(8);
    }

    text.setString(str);
    text.setFillColor(color);
    text.setPosition(x, y - 9);

    target->draw(text);
}

void dirtyHash(int* hashValue, std::string line)
{
    if(line.length() < 1)
        return;

    *hashValue += line.length() + (short)line[0] + (short)line[line.length() - 1];
    return;
}

bool isMapDirty(GameMap* gameMap)
{
    std::ifstream file(gameMap->filename);
    std::string line;

    int hashValue = 0;

    while(std::getline(file, line)) // Go through every line of the file
    {
        dirtyHash(&hashValue, line);
    }

    return (hashValue != gameMap->hashValue);
}

int leftEdge(CollisionBox* collisionBox)
{
    return collisionBox->x;
}

int rightEdge(CollisionBox* collisionBox)
{
    return collisionBox->x + collisionBox->width - 1;
}

int topEdge(CollisionBox* collisionBox)
{
    return collisionBox->y;
}

int bottomEdge(CollisionBox* collisionBox)
{
    return collisionBox->y + collisionBox->height - 1;
}

int isCollidingOnLine(int startBoundFirst, int endBoundFirst, int startBoundSecond, int endBoundSecond)
{
    bool collided = (endBoundFirst >= startBoundSecond && endBoundFirst <= endBoundSecond) ||
                        (startBoundFirst >= startBoundSecond && startBoundFirst <= endBoundSecond) ||
                        (startBoundFirst <= startBoundSecond && endBoundFirst >= endBoundSecond);

    if(!collided)
    {
        return 0;
    }

    if((startBoundFirst + endBoundFirst) / 2 <= (startBoundSecond + endBoundSecond) / 2)    // If the middle point of first is above the middle point of second
    {
        int violationMagnitude = endBoundFirst - startBoundSecond + 1; // means first primarily has collision on bottom edge
        if(violationMagnitude == 0)
            violationMagnitude = 1;
        return violationMagnitude; // means first primarily has collision on bottom edge
    }
    else
    {
        int violationMagnitude = startBoundFirst - endBoundSecond - 1;
        if(violationMagnitude == 0)
            violationMagnitude = -1;
        return startBoundFirst - endBoundSecond - 1;  // means first primarly has collision on top edge
    }
}

bool isTileEntity(int tileId)
{
    return (
        tileId == PLAYER_BATTLER_TILE ||
        tileId == PLAYER_EXPLORER_TILE ||
        tileId == SHAMBLER_TILE ||
        tileId == SPRINTER_TILE ||
        tileId == BRUTE_TILE ||
        tileId == WIZARD_TILE ||
        tileId == GHOST_TILE ||
        tileId == SKELETON_MAGE_TILE ||
        tileId == NPC_GENERIC_GUY_TILE ||
        tileId == CRATE_TILE ||
        tileId == ROCK_TILE ||
        tileId == LANTERN_TILE ||
        false
    );
}

std::string getEntitySubtypeDescription(EntitySubType entitySubType)
{
    if(entitySubType == SUBTYPE_SHAMBLER) return "Slowly advances. Chases you when you get close.";
    if(entitySubType == SUBTYPE_SPRINTER) return "Runs forward, always avoiding you. Never attacks.";
    if(entitySubType == SUBTYPE_BRUTE) return "A towering brute. Likes to throw things.";
    if(entitySubType == SUBTYPE_WIZARD) return "Wanders the map, casting fireballs.";
    if(entitySubType == SUBTYPE_GHOST) return "Drifts forward, ignoring all obstacles.";
    if(entitySubType == SUBTYPE_SKELETON_MAGE) return "Slowly advances. Stops to cast beams of dark energy.";

    return "";
}

void preloadDialogs()
{
    sf::Clock timer;

    // GENERIC_GUY_STARTS_WITH_OPTIONS
    {
        DialogLine* mainBranch = startDialogPack("GENERIC_GUY_STARTS_WITH_OPTIONS");

        DialogLine* yesBranch = option(mainBranch, "Yes!", true, SPEAKER_NO_ONE);
        DialogLine* noBranch = option(mainBranch, "Nope.", true, SPEAKER_NO_ONE);

        yesBranch = line(yesBranch, "OK!");
        noBranch = line(noBranch, "Darn...");
    }

    // GENERIC_GUY_OPTIONS_IN_MIDDLE
    {
        DialogLine* mainBranch = startDialogPack("GENERIC_GUY_OPTIONS_IN_MIDDLE");

        DialogLine* firstLine =
        mainBranch = line(mainBranch, "Hello!");
        mainBranch = line(mainBranch, "I'm the first talking NPC");
        mainBranch = line(mainBranch, "Wow!", true, SPEAKER_PLAYER);
        mainBranch = line(mainBranch, "Time to make a choice.", true, SPEAKER_NO_ONE);

        DialogLine* yesBranch = option(mainBranch, "Yes", true, SPEAKER_PLAYER);
        DialogLine* noBranch = option(mainBranch, "No", true, SPEAKER_PLAYER);
        DialogLine* backToStart = option(mainBranch, "Back to Start", true, SPEAKER_PLAYER);

        yesBranch = line(yesBranch, "You said yes.");
        yesBranch = line(yesBranch, "I agree!");
        yesBranch = line(yesBranch, "Let's continue...");

        noBranch = line(noBranch, "You said no.");
        noBranch = line(noBranch, "But I disagree!");

        optionJump(backToStart, firstLine);
        lineJump(noBranch, yesBranch);

        yesBranch = line(yesBranch, "Bye!");
    }

    // GENERIC_GUY_NO_OPTIONS
    {
        DialogLine* mainBranch = startDialogPack("GENERIC_GUY_NO_OPTIONS");

        mainBranch = line(mainBranch, "Greetings.");
        mainBranch = line(mainBranch, "It's a cold night.");
        mainBranch = line(mainBranch, "The more, the merrier.", false);
    }

    PRINT_DEBUG("Loaded dialogs in " << timer.getElapsedTime().asMilliseconds() << "ms");
}

DialogLine* startDialogPack(std::string dialogPackName)
{
    DialogLine* preTrigger = new DialogLine();
    DialogLine* dialogStart = new DialogLine();

    preTrigger->content = "DIALOG_PRE_TRIGGER";
    dialogStart->content = "DIALOG_START";

    preTrigger->branches[0] = dialogStart;
    allDialogPacks[dialogPackName] = preTrigger;

    return dialogStart;
}

DialogLine* line(DialogLine* currentLinePointer, std::string lineContent, bool isLocking, DialogSpeaker speaker)
{
    if(currentLinePointer == 0)
        PRINT_DEBUG("Fatal Error: Null line pointer passed to function");

    if(currentLinePointer->branches[0] != 0)
        PRINT_DEBUG("Fatal Error: Attempting to add line to dialog that already has at least one branch (option).");

    DialogLine* branch = new DialogLine();

    branch->content = lineContent;
    branch->isLocking = isLocking;
    branch->speaker = speaker;
    currentLinePointer->branches[0] = branch;

    return branch;
}

DialogLine* option(DialogLine* currentLinePointer, std::string optionContent, bool isLocking, DialogSpeaker speaker)
{
    if(currentLinePointer == 0)
        PRINT_DEBUG("Fatal Error: Null line pointer passed to function");

    DialogLine* branch = new DialogLine();

    bool placedSuccessfully = false;
    for(int i = 0; i < 4; i++)
    {
        if(currentLinePointer->branches[i] == 0)
        {
            branch->content = optionContent;
            branch->isLocking = isLocking;
            branch->speaker = speaker;
            currentLinePointer->branches[i] = branch;
            placedSuccessfully = true;
            break;
        }
    }

    if(!placedSuccessfully)
    {
        PRINT_DEBUG("Fatal Error: Failed to add option to dialog tree.");
    }

    if(branch->speaker == SPEAKER_PLAYER)
    {
            DialogLine* confirmationLine = new DialogLine();
            confirmationLine->content = optionContent;
            confirmationLine->isLocking = isLocking;
            confirmationLine->speaker = speaker;
            branch->branches[0] = confirmationLine;

            return confirmationLine;
    }
    else
    {
        return branch;
    }
}

DialogLine* lineJump(DialogLine* currentLinePointer, DialogLine* toLinePointer)
{
    if(currentLinePointer == 0 || toLinePointer == 0)
        PRINT_DEBUG("Fatal Error: Null line pointer passed to function");

    return optionJump(currentLinePointer, toLinePointer);
}

DialogLine* optionJump(DialogLine* currentLinePointer, DialogLine* toLinePointer)
{
    if(currentLinePointer == 0 || toLinePointer == 0)
        PRINT_DEBUG("Fatal Error: Null line pointer passed to function");

    bool placedSuccessfully = false;
    for(int i = 0; i < 4; i++)
    {
        if(currentLinePointer->branches[i] == 0)
        {
            currentLinePointer->branches[i] = toLinePointer;
            placedSuccessfully = true;
            break;
        }
    }

    if(!placedSuccessfully)
    {
        PRINT_DEBUG("Fatal Error: Failed to add (jump) option to dialog tree.");
    }

    return toLinePointer;
}
