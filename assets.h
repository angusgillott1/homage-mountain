#ifndef ASSETS_H
#define ASSETS_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unordered_map>
#include <fstream>
#include <unistd.h>
#include <cstdint>

#include "utils.h"
#include "enums.h"
#include "globals.h"

struct EntitySpawn
{
    short int timeIndex = 0;
    uint8_t y;
    EntitySubType entitySubType;
};

struct SpawnMap
{
    std::string filename = "";
    std::string lookupName = "";

    EntitySpawn entitySpawns[900][32] = {0};
    int totalSpawns = 0;

    int startSkip = 0;

    int scoutsAt = -1;
    int vanguardAt = -1;
    int hordeAt = -1;
    int elitesAt = -1;
    int bossAt = -1;
};

struct GridNode
{
    // precomputed
    int x = 0;
    int y = 0;
    GridNode* adjacentNodes[5] = {0}; // skip first index to lign up with cardinal directions

    // dynamic/override
    int computedG = 0;
    int computedF = 0;
    int directionFromParent = 0;
    GridNode* parentNode = 0;
};

struct CompareGridNodes {
    bool operator() (const GridNode* lhs, const GridNode* rhs) const {
        return lhs->computedF > rhs->computedF;
    }
};

struct CollisionBox
{
    short x = 0;
    short y = 0;
    short width = 0;
    short height = 0;
    short layer = 1;
};

struct Tileset
{
    sf::Image* imageData = 0;
    std::string name = "";
    int firstID = 0;
    int nTiles = 0;
    int nColumns = 0;

    CollisionBox* tileCollisionBoxes[100000] = {0};
};

struct GameMapTileLayer
{
    int tiles[256][256] = {0};
    bool isEmpty = false;
};

struct DialogLine
{
    std::string content = "ERROR_UNSET"; // This dialogue's line (actual content)
    DialogLine* branches[4] = {0}; // If 0 branches, dialog end; If 1 branch, ordinary dialog scroll; if > 1 branch, present options
    bool isLocking = false;
    DialogSpeaker speaker = SPEAKER_ENTITY;
};

struct UnpinnedEntityId
{
    int inferredX = 0;
    int inferredY = 0;
    std::string mapEntityId = "";
};

struct EntityPlaceholder
{
    int tileId = 0; // e.g. 2337
    std::string mapEntityId = ""; // e.g. GENERIC_GUY_WANDERING_TAVERN
    int x = 0;
    int y = 0;
    int layer = 0;
};

struct GameMap
{
    std::string filename = "";
    std::string lookupName = "";
    int hashValue = 0;

    MapType mapType = MAP_TYPE_NONE;

    GameMapTileLayer* tileLayers[9] = {0};

    EntityPlaceholder* entityPlaceholders[100] = {0};
    int nEntityPlaceholders = 0;

    int nLayers = 0;
    int nTilesX = 0;
    int nTilesY = 0;
    int tileWidth = 0;

    int worldBoundLeft = 0;
    int worldBoundRight = 0;
    int worldBoundTop = 0;
    int worldBoundBottom = 0;

    float backgroundXVelocity = 0;
    float foregroundXVelocity = 0;

    Tileset* tilesets[20] = {0};
    int nTilesets = 0;

    CollisionBox* staticCollisionBoxes[10000] = {0};
    int nStaticCollisionBoxes = 0;

    GridNode* pathfindingGridNodes[2][10000] = {0};
    int nPathfindingGridNodes[2] = {0};

    sf::Texture* layerTextures[9] = {0};
};

void preloadSound(std::string filename, SoundId soundId);
void preloadMusic(std::string filename, std::string name);
void preloadTexture(std::string filename, TextureId textureId, int frameWidth, int frameHeight = -1);
void preloadImage(std::string, std::string name, int frameWidth);
void preloadMap(std::string filename, std::string name);
void preloadSpawnMap(std::string filename, std::string name);
void preloadAssets();

void combineCollisionBoxesOnMapAlongRows(GameMap* gameMap);
void combineCollisionBoxesOnMapAlongColumns(GameMap* gameMap);

std::string getStringBetween(std::string str, std::string s1, std::string s2);

void playSound(SoundId soundId, float volumeAdjustment = 1.0, bool isDialogue = false);
void limitInstancesOfSoundToN(SoundId soundId, int n);
bool isSoundPlaying(SoundId soundId);

void playDialogWordSound();

void playMusic(std::string musicName, float volumeAdjustment = 1.0);
void stopMusic();

void playAmbient(std::string ambientName, float volumeAdjustment = 1.0);
void stopAmbient();

void adjustCurrentMusicVolume(float multiplier);
void adjustCurrentAmbientVolume(float multiplier);

void resetSoundsToVolumes();
int modulateVolumeWithMasterVolume(int volume);

sf::Texture* retrieveTexture(TextureId textureId);
sf::Texture* retrieveTextureMask(TextureId textureId);
sf::Image* retrieveImage(std::string name);
int retrieveNumberOfFrames(TextureId textureId);

GameMap* retrieveGameMap(std::string name);
SpawnMap* retrieveSpawnMap(std::string name);
DialogLine* retrieveDialogPack(std::string name);

void drawText(sf::RenderTarget* target, int x, int y, std::string str, int fontSize = REGULAR_FONT, sf::Color color = sf::Color(255,255,255,255));
void dirtyHash(int* hashValue, std::string line);
bool isMapDirty(GameMap* gameMap);

int leftEdge(CollisionBox* collisionBox);
int rightEdge(CollisionBox* collisionBox);
int topEdge(CollisionBox* collisionBox);
int bottomEdge(CollisionBox* collisionBox);
int isCollidingOnLine(int startBoundFirst, int endBoundFirst, int startBoundSecond, int endBoundSecond);

bool isTileEntity(int tileId);

std::string getEntitySubtypeDescription(EntitySubType entitySubType);

void preloadDialogs();

DialogLine* startDialogPack(std::string dialogPackName);
DialogLine* line(DialogLine* currentLinePointer, std::string lineContent, bool isLocking = true, DialogSpeaker speaker = SPEAKER_ENTITY); // Adds a single branch, with a sanity check that it is the only branch
DialogLine* option(DialogLine* currentLinePointer, std::string optionContent, bool isLocking = true, DialogSpeaker speaker = SPEAKER_ENTITY); // "Spawns" a new branch, caller needs to manage
DialogLine* lineJump(DialogLine* currentLinePointer, DialogLine* toLinePointer);
DialogLine* optionJump(DialogLine* currentLinePointer, DialogLine* toLinePointer);

#define PLAYER_BATTLER_TILE 50321
#define SHAMBLER_TILE 50353
#define SPRINTER_TILE 50354
#define BRUTE_TILE 50355
#define WIZARD_TILE 50356
#define GHOST_TILE 50357
#define SKELETON_MAGE_TILE 50358
#define CRATE_TILE 50513
#define ROCK_TILE 50514
#define LANTERN_TILE 50515

#define START_TILE 50545

#define SCOUTS_TRIGGER_TILE 50561
#define VANGUARD_TRIGGER_TILE 50562
#define HORDE_TRIGGER_TILE 50563
#define ELITES_TRIGGER_TILE 50564
#define BOSS_TRIGGER_TILE 50565

#define PLAYER_EXPLORER_TILE 2305

#define NPC_GENERIC_GUY_TILE 2337

#endif
