#version 430

uniform float lightSourcesX[50];
uniform float lightSourcesY[50];
uniform float lightSourcesR[50];
uniform float lightSourcesG[50];
uniform float lightSourcesB[50];
uniform float lightSourcesM[50];
uniform float lightSourcesS[50];

uniform int numberOfLightSources;
uniform int screenHeight;

out vec4 fragColor;

// Function to quantize the color or intensity
float quantize(float value, float steps)
{
    return floor(value * steps) / steps;
}

void main()
{
    vec3 finalColor = vec3(0.0, 0.0, 0.0);

    float px = gl_FragCoord.x;
    float py = screenHeight - gl_FragCoord.y - 1;

    for (int i = 0; i < numberOfLightSources; i++)
    {
        float lsx = lightSourcesX[i];
        float lsy = lightSourcesY[i];
		float radius = lightSourcesM[i] + sin(lightSourcesS[i] / 12) * 2.0;

        // Calculate the distance from the light source to the fragment
        float dist = sqrt((px - lsx) * (px - lsx) + (py - lsy) * (py - lsy));

        // If within the radius, add the glow
        if (dist < radius)
        {
			//float intensity = 1.0 - (dist / radius); 
            float intensity = 1.0 - (dist / radius) * (dist / radius);
			//float intensity = exp(-2.0 * (dist / radius));
			
            vec3 lightColor = vec3(lightSourcesR[i], lightSourcesG[i], lightSourcesB[i]);

            // Add the light's color to the final color, weighted by the intensity
			vec3 adjustedLightColor = lightColor * quantize(intensity, 10);
			
			if(finalColor.r < lightSourcesR[i])
			{
				finalColor.r = min(lightSourcesR[i], finalColor.r + adjustedLightColor.r);
			}
			
			if(finalColor.g < lightSourcesG[i])
			{
				finalColor.g = min(lightSourcesG[i], finalColor.g + adjustedLightColor.g);
			}
			
			if(finalColor.b < lightSourcesB[i])
			{
				finalColor.b = min(lightSourcesB[i], finalColor.b + adjustedLightColor.b);
			}
        }
    }

    // Clamp the final color to ensure it stays in the valid range [0.0, 1.0]
    finalColor = clamp(finalColor, 0.0, 1.0);

    // Output the final color
    fragColor = vec4(finalColor, 0.5);
}
