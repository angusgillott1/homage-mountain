#include "entity.h"

//-------------
// Forward declarations
//-------------

void restorePlayerMana(float amount); // stats.cpp

//---------------
// Local globals
//---------------

#define MAX_ENTITIES 4000

Entity* allEntities[MAX_ENTITIES] = {0};

FloatingText* allFloatingTexts[10000] = {0};
int numberOfFloatingTexts = 0;

CompressedMovementPlan movementPlanCache[2][100][100][100][100] = {0};

const int N_BUCKETS_X_AXIS = 16;
const int N_BUCKETS_Y_AXIS = 16;
short int positionBuckets[N_BUCKETS_X_AXIS][N_BUCKETS_Y_AXIS][MAX_ENTITIES] = {0};
short int positionBucketSizes[N_BUCKETS_X_AXIS][N_BUCKETS_Y_AXIS] = {0};

short int leftEdgesCache[MAX_ENTITIES] = {0};
short int rightEdgesCache[MAX_ENTITIES] = {0};
short int topEdgesCache[MAX_ENTITIES] = {0};
short int bottomEdgesCache[MAX_ENTITIES] = {0};

const int BASE_HEALTHBAR_WIDTH = 24;
const int BASE_HEALTHBAR_HEIGHT = 4;

GameMap* currentMap = 0;
SpawnMap* currentSpawnMap = 0;

ArmyStage currentArmyStage = STAGE_BATTLE_START;

sf::Color screenFlashColor = sf::Color::Transparent;
int screenFlashAlphaChangePerTick = 0;

sf::RenderTexture* supplementaryRenderTextures[9] = {0};   // a texture the size of the whole window that an Entity can use to draw arbitrarily. Adjusted by viewport.
sf::Sprite* supplementaryRenderSprites[9] = {0};

unsigned long int entityGuidCounter = 1;

Entity* dialogOwner = 0;

int worldFrameDelay = 1; // modulo of world update

Entity* newEntity(
    EntityType entityType,
    int startX,
    int startY,
    int maxHp,
    int moveSpeed,
    int layer,
    TextureId spritesheetTextureId,
    void (*aiFunction) (Entity*),
    void (*collisionFunction)(Entity* thisGuy, Entity* thatGuy)
)
{
    if(globals()->numberOfEntities > MAX_ENTITIES)
    {
        PRINT_DEBUG("Warning: Entity limit reached. Program will crash.");
    }

    Entity* e = new Entity();
    teleportEntity(e, startX, startY);
    e->guid = entityGuidCounter++;
    e->maxHp = maxHp;
    e->hp = maxHp;
    e->displayHp = e->hp;
    e->layer = layer;
    e->moveSpeed = moveSpeed;

    e->aiFunction = aiFunction;
    e->collisionFunction = collisionFunction;

    e->sprite = new sf::Sprite();
    e->flashSprite = new sf::Sprite();

    if(spritesheetTextureId != TEXTURE_NONE)
    {
        e->sprite->setTexture(*retrieveTexture(spritesheetTextureId));
        e->flashSprite->setTexture(*retrieveTextureMask(spritesheetTextureId));
        e->spritesheetTextureId = spritesheetTextureId;

        sf::Vector2u spritesheetSize = e->sprite->getTexture()->getSize();
        e->spriteWidth = spritesheetSize.x / 3;
        e->spriteHeight = spritesheetSize.y / 4;
    }
    else
    {
        e->shouldNotBeRendered = true;
    }

    e->entityType = entityType;

    if(getCurrentMap()->mapType == MAP_TYPE_BATTLE_MAP && (e->entityType == PLAYER))
    {
        //createLightSource(64.0, 0.6, 0.6, 0.4, e);
    }

    return e;
}

void addEntity(Entity* entity)
{
    allEntities[globals()->numberOfEntities] = entity;
    globals()->numberOfEntities++;
}

void removeEntity(Entity* entity)
{
    int i = 0;

    for(i = 0; i < globals()->numberOfEntities; i++)
    {
        if(allEntities[i] == entity)
            break;
    }

    if(i < globals()->numberOfEntities)    // means the entity was found
    {
        delete allEntities[i]->flashSprite;
        delete allEntities[i]->sprite;
        delete allEntities[i];

        for(i++; i < globals()->numberOfEntities; i++)
        {
            allEntities[i - 1] = allEntities[i];
        }

        globals()->numberOfEntities--;
    }
}

void cleanupEntities()
{
    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* e = allEntities[i];

        if(e->markedForDestruction)
        {
            if(e->accumulatedDisplayDamage)
            {
               addFloatingText(centerX(e), e->y - 12, e->accumulatedDisplayDamage, sf::Color::Red);
            }

            if(e->cleanupFunction != 0)
                e->cleanupFunction(e);

            removeEntity(allEntities[i]);
        }

    }
}

void destroyAllEntities()
{
    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        delete allEntities[i];
    }

    globals()->numberOfEntities = 0;
}

Entity** getAllEntitiesArray()
{
    return allEntities;
}

void renderEntitySprite(sf::RenderWindow* window, Entity* entity, bool doTopHalfOnly)
{
    if(entity->shouldNotBeRendered)
        return;

    Entity* e = entity;

    int offsetX = -globals()->viewPortX;
    int offsetY = -globals()->viewPortY;

    offsetX -= e->hitBoxClearanceLeft;
    offsetY -= e->hitBoxClearanceTop;

    offsetY -= entity->jumpCurrentHeight;

    int spriteRenderHeight = e->spriteHeight;
    if(doTopHalfOnly) spriteRenderHeight /= 2;

    int effectiveFrameRow = e->frameRow;
    if(e->isAnimationSingleRow)
    {
        effectiveFrameRow = 0;
    }

    int animationBoxX = e->frameColumn * e->spriteWidth;
    int animationBoxY = effectiveFrameRow * e->spriteHeight;

    int shakeOffsetX = 0;
    int shakeOffsetY = 0;
    if(e->shakeTicksRemaining > 0 && !getIsMenuOpen())
    {
        if(rand() % 2 == 0) shakeOffsetX = e->shakeMagnitude;
        else shakeOffsetX = -e->shakeMagnitude;

        if(rand() % 2 == 0) shakeOffsetY = e->shakeMagnitude;
        else shakeOffsetY = -e->shakeMagnitude;
    }

    if(isEntityImmobilized(e))
    {
        e->sprite->setRotation(90.f);
        e->flashSprite->setRotation(90.f);
        offsetX += e->spriteHeight;
    }
    else
    {
        e->sprite->setRotation(0.0f);
        e->flashSprite->setRotation(0.0f);
    }

    e->sprite->setPosition(e->x + offsetX + shakeOffsetX, e->y + offsetY + shakeOffsetY);
    if(e->isAnimated)
    {
        e->sprite->setTextureRect(sf::IntRect(animationBoxX, animationBoxY, e->spriteWidth, spriteRenderHeight));
    }
    else
    {
        e->sprite->setTextureRect(sf::IntRect(0, 0, e->spriteWidth, spriteRenderHeight));
    }
    window->draw(*(e->sprite));

    if(e->flashTicksRemaining >= 0)
    {
        e->flashSprite->setPosition(e->x + offsetX + shakeOffsetX, e->y + offsetY + shakeOffsetY);
        if(e->isAnimated)
        {
            e->flashSprite->setTextureRect(sf::IntRect(animationBoxX, animationBoxY, e->spriteWidth, spriteRenderHeight));
        }

        window->draw(*(e->flashSprite));
    }
}

void renderEntityHealthBar(sf::RenderWindow* window, Entity* entity)
{
    if(entity->entityType == PLAYER)
        return;

    if(entity->hp == entity->maxHp)
        return;

    if(entity->maxHp <= 0)
        return;

    if(entity->displayHp > 0)
    {
        float barWidth = (float)BASE_HEALTHBAR_WIDTH;
        float barHeight = (float)BASE_HEALTHBAR_HEIGHT;

        int hbx = entity->x - globals()->viewPortX - ((barWidth - trueWidth(entity)) / 2);
        int hby = entity->y - entity->hitBoxClearanceTop - (barHeight + 1) - globals()->viewPortY - entity->jumpCurrentHeight;

        sf::RectangleShape rectangle(sf::Vector2f(std::max(floor((barWidth) * ((float)entity->displayHp / entity->maxHp)), (float)3.0), (barHeight)));
        rectangle.setOutlineColor(sf::Color (0, 0, 0, 255));
        rectangle.setOutlineThickness(-1.0f);
        rectangle.setFillColor(sf::Color (20, 255, 20, 255));
        rectangle.setPosition(hbx, hby);
        window->draw(rectangle);
    }
}

void renderMapWithEntities(sf::RenderWindow* window)
{
    sf::Sprite mapSprite;
    int numberOfEntities = globals()->numberOfEntities;

    for(int j = 1; j <= 13; j++)
    {
        if(j >= 1 && j <= 8)
        {
            sf::Texture* texture = currentMap->layerTextures[j];

            mapSprite.setTexture(*texture);

            int x = -globals()->viewPortX;
            int y = -globals()->viewPortY;
            int mapTextureWidth = texture->getSize().x;

            if(j == 1) x = (x + (int)(currentMap->backgroundXVelocity * globals()->worldTick + mapTextureWidth) % mapTextureWidth);
            if(j == 8) x = (x + (int)(currentMap->foregroundXVelocity * globals()->worldTick + mapTextureWidth) % mapTextureWidth);

            mapSprite.setPosition(x, y);
            window->draw(mapSprite);

            if(j == 1 || j == 8)
            {
                mapSprite.setPosition(x - mapTextureWidth, y);
                window->draw(mapSprite);
                mapSprite.setPosition(x + mapTextureWidth, y);
                window->draw(mapSprite);
            }

            // Draw global supplementary texture/sprite
            sf::RenderTexture* r = supplementaryRenderTextures[j];
            sf::Sprite* s = supplementaryRenderSprites[j];
            r->display();
            s->setPosition(-globals()->viewPortX, -globals()->viewPortY);
            window->draw(*s);

            // Render particles
            renderParticleLayer(j, window);
        }

        // Gather all entities in current layer
        std::vector<Entity*> entitiesInLayer;
        for(int i = 0; i < numberOfEntities; i++)
        {
            int effectiveLayer = allEntities[i]->layer;
            if(allEntities[i]->isJumping)
                effectiveLayer += 1;

            if(effectiveLayer == j) entitiesInLayer.push_back(allEntities[i]);
        }

        // Sort and render
        std::sort(entitiesInLayer.begin(), entitiesInLayer.end(), [](Entity* a, Entity* b) { return a->y < b->y; });
        for(Entity* entity : entitiesInLayer) renderEntitySprite(window, entity);

        // Render top half of player a second time, two layers above; to ensure player doesn't have z-axis conflicts
        Entity* player = globals()->player;
        int effectiveLayer = player->layer + 2;
        if(player->isJumping)
            effectiveLayer += 1;
        if(effectiveLayer == j)
        {
            renderEntitySprite(window, player, true);
        }
    }

    renderWeather(window);

    if(currentMap->mapType == MAP_TYPE_BATTLE_MAP)
        renderDarkness(window);

    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        renderEntityHealthBar(window, allEntities[i]);
    }

    // draw screen flash
    sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
    rectangle.setPosition(0, 0);
    rectangle.setFillColor(screenFlashColor);
    window->draw(rectangle);

    // Wipe supplementary textures at the end of each frame
    for(int i = 1; i <= 8; i++)
        supplementaryRenderTextures[i]->clear(sf::Color(0,0,0,0));

    // Render UI "glow" for interactable entity
    if(getCurrentScene() == SCENE_BATTLE_OR_EXPLORE && getCurrentMap()->mapType == MAP_TYPE_EXPLORE_MAP && globals()->currentDialogFocusedEntity != 0)
    {
        Entity* e = globals()->currentDialogFocusedEntity;

        // Brighten entity to highlight it
        makeEntityFlash(e, 10, sf::Color(255,255,255,35), sf::Color(0,0,0,5));

//        // Draw "Talk" box
//        const int smallCharacterTrueWidth = 5;
//        const int smallCharacterTrueXGap = 1;
//        const int smallTextTrueHeight = 12;
//        const int w = (smallCharacterTrueWidth + smallCharacterTrueXGap) * 4 + 4; // 8 is number of characters
//        const int h = smallTextTrueHeight;
//
//        int x = centerX(e) - w / 2 - globals()->viewPortX + 2;
//        int y = topEdge(e) - e->hitBoxClearanceTop - h - globals()->viewPortY - 2;
//
//        drawText(window, x, y, "Talk", REGULAR_FONT);
//
//        sf::RectangleShape rectangle(sf::Vector2f(w, h));
//        rectangle.setPosition(x - 2, y - 2);
//        rectangle.setFillColor(sf::Color (0,0,0,0));
//        rectangle.setOutlineColor(sf::Color(255,255,255,255));
//        rectangle.setOutlineThickness(1.0f);
//        window->draw(rectangle);
    }
}

void renderStaticCollisionOverlay(sf::RenderWindow* window)
{
//    // Pixel hunting
//    sf::RectangleShape shape(sf::Vector2f(1, 1));
//    shape.setFillColor(sf::Color(255, 0, 255, 255));
//    shape.setPosition(sf::Vector2f(346 + 1 * 120 + 2 * 16, 13));
//    window->draw(shape);

    // entity collision boxes
    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* e = allEntities[i];
        int w = rightEdge(e) - leftEdge(e);
        int h = bottomEdge(e) - topEdge(e);

        sf::RectangleShape shape(sf::Vector2f(w, h));
        shape.setFillColor(sf::Color(255, 0, 255, 80));
        shape.setOutlineThickness(0.f);
        shape.setPosition(sf::Vector2f(leftEdge(e) - globals()->viewPortX, topEdge(e) - globals()->viewPortY));
        window->draw(shape);
    }

    // static collision boxes
    for (int i = 0; i < currentMap->nStaticCollisionBoxes; i++)
    {
        CollisionBox* cb = currentMap->staticCollisionBoxes[i];

        sf::RectangleShape shape(sf::Vector2f(cb->width - 2, cb->height - 2));
        shape.setFillColor(sf::Color(255, 0, 255, 80));

        if(cb->layer <= 3)
            shape.setFillColor(sf::Color(255, 0, 255, 30));

        shape.setOutlineColor(sf::Color(255, 0, 0, 120));
        shape.setOutlineThickness(1.f);
        shape.setPosition(sf::Vector2f(cb->x + 1 - globals()->viewPortX, cb->y + 1 - globals()->viewPortY));
        window->draw(shape);
    }

    // passable tiles (pathfinding grid nodes)
    for(int n = 1; n <= 2; n++)
    {
        for (int i = 0; i < currentMap->nPathfindingGridNodes[n - 1]; i++)
        {
            GridNode* gn = currentMap->pathfindingGridNodes[n - 1][i];

            sf::RectangleShape shape(sf::Vector2f(3, 3));
            shape.setFillColor(sf::Color(0, 0, 255, 50 + n * 50));
            shape.setOutlineThickness(0.f);
            shape.setPosition(sf::Vector2f(gn->x * currentMap->tileWidth - globals()->viewPortX + currentMap->tileWidth / 2 - 1, gn->y * currentMap->tileWidth - globals()->viewPortY + currentMap->tileWidth / 2 - 1));
            window->draw(shape);
        }
    }

    // passable movement (pathfinding grid edges)
//    for (int i = 0; i < currentMap->nPathfindingGridNodes; i++)
//    {
//        GridNode* gn = currentMap->pathfindingGridNodes[i];
//
//        int n = 0;
//        for(int i = 1; i <= 4; i++)
//        {
//            if(gn->adjacentNodes[i]) n++;
//        }
//
//        drawText(window, gn->x * currentMap->tileWidth - globals()->viewPortX + currentMap->tileWidth / 2, gn->y * currentMap->tileWidth - globals()->viewPortY + currentMap->tileWidth / 2 - 1, std::to_string(n), REGULAR_FONT, sf::Color(255, 255, 0, 180));
//    }

    // pathfinding debugging
    for(int n = 1; n <= 2; n++)
    {
        for (int i = 0; i < currentMap->nPathfindingGridNodes[n - 1]; i++)
        {
            GridNode* gn = currentMap->pathfindingGridNodes[n - 1][i];
            if(!gn->computedG) continue;
            drawText(window, gn->x * currentMap->tileWidth - globals()->viewPortX + currentMap->tileWidth / 2, gn->y * currentMap->tileWidth - globals()->viewPortY + currentMap->tileWidth / 2 - 1, std::to_string(gn->computedG), REGULAR_FONT, sf::Color(255, 255, 0, 130 + n * 50));
        }
    }

    // draw world bounds
    {
        sf::RectangleShape shape(sf::Vector2f(getWorldBoundRight() - getWorldBoundLeft(), getWorldBoundBottom() - getWorldBoundTop()));
        shape.setFillColor(sf::Color::Transparent);
        shape.setOutlineThickness(2.f);
        shape.setOutlineColor(sf::Color(255, 0, 0, 200));
        shape.setPosition(getWorldBoundLeft(), getWorldBoundTop());
        window->draw(shape);
    }
}

sf::RenderTexture* getSupplementaryRenderTexture(int layer)
{
    return supplementaryRenderTextures[layer];
}

void makeEntityJump(Entity* entity, int dx, int dy, int height, int jumpSpeed, SoundId soundOnLand, int damageOnLand)
{
    if(entity->isJumping)
    {
        return;
    }

    entity->isJumping = true;
    entity->jumpMaxHeight = height;
    entity->jumpCurrentHeight = 0;
    entity->jumpTick = 0;
    entity->jumpSpeed = jumpSpeed;
    entity->jumpLandSoundId = soundOnLand;
    entity->jumpLandDamage = damageOnLand;

    entity->goalX = entity->x + dx;
    entity->goalY = entity->y + dy;

    cancelMovementPlan(entity);
}

void makeEntityJumpTo(Entity* entity, int x, int y, int height, int forcedDuration, SoundId soundOnLand, int damageOnLand)
{
    entity->isJumping = true;
    entity->jumpMaxHeight = height;
    entity->jumpCurrentHeight = 0;
    entity->jumpTick = 0;
    entity->jumpDuration = forcedDuration;
    entity->jumpLandSoundId = soundOnLand;
    entity->jumpLandDamage = damageOnLand;

    int destX = x;
    int destY = y;

    int loopPanicTick = 0;
    while(!isRegionFreeOfStaticCollisions(destX, destY, trueWidth(entity), trueHeight(entity), entity->entityType))
    {
        destX = regressToMean(destX, 1, entity->x);
        destY = regressToMean(destY, 1, entity->y);

        if(loopPanicTick++ > 1000)
        {
            destX = entity->x;
            destY = entity->y;
            PRINT_DEBUG("ERROR: Loop panic condition.");
            break;
        }
    }

    int dxAbs = std::abs(destX - entity->x);
    int dyAbs = std::abs(destY - entity->y);
    entity->jumpSpeed = (int)(std::max(dxAbs, dyAbs) / (float)forcedDuration);

    entity->goalX = x;
    entity->goalY = y;

    cancelMovementPlan(entity);
}

void makeEntityMoveAccordingToCurrentInput(Entity* entity, bool changeFacing, bool adjustIfImpassable, bool diagonalMovementAllowed)
{
    if(entity == globals()->player && globals()->isPlayerMoveSelfDisabled)
        return;

    if(globals()->isPlayerDialogLocked)
        return;

    int desiredDx = 0;
    int desiredDy = 0;

    int desiredSpeed = entity->moveSpeed;

    if(isEntityImmobilized(entity))
    {
        if(isKeyDownThisFrame(KEY_LEFT) || isKeyDownThisFrame(KEY_RIGHT) || isKeyDownThisFrame(KEY_UP) || isKeyDownThisFrame(KEY_DOWN))
        {
            entity->frameRow = (entity->frameRow + 1) % 4;
        }

        return;
    }

    if(isKeyDown(KEY_LEFT))
    {
        desiredDx -= desiredSpeed;
        if(!diagonalMovementAllowed) goto IGNORE_FURTHER_MOVEMENT_INPUT;
    }

    if(isKeyDown(KEY_RIGHT)){
        desiredDx += desiredSpeed;
        if(!diagonalMovementAllowed) goto IGNORE_FURTHER_MOVEMENT_INPUT;
    }

    if(isKeyDown(KEY_UP))
    {
        desiredDy -= desiredSpeed;
        if(!diagonalMovementAllowed) goto IGNORE_FURTHER_MOVEMENT_INPUT;
    }

    if(isKeyDown(KEY_DOWN)){
        desiredDy += desiredSpeed;
        if(!diagonalMovementAllowed) goto IGNORE_FURTHER_MOVEMENT_INPUT;
    }

    IGNORE_FURTHER_MOVEMENT_INPUT:

    setEntityMoveIntention(entity, desiredDx, desiredDy, changeFacing, adjustIfImpassable);
}


void stepEntityAnimations()
{
    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* e = allEntities[i];

        if(e->animationBoundToEntity != 0)
        {
            e->x = e->animationBoundToEntity->x + e->attrs[ATTR_MEMORY_1];
            e->y = e->animationBoundToEntity->y + e->attrs[ATTR_MEMORY_2];
        }

        if(e->flashTicksRemaining <= 0)
        {
            e->flashSprite->setColor(sf::Color::Transparent);
        }
        else
        {
            e->flashSprite->setColor(e->flashColor);
            e->flashColor -= e->flashDecay;
            e->flashTicksRemaining--;
        }

        if(e->shakeTicksRemaining > 0)
        {
            e->shakeTicksRemaining--;
        }

        if(e->isAnimationPaused)
            continue;

        if(!isEntityMoving(e) && !e->shouldAnimateAtRest)
            continue;

        e->animationTick++;

        if(e->animationTick >= e->ticksPerFrame)
        {
            e->animationTick = 0;
            e->animationPatternIndex++;
            if(e->animationPatternIndex + 1 > e->animationPatternLength)
            {
                if(e->dieOnAnimationComplete)
                {
                    e->markedForDestruction = true;
                    e->animationPatternIndex -= 1;
                }
                else
                {
                    e->animationPatternIndex = 0;
                }
            }
        }

        e->frameColumn = e->animationPattern[e->animationPatternIndex];
    }
}

void resetEntityAnimationToItsFacing(Entity* e)
{
    if(e->facing == 3)
        e->frameRow = 0;
    if(e->facing == 2)
        e->frameRow = 1;
    if(e->facing == 4)
        e->frameRow = 2;
    if(e->facing == 1)
        e->frameRow = 3;
}

void setEntityMoveIntention(Entity* entity, int dx, int dy, bool changeFacing, bool adjustIfImpassable)
{
    int gx = entity->x + dx;
    int gy = entity->y + dy;

    if(leftEdge(entity) + dx < getWorldBoundLeft(entity->entityType == PLAYER))
        gx = leftMostValidPoint(entity);
    if(rightEdge(entity) + dx >= getWorldBoundRight(entity->entityType == PLAYER))
        gx = rightMostValidPoint(entity);
    if(topEdge(entity) + dy < getWorldBoundTop())
        gy = topMostValidPoint(entity);
    if(bottomEdge(entity) + dy >= getWorldBoundBottom())
        gy = bottomMostValidPoint(entity);

    if(changeFacing && (dx != 0 || dy != 0) )
    {
        if(abs(dx) > abs(dy))
        {
            if(dx > 0)
                entity->facing = 4;
            else
                entity->facing = 2;
        }
        else
        {
            if(dy > 0)
                entity->facing = 3;
            else
                entity->facing = 1;
        }

        resetEntityAnimationToItsFacing(entity);
    }

    if(adjustIfImpassable)
    {
        Entity dummy;
        dummy.spriteWidth = entity->spriteWidth;
        dummy.spriteHeight = entity->spriteHeight;
        dummy.hitBoxClearanceLeft = entity->hitBoxClearanceLeft;
        dummy.hitBoxClearanceTop = entity->hitBoxClearanceTop;
        dummy.hitBoxClearanceRight = entity->hitBoxClearanceRight;
        dummy.hitBoxClearanceBottom = entity->hitBoxClearanceBottom;
        dummy.x = gx;
        dummy.y = gy;

        int xViolation = 0;
        int yViolation = 0;

        int topEdgeCollidee = 0;
        int leftEdgeCollidee = 0;
        int bottomEdgeCollidee = 0;
        int rightEdgeCollidee = 0;

        if(!(xViolation && yViolation))
        {
            for(int i = 0; i < currentMap->nStaticCollisionBoxes; i++)
            {
                CollisionBox* cb = currentMap->staticCollisionBoxes[i];

                xViolation = isCollidingOnX(&dummy, cb);
                yViolation = isCollidingOnY(&dummy, cb);

                if(entity->entityType == SPELL && cb->layer <= 3)
                    continue;

                if(xViolation && yViolation)
                {
                    topEdgeCollidee = topEdge(cb);
                    leftEdgeCollidee = leftEdge(cb);
                    bottomEdgeCollidee = bottomEdge(cb);
                    rightEdgeCollidee = rightEdge(cb);
                    break;
                }
            }
        }

        if(xViolation && yViolation)
        {
            if(xViolation < 0)
            {
                int xGap = leftEdge(entity) - rightEdgeCollidee - 1;
                if(xGap > 0)
                    gx = entity->x - xGap;
                else
                    gx = entity->x;
            }

            if(xViolation > 0)
            {
                int xGap = leftEdgeCollidee - rightEdge(entity) - 1;
                if(xGap > 0)
                    gx = entity->x + xGap;
                else
                    gx = entity->x;
            }

            if(yViolation < 0)
            {
                int yGap = topEdge(entity) - bottomEdgeCollidee - 1;
                if(yGap > 0)
                    gy = entity->y - yGap;
                else
                    gy = entity->y;
            }

            if(yViolation > 0)
            {
                int yGap = topEdgeCollidee - bottomEdge(entity) - 1;
                if(yGap > 0)
                    gy = entity->y + yGap;
                else
                    gy = entity->y;
            }

            if((entity->facing == 1 || entity->facing == 3)  && abs(xViolation) <= trueWidth(entity) / 1.5)
            {
                gx = entity->x -xViolation;

                if(gx < getWorldBoundLeft(entity->entityType == PLAYER)) gx = getWorldBoundLeft(entity->entityType == PLAYER);
                if(gx + trueWidth(entity) > getWorldBoundRight(entity->entityType == PLAYER)) gx = getWorldBoundRight(entity->entityType == PLAYER) - trueWidth(entity);
            }

            if((entity->facing == 2 || entity->facing == 4) && abs(yViolation) <= trueHeight(entity) / 1.5)
            {
                gy = entity->y -yViolation;

                if(gy < getWorldBoundTop()) gy = getWorldBoundTop();
                if(gy + trueHeight(entity) > getWorldBoundBottom()) gy = getWorldBoundBottom() - trueHeight(entity);
            }
        }
    }

    entity->goalX = gx;
    entity->goalY = gy;
}

void setEntityMoveIntentionGrid(Entity* entity, int cardinalDirection, int distance, bool changeFacing, bool adjustIfImpassable)
{
    if(cardinalDirection == 1)
    {
        setEntityMoveIntention(entity, 0, -distance, changeFacing, adjustIfImpassable);
    }

    if(cardinalDirection == 3)
    {
         setEntityMoveIntention(entity, 0, distance, changeFacing, adjustIfImpassable);
    }

    if(cardinalDirection == 2)
    {
        setEntityMoveIntention(entity, -distance, 0,changeFacing, adjustIfImpassable);
    }

    if(cardinalDirection == 4)
    {
        setEntityMoveIntention(entity, distance, 0, changeFacing, adjustIfImpassable);
    }
}

bool isEntityMoving(Entity* entity)
{
    return (entity->goalX - entity->x != 0 || entity->goalY - entity->y != 0 || entity->didMoveThisFrame);
}

bool isEntityAtGoalLocation(Entity* entity)
{
    return (entity->goalX - entity->x == 0 && entity->goalY - entity->y == 0);
}

void teleportEntity(Entity* entity, int x, int y)
{
    entity->x = x;
    entity->y = y;
    entity->goalX = x;
    entity->goalY = y;
}

void stepEntityActions()
{
    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* e = allEntities[i];
        e->lifeTimer++;

        if(isEntityWaiting(e) || isEntityImmobilized(e))
            continue;

        if(e->aiFunction)
        {
            e->aiFunction(e);
        }
    }
}

void stepEntityMovements()
{
    bool isEntitySlowed[globals()->numberOfEntities] = {0};

    sf::RenderTexture* lightOverlayTexture = getLightOverlayTexturePointer();
    if(lightOverlayTexture != 0)
    {
        const sf::Texture& texture = lightOverlayTexture->getTexture();
        sf::Image image = texture.copyToImage();

        for(int i = 0; i < globals()->numberOfEntities; i++)
        {
            Entity* e = allEntities[i];

            if(e->entityType != ENEMY)
                continue;

            sf::Color pointColor = image.getPixel(centerX(e), centerY(e));
            int pointBrightness = (pointColor.r + pointColor.g + pointColor.b) / 3;

            if(pointBrightness > 40)
            {
                isEntitySlowed[i] = true;
            }
        }
    }

    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* e = allEntities[i];
        int spd = e->moveSpeed;

        int startX = e->x;
        int startY = e->y;

        if(isEntitySlowed[i] && e->lightBurnCooldown == 0)
        {
            dealDamageToEntity(e, 1, true);
            playSound(SOUND_LIGHT_BURN);
            e->lightBurnCooldown = 10;
        }

        bool isSkippedMovement = false;
        if(globals()->worldTick % (1 + (int)isEntitySlowed[i]) != 0 && !e->isJumping)
        {
            isSkippedMovement = true;
            goto SKIP_MOVEMENT;
        }

        if(e->isJumping)
        {
            spd = e->jumpSpeed;
            e->jumpTick++;
            e->jumpCurrentHeight = int(sin(( (float)e->jumpTick / (float)e->jumpDuration ) * PI_VALUE) * e->jumpMaxHeight);
        }
        if(e->isJumping && e->jumpTick == e->jumpDuration)
        {
            e->isJumping = false;
            e->jumpCurrentHeight = 0;
            e->jumpLandedAtTick = globals()->worldTick;
            if(e->jumpLandSoundId != SOUND_NONE)
            {
                playSound(e->jumpLandSoundId);
                e->jumpLandSoundId = SOUND_NONE;
            }
            if(e->jumpLandDamage != 0)
            {
                dealDamageToEntity(e, e->jumpLandDamage);
                e->jumpLandDamage = 0;
            }
        }

        if(isEntityImmobilized(e) && !(e == globals()->player))
        {
            cancelMovementPlan(e);
            if(globals()->worldTick % e->ticksPerFrame == 0) e->frameRow = rand() % 4;
        }

        if(e->goalX > e->x)
        {
            if(e->goalX - e->x >= spd)
            {
                e->x += spd;
            }
            else
            {
                e->x = e->goalX;
            }
        }
        else
        {
            if(e->x - e->goalX >= spd)
            {
                e->x -= spd;
            }
            else
            {
                e->x = e->goalX;
            }
        }

        if(e->goalY > e->y)
        {
            if(e->goalY - e->y >= spd)
            {
                e->y += spd;
            }
            else
            {
                e->y = e->goalY;
            }
        }
        else
        {
            if(e->y - e->goalY >= spd)
            {
                e->y -= spd;
            }
            else
            {
                e->y = e->goalY;
            }
        }

        SKIP_MOVEMENT:

        e->didMoveThisFrame = (e->x != startX || e->y != startY || (!isEntityAtGoalLocation(e) && isSkippedMovement));
        e->impliedXVelocity = e->x - startX;
        e->impliedYVelocity = e->y - startY;
    }

    // Set current dialog focused entity
    globals()->currentDialogFocusedEntity = findEntityInInteractRegion(globals()->player);
}

void stepEntityMiscs()
{
    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* e = allEntities[i];

        e->displayHp = regressToMean(e->displayHp, 4, e->hp);
        e->invincibilityCooldown = regressToMean(e->invincibilityCooldown, 1, 0);
        e->lightBurnCooldown = regressToMean(e->lightBurnCooldown, 1, 0);
        globals()->playerStats->playerDisplayMp = regressToMean(globals()->playerStats->playerDisplayMp, 4.0, globals()->playerStats->playerMp);
    }

    if(screenFlashColor.a > 0)
    {
        screenFlashColor.a -= screenFlashAlphaChangePerTick;
    }

}

void doSpawnMapForTimeIndex(short int timeIndex)
{
    if(currentSpawnMap == 0)
        return;

    timeIndex += currentSpawnMap->startSkip;

    //PRINT_DEBUG("Spawn entities for time index " << timeIndex);

    if(timeIndex < 0)
    {
        PRINT_DEBUG(" Warning: possible time index overflow");
        return;
    }

    if(timeIndex >= 900)
    {
        return; // Valid state; will happen during boss fight
    }

    // Spawn entities for this index
    for(int i = 0; i < 32; i++)
    {
        if(currentSpawnMap->entitySpawns[timeIndex][i].entitySubType == SUBTYPE_NONE)
            continue;

        EntitySpawn spawn = currentSpawnMap->entitySpawns[timeIndex][i];
        int y = spawn.y * 16;

        if(spawn.entitySubType == SUBTYPE_SHAMBLER) shamblerAdd(getWorldBoundRight() - 18, y);
        if(spawn.entitySubType == SUBTYPE_SPRINTER) sprinterAdd(getWorldBoundRight() - 16, y);
        if(spawn.entitySubType == SUBTYPE_BRUTE) bruteAdd(getWorldBoundRight() - 28, y);
        if(spawn.entitySubType == SUBTYPE_WIZARD) wizardAdd(getWorldBoundRight() - 16, y);
        if(spawn.entitySubType == SUBTYPE_GHOST) ghostAdd(getWorldBoundRight() - 16, y);
        if(spawn.entitySubType == SUBTYPE_SKELETON_MAGE) skeletonMageAdd(getWorldBoundRight() - 16, y);

        PRINT_DEBUG(" Spawn enemy!");
    }

    // Set army stage
    if(timeIndex == currentSpawnMap->scoutsAt) currentArmyStage = STAGE_SCOUTS;
    if(timeIndex == currentSpawnMap->vanguardAt) currentArmyStage = STAGE_VANGUARD;
    if(timeIndex == currentSpawnMap->hordeAt) currentArmyStage = STAGE_HORDE;
    if(timeIndex == currentSpawnMap->elitesAt) currentArmyStage = STAGE_ELITES;
    if(timeIndex == currentSpawnMap->bossAt) currentArmyStage = STAGE_BOSS;

    // Set weather if at transition between stages
    if(
       timeIndex == currentSpawnMap->scoutsAt ||
       timeIndex == currentSpawnMap->vanguardAt ||
       timeIndex == currentSpawnMap->hordeAt ||
       timeIndex == currentSpawnMap->elitesAt ||
       timeIndex == currentSpawnMap->bossAt
    )
    {
        matchWeatherToArmyStage();
    }

    // Spawn boss if applicable
    if(timeIndex == currentSpawnMap->bossAt)
    {
        PRINT_DEBUG(" Spawn boss at: " << timeIndex);
    }
}

void makeEntityWait(Entity* entity, int time)
{
    entity->waitUntil = entity->lifeTimer + time;
    cancelMovementPlan(entity);
    setEntityMoveIntention(entity, 0, 0, false);
    resetEntityAnimationToItsFacing(entity);
    entity->animationPatternIndex = 0;
    entity->frameColumn = entity->animationPattern[0];
}

bool isEntityWaiting(Entity* entity)
{
    return (entity->lifeTimer < entity->waitUntil);
}

void immobilizeEntity(Entity* entity, int duration)
{
    entity->immobilizedUntil = entity->lifeTimer + duration;
    cancelMovementPlan(entity);
    resetEntityAnimationToItsFacing(entity);
}

void deImmobilizeEntity(Entity* entity)
{
    entity->immobilizedUntil = entity->lifeTimer - 1;
}

bool isEntityImmobilized(Entity* entity)
{
    return (entity->lifeTimer < entity->immobilizedUntil);
}

bool dealDamageToEntity(Entity* entity, long long int amount, bool suppressAnimation)
{
    if(entity->maxHp <= 0)
        return false;

    if(entity->invincibilityCooldown > 0)
        return false;

    entity->hp -= amount;
    if(entity->hp <= 0)
    {
        //PRINT_DEBUG("Killed entity. Hp: " << entity->hp << ", max hp: " << entity->maxHp);
        entity->hp = 0;

        if(entity->entityType != PLAYER)
        {
            spawnExpPacket(centerX(entity), centerY(entity), entity->maxHp);

            for(int i = 0; i < 20; i++)
            {
                createParticle(centerX(entity) + rand() % 16 - 8, centerY(entity) + rand() % 16 - 8, 2, entity->layer, rand() % 3 - 1, rand() % 3 - 1, rand() % 3, 1, 20, sf::Color(30, 30, 30, 150), sf::Color::Transparent, sf::Color(0, 0, 0, 10));
            }

            entity->markedForDestruction = true;
            globals()->entitiesDefeated++;
            //PRINT_DEBUG("Enemies defeated: " << globals()->entitiesDefeated);
        }
    }

    entity->accumulatedDisplayDamage += amount;
    entity->accumulatedDisplayDamageDebounce = globals()->worldTick;

    if(!suppressAnimation)
    {
        makeEntityFlash(entity, 15, sf::Color(220,0,0,240), sf::Color(0,0,0,16));
        makeEntityShake(entity, 10, 1);
    }

    return true;
}

void healEntity(Entity* entity, int amount)
{
    if(entity->maxHp <= 0)
        return;

    int addAmount = amount;
    if(entity->hp + addAmount > entity->maxHp)
    {
        addAmount = entity->maxHp - entity->hp;
    }

    if(addAmount == 0)
        return;

    entity->hp += addAmount;
    addFloatingText(centerX(entity), entity->y, addAmount, sf::Color(50,255,50));
}

int leftEdge(Entity* entity)
{
    return entity->x;
}

int rightEdge(Entity* entity)
{
    return entity->x + trueWidth(entity) - 1;
}

int topEdge(Entity* entity)
{
    return entity->y;
}

int bottomEdge(Entity* entity)
{
    return entity->y + trueHeight(entity) - 1;
}

int trueWidth(Entity* entity)
{
    return entity->spriteWidth - (entity->hitBoxClearanceLeft + entity->hitBoxClearanceRight);
}

int trueHeight(Entity* entity)
{
    return entity->spriteHeight - (entity->hitBoxClearanceTop + entity->hitBoxClearanceBottom);
}

int centerX(Entity* entity)
{
    return entity->x + trueWidth(entity) / 2;
}

int centerY(Entity* entity)
{
    return entity->y + trueHeight(entity) / 2;
}

int distanceBetweenEntities(Entity* e1, Entity* e2)
{
    int dx = centerX(e1) - centerX(e2);
    int dy = centerY(e1) - centerY(e2);
    return sqrt(dx * dx + dy * dy);
}

int visualLeftEdge(Entity* entity)
{
    return leftEdge(entity) - entity->hitBoxClearanceLeft;
}

int visualRightEdge(Entity* entity)
{
    return rightEdge(entity) + entity->hitBoxClearanceRight;
}

int visualTopEdge(Entity* entity)
{
    return topEdge(entity) - entity->hitBoxClearanceTop;
}

int visualBottomEdge(Entity* entity)
{
    return bottomEdge(entity) + entity->hitBoxClearanceBottom;
}

int getWorldBoundLeft(bool asPlayer)
{
    if(!asPlayer) return currentMap->worldBoundLeft;
    else return 0;
}

int getWorldBoundRight(bool asPlayer)
{
    if(!asPlayer) return currentMap->worldBoundRight;
    else return currentMap->worldBoundRight + currentMap->worldBoundLeft;
}

int getWorldBoundTop()
{
    return currentMap->worldBoundTop;
}

int getWorldBoundBottom()
{
    return currentMap->worldBoundBottom;
}

int leftMostValidPoint(Entity* entity)
{
    return getWorldBoundLeft(entity->entityType == PLAYER);
}

int rightMostValidPoint(Entity* entity)
{
    return getWorldBoundRight(entity->entityType == PLAYER) - trueWidth(entity);
}

int topMostValidPoint(Entity* entity)
{
    return getWorldBoundTop();
}

int bottomMostValidPoint(Entity* entity)
{
    return getWorldBoundBottom() - trueHeight(entity);
}

void warpIntoBounds(Entity* entity)
{
    int x = entity->x;
    int y = entity->y;

    if(leftEdge(entity) < getWorldBoundLeft(entity->entityType == PLAYER))
        x = leftMostValidPoint(entity);
    if(rightEdge(entity) >= getWorldBoundRight(entity->entityType == PLAYER))
        x = rightMostValidPoint(entity);
    if(topEdge(entity) < getWorldBoundTop())
        y = topMostValidPoint(entity);
    if(bottomEdge(entity) >= getWorldBoundBottom())
        y = bottomMostValidPoint(entity);

    teleportEntity(entity, x, y);
}

int ticksSinceEntityLanded(Entity* entity)
{
    return globals()->worldTick - entity->jumpLandedAtTick;
}

int isCollidingOnX(Entity* thisEntity, Entity* thatEntity)
{
    return isCollidingOnLine(leftEdge(thisEntity), rightEdge(thisEntity), leftEdge(thatEntity), rightEdge(thatEntity));
}

int isCollidingOnY(Entity* thisEntity, Entity* thatEntity)
{
    return isCollidingOnLine(topEdge(thisEntity), bottomEdge(thisEntity), topEdge(thatEntity), bottomEdge(thatEntity));
}

int isCollidingOnX(Entity* thisEntity, CollisionBox* collisionBox)
{
    return isCollidingOnLine(leftEdge(thisEntity), rightEdge(thisEntity), leftEdge(collisionBox), rightEdge(collisionBox));
}

int isCollidingOnY(Entity* thisEntity, CollisionBox* collisionBox)
{
    return isCollidingOnLine(topEdge(thisEntity), bottomEdge(thisEntity), topEdge(collisionBox), bottomEdge(collisionBox));
}

int isTouching(Entity* thisEntity, Entity* thatEntity)
{
    return (isCollidingOnLine(leftEdge(thisEntity) - 1, rightEdge(thisEntity) + 1, leftEdge(thatEntity) - 1, rightEdge(thatEntity) + 1) &&
            isCollidingOnLine(topEdge(thisEntity) - 1, bottomEdge(thisEntity) + 1, topEdge(thatEntity) - 1, bottomEdge(thatEntity) + 1));
}

int isRegionFreeOfStaticCollisions(int x, int y, int width, int height, EntityType entityType)
{
    CollisionBox region;
    region.x = x;
    region.y = y;
    region.width = width;
    region.height = height;

    if(leftEdge(&region) < getWorldBoundLeft(entityType == PLAYER) || rightEdge(&region) >= getWorldBoundRight(entityType == PLAYER) || topEdge(&region) < getWorldBoundTop() || bottomEdge(&region) >= getWorldBoundBottom())
        return false;

    for(int i = 0; i < currentMap->nStaticCollisionBoxes; i++)
    {
        CollisionBox* cb = currentMap->staticCollisionBoxes[i];

        if(entityType == SPELL && cb->layer <= 3)
            continue;

        if(isCollidingOnLine(leftEdge(&region), rightEdge(&region), leftEdge(cb), rightEdge(cb)) && isCollidingOnLine(topEdge(&region), bottomEdge(&region), topEdge(cb), bottomEdge(cb)))
        {
            return false;
        }
    }

    return true;
}

void detectAndResolveCollisions()
{
    int numberOfEntities = globals()->numberOfEntities;

    for(int i = 0; i < numberOfEntities; i++)
    {
        allEntities[i]->collisionShiftThisFrame = 0;
    }

    // Collision with other entities

    for(int i = 0; i < N_BUCKETS_X_AXIS; i++)
    {
        for(int j = 0; j < N_BUCKETS_Y_AXIS; j++)
        {
            positionBucketSizes[i][j] = 0;
        }
    }

    int spatialBucketWidth = globals()->worldWidth / (N_BUCKETS_X_AXIS - 2);
    int spatialBucketHeight = globals()->worldHeight / (N_BUCKETS_Y_AXIS - 2);

    for(int k = 0; k < numberOfEntities; k++)
    {
        Entity* ke = allEntities[k];

        if(ke->isEthereal || ke->jumpCurrentHeight >= 48) continue;

        int kx = ke->x;
        int ky = ke->y;

        int primaryBucketX = kx / spatialBucketWidth;
        int primaryBucketY = ky / spatialBucketHeight;

        if(primaryBucketX < 0) primaryBucketX = 0;
        if(primaryBucketY < 0) primaryBucketY = 0;

        positionBuckets[primaryBucketX][primaryBucketY][positionBucketSizes[primaryBucketX][primaryBucketY]] = k;
        positionBucketSizes[primaryBucketX][primaryBucketY] += 1;

        int entityLeft = leftEdge(ke);
        int entityRight = rightEdge(ke);
        int entityTop = topEdge(ke);
        int entityBottom = bottomEdge(ke);
        int edgeCaseX = 0;
        int edgeCaseY = 0;

        leftEdgesCache[k] = entityLeft;
        rightEdgesCache[k] = entityRight;
        topEdgesCache[k] = entityTop;
        bottomEdgesCache[k] = entityBottom;

        if(entityLeft <= primaryBucketX * spatialBucketWidth)
        {
            edgeCaseX = -1;
        }
        else if(entityRight >= primaryBucketX * (spatialBucketWidth + 1))
        {
            edgeCaseX = 1;
        }

        if(entityTop <= primaryBucketY * spatialBucketHeight)
        {
            edgeCaseY = -1;
        }
        else if(entityBottom >= primaryBucketY * (spatialBucketHeight + 1))
        {
            edgeCaseY = 1;
        }

        if(edgeCaseX || edgeCaseY)
        {
            int secondaryBucketX = primaryBucketX + edgeCaseX;
            int secondaryBucketY = primaryBucketY + edgeCaseY;

            if(edgeCaseX && secondaryBucketX >= 0)
            {
                positionBuckets[secondaryBucketX][primaryBucketY][positionBucketSizes[secondaryBucketX][primaryBucketY]] = k;
                positionBucketSizes[secondaryBucketX][primaryBucketY] += 1;
            }

            if(edgeCaseY && secondaryBucketY >= 0)
            {
                positionBuckets[primaryBucketX][secondaryBucketY][positionBucketSizes[primaryBucketX][secondaryBucketY]] = k;
                positionBucketSizes[primaryBucketX][secondaryBucketY] += 1;
            }

            if(edgeCaseY && edgeCaseX && secondaryBucketX >= 0 && secondaryBucketY >= 0)
            {
                positionBuckets[secondaryBucketX][secondaryBucketY][positionBucketSizes[secondaryBucketX][secondaryBucketY]] = k;
                positionBucketSizes[secondaryBucketX][secondaryBucketY] += 1;
            }
        }
    }

    for(int x = 0; x < N_BUCKETS_X_AXIS; x++)
    {
        for(int y = 0; y < N_BUCKETS_Y_AXIS; y++)
        {
            for(int z = 0; z < positionBucketSizes[x][y]; z++)
            {
                int thisEntityIndex = positionBuckets[x][y][z];

                int thisEntityLeft = leftEdgesCache[thisEntityIndex];
                int thisEntityRight = rightEdgesCache[thisEntityIndex];
                int thisEntityTop = topEdgesCache[thisEntityIndex];
                int thisEntityBottom = bottomEdgesCache[thisEntityIndex];

                for(int w = 0; w < positionBucketSizes[x][y]; w++)
                {
                    if(z == w)
                        continue;

                    int thatEntityIndex = positionBuckets[x][y][w];

                    int thatEntityLeft = leftEdgesCache[thatEntityIndex];
                    int thatEntityRight = rightEdgesCache[thatEntityIndex];
                    int thatEntityTop = topEdgesCache[thatEntityIndex];
                    int thatEntityBottom = bottomEdgesCache[thatEntityIndex];

                    if(isCollidingOnLine(thisEntityLeft, thisEntityRight, thatEntityLeft, thatEntityRight) && isCollidingOnLine(thisEntityTop, thisEntityBottom, thatEntityTop, thatEntityBottom))
                    {
                        Entity* i = allEntities[thisEntityIndex];
                        Entity* j = allEntities[thatEntityIndex];

                        if((i->isGhostly && j->entityType != SPELL) || (j->isGhostly && i->entityType != SPELL))
                            continue;

                        if(i->collisionShiftThisFrame < 1) resolveCollision(i, j);
                        if(j->collisionShiftThisFrame < 1) resolveCollision(j, i);

                        i->collisionShiftThisFrame++;
                        j->collisionShiftThisFrame++;
                    }
                }
            }
        }
    }

    // Static collisions
    for(int i = 0; i < numberOfEntities; i++)
    {
        Entity* e = allEntities[i];
        if(e->isEthereal || e->jumpCurrentHeight >= 48 || e->isGhostly)
            continue;

        for(int j = 0; j < currentMap->nStaticCollisionBoxes; j++)
        {
            CollisionBox* cb = currentMap->staticCollisionBoxes[j];

            if(e->entityType == SPELL && cb->layer <= 3)
                    continue;

            if(isCollidingOnX(e, cb) && isCollidingOnY(e, cb))
            {
                resolveCollisionWithHardBarrier(e, cb);
                resolveCollision(e, 0);
            }
        }
    }
}

void resolveCollision(Entity* primary, Entity* secondary)
{
    if(primary->collisionFunction != 0)
    {
        primary->collisionFunction(primary, secondary);
    }
    else
    {
        genericEntityCollision(primary, secondary);
    }
}

void resolveCollisionWithHardBarrier(Entity* entity, CollisionBox* collisionBox)
{
    if(entity->entityType == INANIMATE)
    {
        //PRINT_DEBUG("Logical conflict: INANIMATE object colliding with hard barrier.");
        //return;
    }

    int xViolation = isCollidingOnX(entity, collisionBox);
    int yViolation = isCollidingOnY(entity, collisionBox);

    if(xViolation && yViolation)
    {
        if(abs(xViolation) < abs(yViolation))
        {
            entity->x -= xViolation;
            entity->goalX -= xViolation;
        }
        else
        {
            entity->y -= yViolation;
            entity->goalY -= yViolation;
        }

        // Fix for the "stuck between wall and world bounds" bug
        if(topEdge(entity) < getWorldBoundTop() || bottomEdge(entity) > getWorldBoundBottom())
        {
            entity->y += yViolation * 2;
            entity->goalY += yViolation * 2;
        }
    }
}

void genericEntityCollision(Entity* thisEntity, Entity* thatEntity)
{
    if(thatEntity == 0)
        return;

    if(thatEntity->entityType == SPELL || thatEntity->entityType == COLLECTIBLE)
        return;

    if(thisEntity->isUnpushable) return;

    int xCollision = isCollidingOnX(thisEntity, thatEntity);
    int yCollision = isCollidingOnY(thisEntity, thatEntity);

    int dx = 0;
    int dy = 0;

    if(xCollision > 0)
        dx = -1;
    if(xCollision < 0)
        dx = 1;
    if(yCollision > 0)
        dy = -1;
    if(yCollision < 0)
        dy = 1;

    int destinationX = thisEntity->x + dx;
    int destinationY = thisEntity->y + dy;

    if(destinationX < getWorldBoundLeft(thisEntity->entityType == PLAYER))
        destinationX = leftMostValidPoint(thisEntity);
    if(destinationX + trueWidth(thisEntity) >= getWorldBoundRight(thisEntity->entityType == PLAYER))
        destinationX = rightMostValidPoint(thisEntity);
    if(destinationY < getWorldBoundTop())
        destinationY = topMostValidPoint(thisEntity);
    if(destinationY + trueHeight(thisEntity) >= getWorldBoundBottom())
        destinationY = bottomMostValidPoint(thisEntity);

    teleportEntity(thisEntity, destinationX, destinationY);

    setEntityMoveIntention(thisEntity, 0, 0, false);
    //cancelMovementPlan(thisEntity);

    if(!isEntityWaiting(thisEntity)) makeEntityWait(thisEntity, 5 + rand() % 5);
}

void ignoreEntityCollision(Entity* thisEntity, Entity* thatEntity)
{
    return;
}

void triggerDialog(Entity* owner, Entity* triggerer)
{
    dialogOwner = owner;

    makeEntityFaceEntity(owner, triggerer);

    if(owner->dialogPointer == 0)
        return;

    owner->dialogPointer = owner->dialogPointer->branches[getDialogCursor()]; // Step forward through dialog
    DialogLine* firstBranch = owner->dialogPointer->branches[0];

    if(firstBranch == 0)
    {
        // No branches set, means end of dialog
        endDialog();
    }
    else
    {
        std::string branchesToDisplay[4];
        globals()->isPlayerDialogLocked = false;
        for(int i = 0; i < 4; i++)
        {
            branchesToDisplay[i] = "";
            DialogLine* line = owner->dialogPointer->branches[i];
            if(line != 0)
            {
                branchesToDisplay[i] = line->content;
                if(line->isLocking)
                {
                    globals()->isPlayerDialogLocked = true;
                }
            }
        }

        Entity* speaker = 0;
        if(firstBranch->speaker == SPEAKER_ENTITY) speaker = owner;
        if(firstBranch->speaker == SPEAKER_PLAYER) speaker = globals()->player;

        displayDialog(branchesToDisplay, speaker);
    }
}

void endDialog()
{
    closeDialogDisplay();

    if(dialogOwner != 0)
    {
        dialogOwner->dialogPointer = dialogOwner->initialDialogPointer;
        dialogOwner = 0;
    }

    globals()->isPlayerDialogLocked = false;
}

Entity* findEntityInInteractRegion(Entity* triggerer)
{
    if(triggerer->facing < 1 || triggerer->facing > 4)
        return 0;

    const int regionLength = (int)(trueHeight(triggerer) * 1.5);

    int checkRegionLeft; int checkRegionRight; int checkRegionTop; int checkRegionBottom;

    if(triggerer->facing == 1 || triggerer->facing == 3)
    {
        checkRegionLeft = leftEdge(triggerer);
        checkRegionRight = rightEdge(triggerer);
    }
    else
    {
        checkRegionTop = topEdge(triggerer);
        checkRegionBottom = bottomEdge(triggerer);
    }

    if(triggerer->facing == 1)
    {
        checkRegionBottom = topEdge(triggerer);
        checkRegionTop = checkRegionBottom - regionLength;
    }
    if(triggerer->facing == 3)
    {
        checkRegionTop = bottomEdge(triggerer);
        checkRegionBottom = checkRegionTop + regionLength;
    }
    if(triggerer->facing == 2)
    {
        checkRegionRight = leftEdge(triggerer);
        checkRegionLeft = checkRegionRight - regionLength;
    }
    if(triggerer->facing == 4)
    {
        checkRegionLeft = rightEdge(triggerer);
        checkRegionRight = checkRegionLeft + regionLength;
    }

    Entity* closestEntity = 0;
    int closestDistance = 9999999;

    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* iEntity = allEntities[i];

        if(iEntity == triggerer || (iEntity->entityType != NPC && iEntity->entityType != ENEMY && iEntity->entityType != PLAYER) || iEntity->isEthereal || iEntity->jumpCurrentHeight >= 48)
            continue;

        if(leftEdge(iEntity) > checkRegionRight || rightEdge(iEntity) < checkRegionLeft || topEdge(iEntity) > checkRegionBottom || bottomEdge(iEntity) < checkRegionTop)
            continue;

        int dx = abs(iEntity->x - triggerer->x);
        int dy = abs(iEntity->y - triggerer->y);

        int sqd = dx * dx + dy * dy;

        if(sqd < closestDistance)
        {
            closestDistance = sqd;
            closestEntity = iEntity;
        }
    }

    return closestEntity;
}

Entity* findClosest(Entity* e, Entity* ignore[], int numberOfIgnoredEntities, bool (*entityIgnoreCheck) (Entity*))
{
    Entity* closestEntity = 0;
    int closestDistance = 9999999;

    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* iEntity = allEntities[i];
        bool ign = false;

        for(int j = 0; j < numberOfIgnoredEntities; j++)
        {
            if(iEntity == ignore[j])
                ign = true;
        }

        if(iEntity == e || ign || (iEntity->entityType != NPC && iEntity->entityType != ENEMY && iEntity->entityType != PLAYER) || iEntity->isEthereal || iEntity->jumpCurrentHeight >= 48)
            continue;

        if(entityIgnoreCheck && entityIgnoreCheck(iEntity))
            continue;

        int dx = abs(iEntity->x - e->x);
        int dy = abs(iEntity->y - e->y);

        int sqd = dx * dx + dy * dy;

        if(sqd < closestDistance)
        {
            closestDistance = sqd;
            closestEntity = iEntity;
        }
    }

    return closestEntity;
}

void entityApproachOrAvoid(Entity* actor, Entity* actee, bool approach)
{
    makeEntityFaceEntity(actor, actee, approach);

    int d = actor->spriteWidth;
    if(actor->facing == 1 || actor->facing == 3) d = actor->spriteHeight;

    setEntityMoveIntentionGrid(actor, actor->facing, d);
}

void makeEntityFaceEntity(Entity* actor, Entity* actee, bool approach)
{
    int dx = actee->x - actor->x;
    int dy = actee->y - actor->y;

    if(!approach)
    {
        dx = -dx;
        dy = -dy;
    }

    int dir = 0;

    if(abs(dx) > abs(dy))
    {
        if(dx > 0)
            dir = 4;
        else
            dir = 2;
    }
    else
    {
        if(dy > 0)
            dir = 3;
        else
            dir = 1;
    }

    actor->facing = dir;
}

void entityTakeRandomWalkStep(Entity* entity)
{
    int dir;
    bool validRegionFound = false;
    int panic = 0;

    while(!validRegionFound)
    {
        dir = rand() % 4 + 1;

        int checkX = leftEdge(entity);
        int checkY = topEdge(entity);
        int checkWidth = rightEdge(entity) - checkX;
        int checkHeight = bottomEdge(entity) - checkY;

        if(dir == 1) checkY -= trueHeight(entity);
        if(dir == 2) checkX -= trueWidth(entity);
        if(dir == 3) checkY += trueHeight(entity);
        if(dir == 4) checkX += trueWidth(entity);

        validRegionFound = isRegionFreeOfStaticCollisions(checkX, checkY, checkWidth, checkHeight, entity->entityType);

        panic++;
        if(panic > 10)
        {
            validRegionFound = true;
        }
    }

    int distance;
    if(dir == 1 || dir == 3) distance = trueHeight(entity);
    if(dir == 2 || dir == 4) distance = trueWidth(entity);
    setEntityMoveIntentionGrid(entity, dir, distance, true);
}

bool isEntityHasMovementPlan(Entity* entity)
{
    return entity->movementPlanLength > 0;
}

void cancelMovementPlan(Entity* entity)
{
    entity->movementPlanIndex = 0;
    entity->movementPlanLength = 0;
}

void makeMovementPlanToPoint(Entity* entity, int goalX, int goalY)
{
    cancelMovementPlan(entity);
    entity->movementPlanLength = planPath(
        roundedGridX(entity->x, entity->pathfindingClearance),
        roundedGridY(entity->y, entity->pathfindingClearance),
        roundedGridX(goalX, entity->pathfindingClearance),
        roundedGridY(goalY, entity->pathfindingClearance),
        entity->movementPlan,
        entity->pathfindingClearance
    );
}

void doNextStepInMovementPlan(Entity* entity)
{
    setEntityMoveIntentionGrid(entity, entity->movementPlan[entity->movementPlanIndex], currentMap->tileWidth * entity->pathfindingClearance, true, true);
    entity->movementPlanIndex++;

    if (entity->movementPlanIndex >= entity->movementPlanLength)
    {
        cancelMovementPlan(entity);
    }
}

int roundedGridX(int pointX, int pathClearance)
{
    return ((int)((float)pointX / (float)currentMap->tileWidth + 0.5) / pathClearance) * pathClearance;
}

int roundedGridY(int pointY, int pathClearance)
{
    return ((int)((float)pointY / (float)currentMap->tileWidth + 0.5) / pathClearance) * pathClearance;
}

void makeEntityShake(Entity* entity, int duration, int magnitude)
{
    entity->shakeTicksRemaining = duration;
    entity->shakeMagnitude = magnitude;
}

void makeEntityFlash(Entity* entity, int duration, sf::Color flashColor, sf::Color flashDecay)
{
    entity->flashTicksRemaining = duration;
    entity->flashColor = flashColor;
    entity->flashDecay = flashDecay;
}

void animateEntity(Entity* entity, TextureId animationId, int ticksPerFrame, bool shouldAnimateAtRest)
{
    if(globals()->numberOfEntities > MAX_ENTITIES)
    {
        PRINT_DEBUG("Warning: Entity limit reached. Program will crash.");
    }

    Entity* e = entity;

    sf::Texture* t = retrieveTexture(animationId);
    e->sprite->setTexture(*t);

    int numFrames = retrieveNumberOfFrames(animationId);

    e->shouldAnimateAtRest = shouldAnimateAtRest;
    for(int i = 0; i < numFrames; i++)
    {
        e->animationPattern[i] = i;
    }

    e->spriteWidth = t->getSize().x / numFrames;
    e->spriteHeight = t->getSize().y;

    e->ticksPerFrame = ticksPerFrame;
    e->animationPatternIndex = 0;
    e->animationPatternLength = numFrames;
    e->shouldNotBeRendered = false;
    e->frameRow = 0;
    e->animationTick = 0;
    e->isAnimated = true;
    e->isAnimationSingleRow = true;
}

void createAnimationAtPoint(TextureId animationId, int ticksPerFrame, int x, int y, int layer)
{
    if(globals()->numberOfEntities > MAX_ENTITIES)
    {
        PRINT_DEBUG("Warning: Entity limit reached. Program will crash.");
    }

    sf::Texture* t = retrieveTexture(animationId);
    int numFrames = retrieveNumberOfFrames(animationId);
    int width = t->getSize().x / numFrames;
    int height = t->getSize().y;

    Entity* e = newEntity(INANIMATE, x - width / 2, y - height / 2, 0, 0, layer);

    e->isEthereal = true;
    e->moveSpeed = 0;
    e->dieOnAnimationComplete = true;

    animateEntity(e, animationId, ticksPerFrame);
    addEntity(e);
}

void createAnimationOnEntity(Entity* entity, TextureId animationId, int ticksPerFrame, int offsetX, int offsetY, int layer)
{
    Entity* e = newEntity(INANIMATE, 0, 0, 0, 0, layer);

    e->isEthereal = true;
    e->layer = layer;
    e->moveSpeed = 0;
    e->dieOnAnimationComplete = true;

    e->animationBoundToEntity = entity;
    e->attrs[ATTR_MEMORY_1] = offsetX;
    e->attrs[ATTR_MEMORY_2] = offsetY;

    animateEntity(e, animationId, ticksPerFrame);
    addEntity(e);
}

void makeScreenFlash(sf::Color color, int duration)
{
    screenFlashColor = color;
    screenFlashAlphaChangePerTick = color.a / duration;
}

void playSoundAtEntityLocation(SoundId soundId, Entity* entity)
{
    if(entity == 0)
        entity = globals()->player;

    int soundDistance = std::sqrt((globals()->player->x - entity->x) * (globals()->player->x - entity->x) + (globals()->player->y - entity->y) * (globals()->player->y - entity->y));

    int wholeMapPixelWidth = currentMap->nTilesX * currentMap->tileWidth;
    int wholeMapPixelHeight = currentMap->nTilesY * currentMap->tileWidth;
    int wholeMapDiagonalDistance = std::sqrt(wholeMapPixelWidth * wholeMapPixelWidth + wholeMapPixelHeight * wholeMapPixelHeight);

    float volumeAdjustment = 1.0 - ((float)soundDistance / (float)wholeMapDiagonalDistance) * 1.5;
    if(volumeAdjustment < 0.1)
        volumeAdjustment = 0.1;

    playSound(soundId, volumeAdjustment);
}

void addFloatingText(int x, int y, int val, const sf::Color color, bool isDialog)
{
    FloatingText* dt = new FloatingText();
    dt->x = x - 2;
    dt->y = y;
    dt->color.r = color.r;
    dt->color.g = color.g;
    dt->color.b = color.b;
    dt->isDialog = isDialog;

    dt->val = val;

    allFloatingTexts[numberOfFloatingTexts] = dt;
    numberOfFloatingTexts++;
}

void stepFloatingTexts()
{
    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* e = allEntities[i];

        int debounceWindow = 3;
        if(e == globals()->player)
            debounceWindow = 0;

        if(e->accumulatedDisplayDamage && globals()->worldTick - e->accumulatedDisplayDamageDebounce >= debounceWindow)
        {
           addFloatingText(centerX(e), e->y - 12, e->accumulatedDisplayDamage, sf::Color::Red);
           e->accumulatedDisplayDamage = 0;
           e->accumulatedDisplayDamageDebounce = globals()->worldTick;
        }
    }

    for(int i = 0; i < numberOfFloatingTexts; i++)
    {
        FloatingText* dt = allFloatingTexts[i];
        dt->lifeTicker += 1;

        if(dt->lifeTicker % 3 == 0)
        {
            dt->y -= 1;
            dt->opacity -= 15;
        }

        if(dt->opacity <= 0)
        {
            dt->markedForDestruction = true;
        }
    }

    for(int i = 0; i < numberOfFloatingTexts; i++)
    {
        FloatingText* it = allFloatingTexts[i];
        if(it->markedForDestruction)
        {
            delete(it);
            for(int j = i + 1; j < numberOfFloatingTexts; j++)
            {
                allFloatingTexts[i] = allFloatingTexts[j];
            }
            numberOfFloatingTexts--;
        }
    }
}

void renderFloatingTexts(sf::RenderWindow* window)
{
    for(int i = 0; i < numberOfFloatingTexts; i++)
    {
        FloatingText* dt = allFloatingTexts[i];
        drawText(window, dt->x - globals()->viewPortX, dt->y - globals()->viewPortY, toScientificNotation(dt->val, 0), REGULAR_FONT, sf::Color(dt->color.r, dt->color.g, dt->color.b, dt->opacity));
    }
}

void destroyAllFloatingTexts()
{
    for(int i = 0; i < numberOfFloatingTexts; i++)
    {
        if(allFloatingTexts[i] != 0)
        {
            delete allFloatingTexts[i];
            allFloatingTexts[i] = 0;
        }
    }

    numberOfFloatingTexts = 0;
}

void setupMap(GameMap* newMap, SpawnMap* spawnMap)
{
    destroyAllEntities();
    destroyAllParticles();
    destroyAllFloatingTexts();
    setWeather(WEATHER_NO_WEATHER);

    currentMap = newMap;
    currentSpawnMap = spawnMap;

    setupTiledEntitiesOnMap(newMap);
    setupMapUniqueConditions(newMap);

    globals()->worldWidth = newMap->nTilesX * newMap->tileWidth;
    globals()->worldHeight = newMap->nTilesY * newMap->tileWidth;

    for(int i = 1; i <= 8; i++)
    {
        sf::RenderTexture* r = supplementaryRenderTextures[i];
        sf::Sprite* s = supplementaryRenderSprites[i];

        if(s != 0) delete s;
        if(r != 0) delete r;

        r = new sf::RenderTexture();
        s = new sf::Sprite();

        r->create(globals()->worldWidth, globals()->worldHeight);
        s->setTexture(r->getTexture());

        supplementaryRenderTextures[i] = r;
        supplementaryRenderSprites[i] = s;
    }

    for(int i = 0; i < 0; i++)
    {
        randomEnemyAdd(rand() % (getWorldBoundRight() - 128) + 64, (rand() % (getWorldBoundBottom() - getWorldBoundTop() - 32)) + getWorldBoundTop() + 16);
    }
}

void setupTiledEntitiesOnMap(GameMap* gameMap)
{
    for(int i = 0; i < gameMap->nEntityPlaceholders; i++)
    {
        EntityPlaceholder* e = gameMap->entityPlaceholders[i];

        int tile = e->tileId;
        int x = e->x;
        int y = e->y;

        if(tile == PLAYER_BATTLER_TILE || tile == PLAYER_EXPLORER_TILE) playerAdd(x, y);

        if(tile == CRATE_TILE) crateAdd(x, y);
        if(tile == ROCK_TILE) rockAdd(x, y);
        if(tile == LANTERN_TILE) lanternAdd(x, y);

        if(tile == SHAMBLER_TILE) shamblerAdd(x, y);
        if(tile == SPRINTER_TILE) sprinterAdd(x, y);
        if(tile == BRUTE_TILE) bruteAdd(x, y);
        if(tile == WIZARD_TILE) wizardAdd(x, y);
        if(tile == GHOST_TILE) ghostAdd(x, y);
        if(tile == SKELETON_MAGE_TILE) skeletonMageAdd(x, y);

        if(tile == NPC_GENERIC_GUY_TILE) wanderingNpcAdd(x, y, e->mapEntityId);
    }
}

void hotReloadCurrentMap()
{
    if(isMapDirty(currentMap))
    {
        PRINT_DEBUG("Hot reload this map.\n");
        preloadMap(currentMap->filename, currentMap->lookupName);
        setupMap(retrieveGameMap(currentMap->lookupName), retrieveSpawnMap(currentSpawnMap->lookupName));
    }

    return;
}

GameMap* getCurrentMap()
{
    return currentMap;
}

void setupMapUniqueConditions(GameMap* newMap)
{
    if(newMap == 0 || newMap->lookupName == "")
        return;

    if(newMap->lookupName == "SLID_BOSS_BATTLE_MAP")
    {
        setWeather(WEATHER_STORM, 8);
    }
}

std::string getLevelNiceName()
{
    if(currentMap->lookupName == "SLID_BOSS_BATTLE_MAP") return "Slid, Whose Soul is By The Sea";

    if(currentSpawnMap == 0)
        return "";

    std::string name = "";

    if(currentSpawnMap->lookupName == "ARMY_OF_MUNG_SPAWN_MAP")
    {
        name += "The Army of Mung";

        if(currentArmyStage == STAGE_SCOUTS) name += ": His Scouts";
        if(currentArmyStage == STAGE_VANGUARD) name += ": His Vanguard";
        if(currentArmyStage == STAGE_HORDE) name += ": His Horde";
        if(currentArmyStage == STAGE_ELITES) name += ": His Damned";
        if(currentArmyStage == STAGE_BOSS) name = "Mung, Lord of All Deaths Between Pegana and the Rim";
    }

    return name;
}

std::string getArmyStageModifierDescription()
{
    if(currentSpawnMap == 0)
        return "";

    std::string description = "";

    if(currentSpawnMap->lookupName == "ARMY_OF_MUNG_SPAWN_MAP")
    {
        if(currentArmyStage == STAGE_SCOUTS) description += "1x strength, 1x quantity";
        if(currentArmyStage == STAGE_VANGUARD) description += "2x strength, 1x quantity";
        if(currentArmyStage == STAGE_HORDE) description += "1x strength, 3x quantity";
        if(currentArmyStage == STAGE_ELITES) description += "4x strength, 0.5x quantity";
    }

    return description;
}

void matchWeatherToArmyStage()
{
    if(currentMap->lookupName == "STORM_COAST")
    {
        if(currentArmyStage == STAGE_SCOUTS) setWeather(WEATHER_RAIN, 3);
        if(currentArmyStage == STAGE_VANGUARD) setWeather(WEATHER_RAIN, 7);
        if(currentArmyStage == STAGE_HORDE) setWeather(WEATHER_STORM, 3);
        if(currentArmyStage == STAGE_ELITES) setWeather(WEATHER_STORM, 6);
        if(currentArmyStage == STAGE_BOSS) setWeather(WEATHER_STORM, 10);
    }
}

int adjustStrengthForArmyStage(int strength)
{
    if(currentArmyStage == STAGE_VANGUARD) return strength * 2;
    if(currentArmyStage == STAGE_ELITES) return strength * 4;

    return strength;
}

void clearMovementPlanCache()
{
    memset(movementPlanCache, 0, sizeof(movementPlanCache));
}

int retrieveCachedMovementPlan(int startX, int startY, int goalX, int goalY, int* outCardinalDirectionPlan, int pathClearance)
{
    CompressedMovementPlan* plan = &(movementPlanCache[pathClearance - 1][startX][startY][goalX][goalY]);
    int movementPlanLength = plan->byte_one >> 4;

    if(movementPlanLength == 0) return 0;

    if(movementPlanLength >= 1) outCardinalDirectionPlan[0] = ((unsigned char)(plan->byte_one << 4) >> 6) + 1;
    if(movementPlanLength >= 2) outCardinalDirectionPlan[1] = ((unsigned char)(plan->byte_one << 6) >> 6) + 1;

    if(movementPlanLength >= 3) outCardinalDirectionPlan[2] = ((unsigned char)(plan->byte_two << 0) >> 6) + 1;
    if(movementPlanLength >= 4) outCardinalDirectionPlan[3] = ((unsigned char)(plan->byte_two << 2) >> 6) + 1;
    if(movementPlanLength >= 5) outCardinalDirectionPlan[4] = ((unsigned char)(plan->byte_two << 4) >> 6) + 1;
    if(movementPlanLength >= 6) outCardinalDirectionPlan[5] = ((unsigned char)(plan->byte_two << 6) >> 6) + 1;

    if(movementPlanLength >= 7) outCardinalDirectionPlan[6] = ((unsigned char)(plan->byte_three << 0) >> 6) + 1;
    if(movementPlanLength >= 8) outCardinalDirectionPlan[7] = ((unsigned char)(plan->byte_three << 2) >> 6) + 1;
    if(movementPlanLength >= 9) outCardinalDirectionPlan[8] = ((unsigned char)(plan->byte_three << 4) >> 6) + 1;
    if(movementPlanLength >= 10) outCardinalDirectionPlan[9] = ((unsigned char)(plan->byte_three << 6) >> 6) + 1;

    return movementPlanLength;
}

void storeMovementPlanToCache(uint8_t* movementPlanArray, int movementPlanLength, int startX, int startY, int goalX, int goalY, int pathClearance)
{
    CompressedMovementPlan* plan = &(movementPlanCache[pathClearance - 1][startX][startY][goalX][goalY]);

    int byteOne = 0;
    byteOne += movementPlanLength << 4;
    if(movementPlanLength >= 1) byteOne += (movementPlanArray[0] - 1) << 2;
    if(movementPlanLength >= 2) byteOne += (movementPlanArray[1] - 1) << 0;
    plan->byte_one = byteOne;

    int byteTwo = 0;
    if(movementPlanLength >= 3) byteTwo += (movementPlanArray[2] - 1) << 6;
    if(movementPlanLength >= 4) byteTwo += (movementPlanArray[3] - 1) << 4;
    if(movementPlanLength >= 5) byteTwo += (movementPlanArray[4] - 1) << 2;
    if(movementPlanLength >= 6) byteTwo += (movementPlanArray[5] - 1) << 0;
    plan->byte_two = byteTwo;

    int byteThree = 0;
    if(movementPlanLength >= 7) byteThree += (movementPlanArray[6] - 1) << 6;
    if(movementPlanLength >= 8) byteThree += (movementPlanArray[7] - 1) << 4;
    if(movementPlanLength >= 9) byteThree += (movementPlanArray[8] - 1) << 2;
    if(movementPlanLength >= 10) byteThree += (movementPlanArray[9] - 1) << 0;
    plan->byte_three = byteThree;
}

int planPath(int startGridX, int startGridY, int goalGridX, int goalGridY, uint8_t* outCardinalDirectionPlan, int pathClearance)
{
    int cachedMovementPlan[10] = {0};
    int cachedMovementPlanLength = retrieveCachedMovementPlan(startGridX, startGridY, goalGridX, goalGridY, cachedMovementPlan, pathClearance);

    if(cachedMovementPlanLength > 0)
    {
        for(int i = 0; i < 10; i++) outCardinalDirectionPlan[i] = cachedMovementPlan[i];
        return cachedMovementPlanLength;
    }

    // A* algorithm
    // TODO: rewrite at some point

    std::priority_queue<GridNode*, std::vector<GridNode*>, CompareGridNodes> openSet;
    std::unordered_set<GridNode*> openSetAsUnorderedSet;
    std::unordered_set<GridNode*> openSetGhostEntries;
    std::unordered_set<GridNode*> closedSet;

    // Reset pathfinding data on all nodes
    for(int i = 0; i < currentMap->nPathfindingGridNodes[pathClearance - 1]; i++)
    {
        GridNode* gn = currentMap->pathfindingGridNodes[pathClearance - 1][i];
        gn->computedG = 0;
        gn->computedF = 0;
        gn->directionFromParent = 0;
        gn->parentNode = 0;
    }

    // Find start node
    GridNode* startNode = 0;
    for(int i = 0; i < currentMap->nPathfindingGridNodes[pathClearance - 1]; i++)
    {
        GridNode* gn = currentMap->pathfindingGridNodes[pathClearance - 1][i];
        if(gn->x == startGridX && gn->y == startGridY)
        {
            startNode = gn;
            break;
        }
    }
    if(!startNode)
    {
        //PRINT_DEBUG(" Note: Start grid node for path not found in map grid nodes. \n");
        return 0;
    }

    // Find goal node
    GridNode* goalNode = 0;
    for(int i = 0; i < currentMap->nPathfindingGridNodes[pathClearance - 1]; i++)
    {
        GridNode* gn = currentMap->pathfindingGridNodes[pathClearance - 1][i];
        if(gn->x == goalGridX && gn->y == goalGridY)
        {
            goalNode = gn;
            break;
        }
    }
    if(!goalNode)
    {
        //PRINT_DEBUG(" Note: Goal grid node for path not found in map grid nodes. \n");
        return 0;
    }

    if(startNode == goalNode)
    {
        //PRINT_DEBUG(" Note: Start node and goal node are the same. \n");
        return 0;
    }

    //PRINT_DEBUG("START PATHFINDING for " << startNode->x << "," << startNode->y << " -> " << goalNode->x << "," << goalNode->y << "\n");

    // A* start proper
    GridNode* currentNode = startNode;
    openSet.push(startNode); openSetAsUnorderedSet.insert(startNode);

    while(openSet.top() != goalNode && !openSet.empty())
    {
        currentNode = openSet.top(); openSet.pop(); openSetAsUnorderedSet.erase(currentNode);
        if(openSetGhostEntries.find(currentNode) != openSetGhostEntries.end())
        {
            openSetGhostEntries.erase(currentNode);
            continue;   // skip it, because we would have removed this from openSet if we could have at the time
        }

        closedSet.insert(currentNode);

        for(int i = 1; i <= 4; i++)
        {
            GridNode* neighbour = currentNode->adjacentNodes[i];
            if(!neighbour) continue;

            int cost = currentNode->computedG + 1;

            if(cost < neighbour->computedG)
            {
                bool isNeighbourInOpenSet = openSetAsUnorderedSet.find(neighbour) != openSetAsUnorderedSet.end() && openSetGhostEntries.find(neighbour) == openSetGhostEntries.end();
                bool isNeighbourInClosedSet = closedSet.find(neighbour) != closedSet.end();

                if(isNeighbourInOpenSet)
                {
                    openSetGhostEntries.insert(neighbour);  // See further up, this is meant to be "removing" the item from openSet without actually removing
                }

                if(isNeighbourInClosedSet)
                {
                    closedSet.erase(neighbour);
                }
            }

            bool isNeighbourInOpenSet = openSetAsUnorderedSet.find(neighbour) != openSetAsUnorderedSet.end() && openSetGhostEntries.find(neighbour) == openSetGhostEntries.end();
            bool isNeighbourInClosedSet = closedSet.find(neighbour) != closedSet.end();
            if(!isNeighbourInOpenSet && !isNeighbourInClosedSet)
            {
                neighbour->computedG = cost;
                int computedH = abs(neighbour->x - goalNode->x) + abs(neighbour->y - goalNode->y);
                neighbour->computedF = neighbour->computedG + computedH;
                neighbour->parentNode = currentNode;
                neighbour->directionFromParent = i;
                openSet.push(neighbour); openSetAsUnorderedSet.insert(neighbour);
                openSetGhostEntries.erase(currentNode); // Remove the node from ghost entries, it shouldn't be flagged for delete any more.
            }
        }
    }

    if(openSet.top() != goalNode)
    {
        PRINT_DEBUG(" Note: Did not find path to goal before openSet became empty. \n");
        return 0; // didn't find a path to goal before openSet became empty
    }

    currentNode = goalNode;
    int stepsCount = 0;
    for(GridNode* pathCountNode = goalNode; pathCountNode->parentNode != 0; pathCountNode = pathCountNode->parentNode)
    {
        stepsCount++;
        if(stepsCount > 100)
        {
            std::string nicePath = "";
            int p = 0;
            for(GridNode* pathCountNode = goalNode; p < 100; pathCountNode = pathCountNode->parentNode)
            {
                nicePath += "(";
                nicePath += std::to_string(pathCountNode->x);
                nicePath += ",";
                nicePath += std::to_string(pathCountNode->y);
                nicePath += ")";
                p++;
            }
            PRINT_DEBUG("\n\n\n" << nicePath << "\n\n\n");
            PRINT_DEBUG("");

            PRINT_DEBUG(" ERROR: Exit pathfinding because loop limit exceeded. \n");
            return 0;
        }
    }

    for(int i = 0; i < stepsCount; i++)
    {
        int r = stepsCount - 1 - i;
        if(r < MAX_MOVEMENT_PLAN_LENGTH)
            outCardinalDirectionPlan[r] = currentNode->directionFromParent;

        if(currentNode->parentNode == 0)
        {
            PRINT_DEBUG(" ERROR: Null pointer encountered while backtracing through parents. \n");
        }

        currentNode = currentNode->parentNode;
    }

    //PRINT_DEBUG("PATH FOUND for " << startNode->x << "," << startNode->y << " -> " << goalNode->x << "," << goalNode->y << " with " << stepsCount << " steps." << "\n");

    storeMovementPlanToCache(outCardinalDirectionPlan, std::min(stepsCount, MAX_MOVEMENT_PLAN_LENGTH), startGridX, startGridY, goalGridX, goalGridY, pathClearance);

    return std::min(stepsCount, MAX_MOVEMENT_PLAN_LENGTH);
}

Entity* getEntityByGuid(unsigned long int guid)
{
    int numberOfEntities = globals()->numberOfEntities;
    for(int i = 0; i < numberOfEntities; i++)
    {
        if(allEntities[i]->guid == guid)
        {
            if(allEntities[i]->markedForDestruction)
                return 0;
            else
                return allEntities[i];
        }
    }

    return 0;
}

void printEntitySize()
{
    Entity* e = newEntity(ENEMY, 0, 0, 24, 1, 5, TEXTURE_SHAMBLER_SPRITESHEET, shamblerAi, shamblerCollision);

    PRINT_DEBUG(VAR_NAME(e->guid) << ": " << sizeof(e->guid));

    PRINT_DEBUG(VAR_NAME(e->lifeTimer) << ": " << sizeof(e->lifeTimer));
    PRINT_DEBUG(VAR_NAME(e->waitUntil) << ": " << sizeof(e->waitUntil));

    PRINT_DEBUG(VAR_NAME(e->markedForDestruction) << ": " << sizeof(e->markedForDestruction));

    PRINT_DEBUG(VAR_NAME(e->sprite) << ": " << sizeof(e->sprite));

    PRINT_DEBUG(VAR_NAME(e->flashColor) << ": " << sizeof(e->flashColor));
    PRINT_DEBUG(VAR_NAME(e->flashDecay) << ": " << sizeof(e->flashDecay));
    PRINT_DEBUG(VAR_NAME(e->flashSprite) << ": " << sizeof(e->flashSprite));
    PRINT_DEBUG(VAR_NAME(e->flashTicksRemaining) << ": " << sizeof(e->flashTicksRemaining));

    PRINT_DEBUG(VAR_NAME(e->shouldNotBeRendered) << ": " << sizeof(e->shouldNotBeRendered));

    PRINT_DEBUG(VAR_NAME(e->entityType) << ": " << sizeof(e->entityType));
    PRINT_DEBUG(VAR_NAME(e->entitySubType) << ": " << sizeof(e->entitySubType));
    PRINT_DEBUG(VAR_NAME(e->attrs) << ": " << sizeof(e->attrs));
    PRINT_DEBUG(VAR_NAME(e->relatedEntities) << ": " << sizeof(e->relatedEntities));
    PRINT_DEBUG(VAR_NAME(e->aiFunction) << ": " << sizeof(e->aiFunction));
    PRINT_DEBUG(VAR_NAME(e->collisionFunction) << ": " << sizeof(e->collisionFunction));
    PRINT_DEBUG(VAR_NAME(e->cleanupFunction) << ": " << sizeof(e->cleanupFunction));

    PRINT_DEBUG(VAR_NAME(e->isEthereal) << ": " << sizeof(e->isEthereal));

    PRINT_DEBUG(VAR_NAME(e->immobilizedUntil) << ": " << sizeof(e->immobilizedUntil));

    PRINT_DEBUG(VAR_NAME(e->spriteWidth) << ": " << sizeof(e->spriteWidth));
    PRINT_DEBUG(VAR_NAME(e->spriteHeight) << ": " << sizeof(e->spriteHeight));
    PRINT_DEBUG(VAR_NAME(e->hitBoxClearanceLeft) << ": " << sizeof(e->hitBoxClearanceLeft));
    PRINT_DEBUG(VAR_NAME(e->hitBoxClearanceTop) << ": " << sizeof(e->hitBoxClearanceTop));
    PRINT_DEBUG(VAR_NAME(e->hitBoxClearanceRight) << ": " << sizeof(e->hitBoxClearanceRight));
    PRINT_DEBUG(VAR_NAME(e->hitBoxClearanceBottom) << ": " << sizeof(e->hitBoxClearanceBottom));
    PRINT_DEBUG(VAR_NAME(e->pathfindingClearance) << ": " << sizeof(e->pathfindingClearance));

    PRINT_DEBUG(VAR_NAME(e->frameRow) << ": " << sizeof(e->frameRow));
    PRINT_DEBUG(VAR_NAME(e->frameColumn) << ": " << sizeof(e->frameColumn));
    PRINT_DEBUG(VAR_NAME(e->animationPattern) << ": " << sizeof(e->animationPattern));
    PRINT_DEBUG(VAR_NAME(e->animationPatternIndex) << ": " << sizeof(e->animationPatternIndex));
    PRINT_DEBUG(VAR_NAME(e->animationPatternLength) << ": " << sizeof(e->animationPatternLength));
    PRINT_DEBUG(VAR_NAME(e->ticksPerFrame) << ": " << sizeof(e->ticksPerFrame));
    PRINT_DEBUG(VAR_NAME(e->animationTick) << ": " << sizeof(e->animationTick));
    PRINT_DEBUG(VAR_NAME(e->dieOnAnimationComplete) << ": " << sizeof(e->dieOnAnimationComplete));

    PRINT_DEBUG(VAR_NAME(e->animationBoundToEntity) << ": " << sizeof(e->animationBoundToEntity));

    PRINT_DEBUG(VAR_NAME(e->isAnimated) << ": " << sizeof(e->isAnimated));
    PRINT_DEBUG(VAR_NAME(e->isAnimationPaused) << ": " << sizeof(e->isAnimationPaused));
    PRINT_DEBUG(VAR_NAME(e->shouldAnimateAtRest) << ": " << sizeof(e->shouldAnimateAtRest));
    PRINT_DEBUG(VAR_NAME(e->isAnimationSingleRow) << ": " << sizeof(e->isAnimationSingleRow));

    PRINT_DEBUG(VAR_NAME(e->facing) << ": " << sizeof(e->facing));

    PRINT_DEBUG(VAR_NAME(e->layer) << ": " << sizeof(e->layer));

    PRINT_DEBUG(VAR_NAME(e->hp) << ": " << sizeof(e->hp));
    PRINT_DEBUG(VAR_NAME(e->displayHp) << ": " << sizeof(e->displayHp));
    PRINT_DEBUG(VAR_NAME(e->maxHp) << ": " << sizeof(e->maxHp));

    PRINT_DEBUG(VAR_NAME(e->accumulatedDisplayDamage) << ": " << sizeof(e->accumulatedDisplayDamage));
    PRINT_DEBUG(VAR_NAME(e->accumulatedDisplayDamageDebounce) << ": " << sizeof(e->accumulatedDisplayDamageDebounce));

    PRINT_DEBUG(VAR_NAME(e->x) << ": " << sizeof(e->x));
    PRINT_DEBUG(VAR_NAME(e->y) << ": " << sizeof(e->y));
    PRINT_DEBUG(VAR_NAME(e->goalX) << ": " << sizeof(e->goalX));
    PRINT_DEBUG(VAR_NAME(e->goalY) << ": " << sizeof(e->goalY));
    PRINT_DEBUG(VAR_NAME(e->moveSpeed) << ": " << sizeof(e->moveSpeed));
    PRINT_DEBUG(VAR_NAME(e->didMoveThisFrame) << ": " << sizeof(e->didMoveThisFrame));

    PRINT_DEBUG(VAR_NAME(e->weight) << ": " << sizeof(e->weight));

    PRINT_DEBUG(VAR_NAME(e->movementPlan) << ": " << sizeof(e->movementPlan));
    PRINT_DEBUG(VAR_NAME(e->movementPlanIndex) << ": " << sizeof(e->movementPlanIndex));
    PRINT_DEBUG(VAR_NAME(e->movementPlanLength) << ": " << sizeof(e->movementPlanLength));

    PRINT_DEBUG(VAR_NAME(e->jumpDuration) << ": " << sizeof(e->jumpDuration));
    PRINT_DEBUG(VAR_NAME(e->isJumping) << ": " << sizeof(e->isJumping));
    PRINT_DEBUG(VAR_NAME(e->jumpMaxHeight) << ": " << sizeof(e->jumpMaxHeight));
    PRINT_DEBUG(VAR_NAME(e->jumpCurrentHeight) << ": " << sizeof(e->jumpCurrentHeight));
    PRINT_DEBUG(VAR_NAME(e->jumpTick) << ": " << sizeof(e->jumpTick));
    PRINT_DEBUG(VAR_NAME(e->jumpSpeed) << ": " << sizeof(e->jumpSpeed));
    PRINT_DEBUG(VAR_NAME(e->jumpLandedAtTick) << ": " << sizeof(e->jumpLandedAtTick));
    PRINT_DEBUG(VAR_NAME(e->jumpLandSoundId) << ": " << sizeof(e->jumpLandSoundId));
    PRINT_DEBUG(VAR_NAME(e->jumpLandDamage) << ": " << sizeof(e->jumpLandDamage));

    PRINT_DEBUG(VAR_NAME(e->collisionShiftThisFrame) << ": " << sizeof(e->collisionShiftThisFrame));

    PRINT_DEBUG(VAR_NAME(e->relatedEntityGuid) << ": " << sizeof(e->relatedEntityGuid));

    PRINT_DEBUG("TOTAL SIZE: " << sizeof(*e));
}

void setWorldFrameDelay(int delay)
{
    worldFrameDelay = delay;
}

int getWorldFrameDelay()
{
    return worldFrameDelay;
}
