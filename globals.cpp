#include "globals.h"

Globals globalsValues;

Globals* globals()
{
    return &globalsValues;
}

void loadOrDefaultSystemOptions()
{
    // Default options

    for(int i = OPTION_SPELL_1_BINDING; i <= OPTION_SPELL_9_BINDING; i++)
        globals()->systemOptions[i] = -1;

    globals()->systemOptions[OPTION_FULL_SCREEN] = 1;
    globals()->systemOptions[OPTION_MASTER_VOLUME] = 100;
    globals()->systemOptions[OPTION_SOUND_VOLUME] = 100;
    globals()->systemOptions[OPTION_MUSIC_VOLUME] = 100;
    globals()->systemOptions[OPTION_VOICE_VOLUME] = 100;

    // Load options from file

    readSystemOptionsFromFile();
}

void writeSystemOptionsToFile()
{
    std::string saveString = "";

    for(int i = 0; i < 100; i++)
    {
        saveString += std::to_string(globalsValues.systemOptions[i]);
        saveString += ",";
    }

    writeStringToFile(saveString, "options.txt");
}

void readSystemOptionsFromFile()
{
    std::string saveString = readStringFromFile("options.txt");

    if(saveString == "NO_FILE_FOUND")
    {
        PRINT_DEBUG("Options file not found.");
    }

    std::string acc = "";
    int i = 0;

    for(int j = 0; j < (int)saveString.length(); j++)
    {
        char c = saveString[j];

        if(c == ',')
        {
            globalsValues.systemOptions[i] = std::stoi(acc);
            acc = "";
            i++;
        }
        else
        {
            acc += c;
        }
    }
}
