#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>

#include "enums.h"
#include "utils.h"

struct Entity; // Forward declaration for entity.h
struct PlayerStats; // Forward declaration for stats.h

#define FRAME_RATE 60
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

struct Globals
{
    int globalTick = 0;
    int worldTick = 0;

    Entity* player = 0;
    int numberOfEntities = 0;
    int entitiesDefeated = 0;

    int worldWidth = 1;
    int worldHeight = 1;

    int viewPortX = 0;
    int viewPortY = 0;

    bool shouldQuitImmediately = false;

    bool allLearnedSpells[100] = {0};

    int systemOptions[100] = {0};

    float inferredFps = 0;

    bool isPlayerMoveSelfDisabled = false;
    bool isPlayerDialogLocked = false;

    PlayerStats* playerStats;

    Entity* currentDialogFocusedEntity = 0;
};

void loadOrDefaultSystemOptions();

void writeSystemOptionsToFile();
void readSystemOptionsFromFile();

Globals* globals();

#endif // GLOBALS_H
