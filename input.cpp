#include "input.h"

bool keysDown[1000] = {0};
bool keysDownLastFrame[1000] = {0};

void stepInputState(sf::RenderWindow* window)
{
    for(int i = 0; i < 1000; i++)
    {
        keysDownLastFrame[i] = keysDown[i];
    }

    sf::Event event;
    while (window->pollEvent(event))
    {
        if(event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Tilde))
            globals()->shouldQuitImmediately = true;

        if(event.type == sf::Event::EventType::KeyPressed)
        {
            keysDown[event.key.code] = true;
        }

        if(event.type == sf::Event::EventType::KeyReleased)
            keysDown[event.key.code] = false;
    }
}

int gameKeyEnumToSystemOptionEnum(int key)
{
    return key - KEY_SPELL_1 + OPTION_SPELL_1_BINDING;
}

int mapGameKeyToSfmlKey(int key)
{
    if(isKeyHasCustomBinding(key))
    {
        return globals()->systemOptions[gameKeyEnumToSystemOptionEnum(key)];
    }

    // Default keys

    if(key == KEY_NO_KEY) return -1;
    if(key == KEY_LEFT) return sf::Keyboard::Key::Left;
    if(key == KEY_RIGHT) return sf::Keyboard::Key::Right;
    if(key == KEY_UP) return sf::Keyboard::Key::Up;
    if(key == KEY_DOWN) return sf::Keyboard::Key::Down;
    if(key == KEY_ESCAPE) return sf::Keyboard::Key::Escape;
    if(key == KEY_ENTER) return sf::Keyboard::Key::Enter;
    if(key == KEY_SPACE) return sf::Keyboard::Key::Space;
    if(key == KEY_CAST) return sf::Keyboard::Key::Z;
    if(key == KEY_SPELL_1) return sf::Keyboard::Key::W;
    if(key == KEY_SPELL_2) return sf::Keyboard::Key::A;
    if(key == KEY_SPELL_3) return sf::Keyboard::Key::S;
    if(key == KEY_SPELL_4) return sf::Keyboard::Key::D;
    if(key == KEY_SPELL_5) return sf::Keyboard::Key::LShift;
    if(key == KEY_DEBUG_1) return sf::Keyboard::Key::Num1;
    if(key == KEY_DEBUG_2) return sf::Keyboard::Key::Num2;
    if(key == KEY_DEBUG_3) return sf::Keyboard::Key::Num3;
    if(key == KEY_DEBUG_4) return sf::Keyboard::Key::Num4;
    if(key == KEY_DEBUG_5) return sf::Keyboard::Key::Num5;
    if(key == KEY_DEBUG_6) return sf::Keyboard::Key::Num6;
    if(key == KEY_DEBUG_7) return sf::Keyboard::Key::Num7;
    if(key == KEY_DEBUG_8) return sf::Keyboard::Key::Num8;
    if(key == KEY_DEBUG_9) return sf::Keyboard::Key::Num9;
    if(key == KEY_DEBUG_0) return sf::Keyboard::Key::Num0;
    if(key == KEY_START_PROFILING) return sf::Keyboard::Key::Subtract;
    if(key == KEY_REPORT_PROFILING) return sf::Keyboard::Key::Equal;
    if(key == KEY_DEBUG_EXIT) return sf::Keyboard::Key::Tilde;

    return -1;
}

void setCustomKeyBinding(int key, int sfmlKey)
{
    globals()->systemOptions[gameKeyEnumToSystemOptionEnum(key)] = sfmlKey;
}

bool isKeyHasCustomBinding(int key)
{
    if(!isKeyCustomMappable(key)) return false;

    return globals()->systemOptions[gameKeyEnumToSystemOptionEnum(key)] != -1;
}

bool isKeyCustomMappable(int key)
{
    return key >= KEY_SPELL_1 && key <= KEY_SPELL_5;
}

bool isKeyDown(int key)
{
    int sfmlKey = mapGameKeyToSfmlKey(key);
    if(sfmlKey == -1) return false;

    return keysDown[sfmlKey];
}

bool isKeyDownThisFrame(int key)
{
    int sfmlKey = mapGameKeyToSfmlKey(key);
    if(sfmlKey == -1) return false;

    return keysDown[sfmlKey] && !keysDownLastFrame[sfmlKey];
}

bool isKeyReleasedThisFrame(int key)
{
    int sfmlKey = mapGameKeyToSfmlKey(key);
    if(sfmlKey == -1) return false;

    return !keysDown[sfmlKey] && keysDownLastFrame[sfmlKey];
}

std::string getSfmlKeyNiceName(int sfmlKey)
{
    using K = sf::Keyboard::Key;

    if(sfmlKey == K::A) return "A";
    if(sfmlKey == K::B) return "B";
    if(sfmlKey == K::C) return "C";
    if(sfmlKey == K::D) return "D";
    if(sfmlKey == K::E) return "E";
    if(sfmlKey == K::F) return "F";
    if(sfmlKey == K::G) return "G";
    if(sfmlKey == K::H) return "H";
    if(sfmlKey == K::I) return "I";
    if(sfmlKey == K::J) return "J";
    if(sfmlKey == K::K) return "K";
    if(sfmlKey == K::L) return "L";
    if(sfmlKey == K::M) return "M";
    if(sfmlKey == K::N) return "N";
    if(sfmlKey == K::O) return "O";
    if(sfmlKey == K::P) return "P";
    if(sfmlKey == K::Q) return "Q";
    if(sfmlKey == K::R) return "R";
    if(sfmlKey == K::S) return "S";
    if(sfmlKey == K::T) return "T";
    if(sfmlKey == K::U) return "U";
    if(sfmlKey == K::V) return "V";
    if(sfmlKey == K::W) return "W";
    if(sfmlKey == K::X) return "X";
    if(sfmlKey == K::Y) return "Y";
    if(sfmlKey == K::Z) return "Z";
    if(sfmlKey == K::Num0) return "0";
    if(sfmlKey == K::Num1) return "1";
    if(sfmlKey == K::Num2) return "2";
    if(sfmlKey == K::Num3) return "3";
    if(sfmlKey == K::Num4) return "4";
    if(sfmlKey == K::Num5) return "5";
    if(sfmlKey == K::Num6) return "6";
    if(sfmlKey == K::Num7) return "7";
    if(sfmlKey == K::Num8) return "8";
    if(sfmlKey == K::Num9) return "9";
    if(sfmlKey == K::LBracket) return "[";
    if(sfmlKey == K::RBracket) return "]";
    if(sfmlKey == K::Semicolon) return ";";
    if(sfmlKey == K::Comma) return ",";
    if(sfmlKey == K::Period) return ".";
    if(sfmlKey == K::Quote) return "'";
    if(sfmlKey == K::Slash) return "/";
    if(sfmlKey == K::Backslash) return "\\";
    if(sfmlKey == K::Equal) return "=";
    if(sfmlKey == K::Hyphen) return "-";
    if(sfmlKey == K::Add) return "+";
    if(sfmlKey == K::Subtract) return "-";
    if(sfmlKey == K::Multiply) return "*";
    if(sfmlKey == K::Divide) return "/";
    if(sfmlKey == K::Numpad0) return "Num0";
    if(sfmlKey == K::Numpad1) return "Num1";
    if(sfmlKey == K::Numpad2) return "Num2";
    if(sfmlKey == K::Numpad3) return "Num3";
    if(sfmlKey == K::Numpad4) return "Num4";
    if(sfmlKey == K::Numpad5) return "Num5";
    if(sfmlKey == K::Numpad6) return "Num6";
    if(sfmlKey == K::Numpad7) return "Num7";
    if(sfmlKey == K::Numpad8) return "Num8";
    if(sfmlKey == K::Numpad9) return "Num9";
    if(sfmlKey == K::F1) return "F1";
    if(sfmlKey == K::F2) return "F2";
    if(sfmlKey == K::F3) return "F3";
    if(sfmlKey == K::F4) return "F4";
    if(sfmlKey == K::F5) return "F5";
    if(sfmlKey == K::F6) return "F6";
    if(sfmlKey == K::F7) return "F7";
    if(sfmlKey == K::F8) return "F8";
    if(sfmlKey == K::F9) return "F9";
    if(sfmlKey == K::F10) return "F10";

    if(sfmlKey == K::LControl) return "LCtrl";
    if(sfmlKey == K::LShift) return "LShift";
    if(sfmlKey == K::LAlt) return "LAlt";
    if(sfmlKey == K::RControl) return "RCtrl";
    if(sfmlKey == K::RShift) return "RShift";
    if(sfmlKey == K::RAlt) return "RAlt";
    if(sfmlKey == K::Tab) return "Tab";

    return "NONE";
}

bool isRawSfmlKeyDownThisFrame(int sfmlKey)
{
    return keysDown[sfmlKey] && !keysDownLastFrame[sfmlKey];
}

bool isInteractPressedThisFrame()
{
    return isKeyDownThisFrame(KEY_ENTER) || isKeyDownThisFrame(KEY_SPACE) || isKeyDownThisFrame(KEY_CAST);
}

bool isAnyKeyPressedThisFrame()
{
    for(int i = 0; i < 1000; i++)
    {
        if(isRawSfmlKeyDownThisFrame(i))
            return true;
    }

    return false;
}
