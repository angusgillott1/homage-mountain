#ifndef INPUT_H
#define INPUT_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <iostream>

#include "globals.h"
#include "enums.h"

void stepInputState(sf::RenderWindow* window);

int mapGameKeyToSfmlKey(int key);

int gameKeyEnumToSystemOptionEnum(int key);

void setCustomKeyBinding(int key, int sfmlKey);
bool isKeyHasCustomBinding(int key);
bool isKeyCustomMappable(int key);

bool isKeyDown(int key);
bool isKeyDownThisFrame(int key);
bool isKeyReleasedThisFrame(int key);

std::string getSfmlKeyNiceName(int sfmlKey);

bool isRawSfmlKeyDownThisFrame(int sfmlKey);

bool isInteractPressedThisFrame();
bool isAnyKeyPressedThisFrame();

#endif // INPUT_H
