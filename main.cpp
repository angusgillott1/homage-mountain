#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <algorithm>

#include "input.h"
#include "entity.h"
#include "npcs.h"
#include "spell.h"
#include "particle.h"
#include "assets.h"
#include "globals.h"
#include "scene.h"
#include "stats.h"
#include "weather.h"

//---------------
// Local globals
//---------------

int suspendMovement = 0;

const int FRAME_TIME = 1000 / FRAME_RATE;
const int SUSPEND_MOVEMENT_DELAY_FRAMES = 8;

//---
// Enemy scaling
//---

const int baseSecondsBetweenEnemySpawns = 8; // setting
const int baseWaveSpawnEveryNthEnemySpawn = 10; // setting

const int baseTicksBetweenEnemySpawns = baseSecondsBetweenEnemySpawns * FRAME_RATE;

bool isWindowFullscreen = false;

//-------------
// Function declarations
//-------------

void handleWorldInput()
{
    if(getCurrentMap() && getCurrentMap()->mapType != MAP_TYPE_BATTLE_MAP)
        goto SKIP_CAST_INPUT;

    if(isKeyDown(KEY_CAST))
    {
        if(isKeyDownThisFrame(KEY_CAST))
        {
            playSound(SOUND_CAST_GLOW);
            makeEntityFlash(globals()->player, 10, sf::Color(255,255,50,128), sf::Color(0,0,0,(128 / 10) + 1));
        }

        if(isKeyDownThisFrame(KEY_LEFT))
            addSpellStroke(2);

        if(isKeyDownThisFrame(KEY_RIGHT))
            addSpellStroke(4);

        if(isKeyDownThisFrame(KEY_UP))
            addSpellStroke(1);

        if(isKeyDownThisFrame(KEY_DOWN))
            addSpellStroke(3);

        suspendMovement = SUSPEND_MOVEMENT_DELAY_FRAMES;
    }
    else
    {
        if(isKeyReleasedThisFrame(KEY_CAST))
        {
            dischargeSpellInput();
            suspendMovement = SUSPEND_MOVEMENT_DELAY_FRAMES;
        }
    }

    SKIP_CAST_INPUT:

    if(!isEntityMoving(globals()->player))
    {
        globals()->player->animationPatternIndex = 0;
        globals()->player->frameColumn = globals()->player->animationPattern[0];
    }

    // Easy cast spells
    if(getCurrentMap() && getCurrentMap()->mapType == MAP_TYPE_BATTLE_MAP)
    {
//        if(isKeyDownThisFrame(KEY_SPELL_1)) incantSpell(SPELL_FIREBALL);
//        if(isKeyDownThisFrame(KEY_SPELL_2)) incantSpell(SPELL_CHAIN_LIGHTNING);
//        if(isKeyDownThisFrame(KEY_SPELL_3)) incantSpell(SPELL_BLIZZARD);
//        if(isKeyDownThisFrame(KEY_SPELL_4)) incantSpell(SPELL_SCORCHING_LIGHT);
        if(isKeyDownThisFrame(KEY_SPELL_5)) incantSpell(SPELL_TELEPORT);
    }

    // Check input for movement
    if(suspendMovement > 0)
        suspendMovement--;
    else
        makeEntityMoveAccordingToCurrentInput(globals()->player, true, true);

    // Check for dialog interactions input

    if(isInteractPressedThisFrame() && getCurrentMap()->mapType == MAP_TYPE_EXPLORE_MAP && globals()->currentDialogFocusedEntity != 0)
    {
        triggerDialog(globals()->currentDialogFocusedEntity, globals()->player);
    }
}

void updateWorld()
{
    globals()->worldTick++;

    // Spawn enemies according to spawn rate
//    int ticksBetweenEnemySpawns = baseTicksBetweenEnemySpawns;
//
//    if(globals()->worldTick % (int)ticksBetweenEnemySpawns == 0)
//    {
//        PRINT_DEBUG("Ticks between enemy spawns: " << ticksBetweenEnemySpawns << " (" << ((float)baseTicksBetweenEnemySpawns / ticksBetweenEnemySpawns) * 100.0 - 100  << "%)");
//        randomEnemyAdd(getWorldBoundRight() - 16, (rand() % (getWorldBoundBottom() - getWorldBoundTop() - 32)) + getWorldBoundTop() + 16);
//
//        if(globals()->worldTick % ((int)ticksBetweenEnemySpawns * baseWaveSpawnEveryNthEnemySpawn) == 0)
//        {
//            PRINT_DEBUG("BIG WAVE!");
//            for(int i = 0; i < 5; i++)
//            {
//                randomEnemyAdd(getWorldBoundRight() - 16, (rand() % (getWorldBoundBottom() - getWorldBoundTop() - 32)) + getWorldBoundTop() + 16);
//            }
//        }
//    }

    bool isBattleMap = getCurrentMap() != 0 && getCurrentMap()->mapType == MAP_TYPE_BATTLE_MAP;

    if(isBattleMap)
    {
        const int spawnInterval = 30; // The number of frames represented by one tile in spawn map
        if((globals()->worldTick - 1) % spawnInterval == 0)
        {
            short int spawnTimeIndex = (short int)((globals()->worldTick - 1) / spawnInterval);
            doSpawnMapForTimeIndex(spawnTimeIndex);
        }
    }

    // Done spawning enemies
    if(isBattleMap) stepStats();

    stepSpells();
    stepEntityActions();
    stepEntityMovements();
    stepEntityMiscs();
    detectAndResolveCollisions();

    stepParticles();
    stepWeather();
    stepEntityAnimations();
    stepFloatingTexts();

    cleanupEntities();
}

// SCENE RENDERING FUNCTIONS


int main()
{
    std::srand(std::time(0));
    loadOrDefaultSystemOptions();

    int frameLog[20] = {0};
    int frameLogIndex = 0;

    //Preload assets
    preloadAssets();

    // Set up window
    int windowStyle = sf::Style::Default;
    if(globals()->systemOptions[OPTION_FULL_SCREEN]) windowStyle = sf::Style::Fullscreen;

    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Homage Mountain", windowStyle);
    window.setVerticalSyncEnabled(true);
    window.setFramerateLimit(60);

    initializeStats();
    initializeSpellInputGraphics();
    initializeSpells();
    initializeInterface();
    initializeWeather();

    switchToScene(SCENE_BATTLE_OR_EXPLORE);

    //printEntitySize();

    while (true)
    {
        // Toggle window fullscreen/windowed
        if(isWindowFullscreen != globals()->systemOptions[OPTION_FULL_SCREEN])
        {
            window.close();
            window.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Homage Mountain", isWindowFullscreen ? sf::Style::Default : sf::Style::Fullscreen);
            isWindowFullscreen = !isWindowFullscreen;
        }

        // Gather/scan input state
        stepInputState(&window);

        // Debug input

        if(isKeyDown(KEY_DEBUG_1) && !(rand() % 1))
        {
            for(int i = 0; i < 10; i++)
                randomEnemyAdd(getWorldBoundRight() - 16, (rand() % (getWorldBoundBottom() - getWorldBoundTop() - 32)) + getWorldBoundTop() + 16);

            if(globals()->numberOfEntities % 10 == 2)
            {
                PRINT_DEBUG("Number of entities: " << globals()->numberOfEntities);
            }
        }

        if(isKeyDown(KEY_DEBUG_2))
        {
            restorePlayerMana(10);
        }

        if(isKeyDownThisFrame(KEY_DEBUG_3))
        {
            setWeather(WEATHER_RAIN, 5);
        }

        if(isKeyDownThisFrame(KEY_DEBUG_4))
        {
            setWeather(WEATHER_STORM, 5);
        }

        if(isKeyDownThisFrame(KEY_DEBUG_5))
        {
            setWeather(WEATHER_SNOW, 5);
        }

        if(isKeyDownThisFrame(KEY_DEBUG_6))
        {
            setWeather(WEATHER_BLIZZARD, 5);
        }

        if(isKeyDownThisFrame(KEY_DEBUG_7))
        {
            PRINT_DEBUG("Set weather: none");
            setWeather(WEATHER_NO_WEATHER);
        }

        if(isKeyDownThisFrame(KEY_SPACE))
        {
            lanternAdd(globals()->player->x, globals()->player->y);
        }

        if(isKeyDown(KEY_DEBUG_9))
        {
            for(int k = 0; k < 100; k++)
            {
                int r = 255;
                int g = rand() % 150;
                int b = rand() % 50;
                int a = 180;

                Particle* p = createParticle(rand() % globals()->worldWidth, rand() % globals()->worldHeight, rand() % 4 + 2, 5, rand() % 3 - 1, rand() % 3 - 1, 3, 1, 1200, sf::Color(r, g, b, a), sf::Color(r, g, b, a + 40), sf::Color::Transparent);
                if(p)   // p may be 0 if max particles reached
                {
                    p->isExpDiamond = true;
                    p->expValue = 1;
                }
            }
        }

        if(isKeyDownThisFrame(KEY_START_PROFILING))
        {
            startProfiling();
        }

        if(isKeyDownThisFrame(KEY_REPORT_PROFILING))
        {
            reportProfiling();
        }

        // Start frame

        sf::Clock frameClock; // starts the clock
        globals()->globalTick++;

        if(globals()->shouldQuitImmediately)
            goto QUIT;

        if(getSceneTransitionState() != TRANSITION_FROM)
        {
            handleGlobalInput();
        }

        stepInterface();

        if(
            (getCurrentScene() == SCENE_BATTLE_OR_EXPLORE && !getIsMenuOpen() && getSceneTransitionState() != TRANSITION_FROM) &&
            (globals()->globalTick % getWorldFrameDelay() == 0) // Time slowdown, by only advancing on every nth tick
        )
        {

            hotReloadCurrentMap();
            handleWorldInput();
            updateWorld();
        }

        window.clear();
            renderCurrentScene(&window);
        window.display();

        sf::Time timeOnThisFrame = frameClock.getElapsedTime();

        // Keep a running average of frame time
        frameLog[frameLogIndex] = timeOnThisFrame.asMilliseconds();
        frameLogIndex++;
        frameLogIndex %= 20;
        int runningFrameAverage = 0;
        for(int i = 0; i < 20; i++)
        {
            runningFrameAverage += frameLog[i];
        }
        runningFrameAverage /= 20;

        if(timeOnThisFrame.asMilliseconds() > FRAME_TIME + 5)
        {
            PRINT_DEBUG("Average frame time: " << runningFrameAverage << "\n");
        }

        globals()->inferredFps = 1000.0 / runningFrameAverage;
    }

    QUIT:
    return 0;
}
