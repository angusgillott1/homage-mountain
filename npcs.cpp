#include "npcs.h"

extern const int MAX_MOVEMENT_PLAN_LENGTH;

void doOffLeftEdgeCheck(Entity* entity)
{
    // If it's offscreen, do damage and despawn
    if(entity->x == getWorldBoundLeft())
    {
        dealDamageToEntity(globals()->player, entity->maxHp / 2);
        entity->markedForDestruction = true;
    }
}

void shamblerAi(Entity* entity)
{
    doOffLeftEdgeCheck(entity);

    const int chasePlayerRadius = 120;

    // If very close to player, jump at player
    if(distanceBetweenEntities(globals()->player, entity) < 36 && ticksSinceEntityLanded(entity) > 35)
    {
        makeEntityJump(entity, globals()->player->x - entity->x, (globals()->player->y + 12) - entity->y, 15, entity->moveSpeed);
        return;
    }

    // If it's right in the middle of movement, finish moving
    if(!isEntityAtGoalLocation(entity))
    {
        return;
    }

    // If nearly at left edge, just go left
    if(entity->x <= 32)
    {
        setEntityMoveIntentionGrid(entity, 2, trueWidth(entity), true, true);
        return;
    }

    // If close to player, approach player
    if(distanceBetweenEntities(entity, globals()->player) < chasePlayerRadius && distanceBetweenEntities(entity, globals()->player) < entity->x)
    {
        entityApproachOrAvoid(entity, globals()->player, true);
        return;
    }

    // Default behaviour, walk left
    setEntityMoveIntentionGrid(entity, 2, trueWidth(entity), true, true);
}

void shamblerCollision(Entity* thisEntity, Entity* thatEntity)
{
    if(thatEntity == globals()->player && !isEntityImmobilized(thisEntity))
    {
        if(!isEntityWaiting(thisEntity))
        {
            dealDamageToEntity(globals()->player, thisEntity->maxHp / 8);

            makeEntityWait(thisEntity, 60);
            makeEntityJump(thisEntity, 0, 0, 20, thisEntity->moveSpeed);
            playSoundAtEntityLocation(SOUND_SHAMBLER_HIT);
        }
    }
    genericEntityCollision(thisEntity, thatEntity);
}

void sprinterAi(Entity* entity)
{
    doOffLeftEdgeCheck(entity);

    if(entity->hp < entity->maxHp && entity->moveSpeed < 3)
    {
        entity->moveSpeed = 3;
    }

    int fleePlayerRadius = 64;

    if(!isEntityAtGoalLocation(entity)) return; // It's right in the middle of movement, finish moving

    if(entity->x <= 32) // Always move left if nearly at edge
    {
        setEntityMoveIntentionGrid(entity, 2, trueWidth(entity), true, true);
        return;
    }

    // If near player, flee player
    if(distanceBetweenEntities(entity, globals()->player) < fleePlayerRadius && centerX(entity) > centerX(globals()->player))
    {
        int dyToMidpoints = centerY(globals()->player) - centerY(entity);
        if(std::abs(dyToMidpoints) < fleePlayerRadius)
        {
            int jumpDy;
            if(dyToMidpoints > 0)
                jumpDy = -fleePlayerRadius;
            else
                jumpDy = fleePlayerRadius;

            if(std::abs(getWorldBoundTop() - centerY(entity)) < trueHeight(globals()->player) * 2)
                jumpDy = fleePlayerRadius;

            if(std::abs(getWorldBoundBottom() - centerY(entity)) < trueHeight(globals()->player))
                jumpDy = -fleePlayerRadius;

            // Do jump
            playSoundAtEntityLocation(SOUND_JESTER_LAND, entity);
            makeEntityJump(entity, 0, jumpDy, 16, entity->moveSpeed, SOUND_SOFT_FALL);
        }
    }

    if(!entity->isJumping)
    {
        setEntityMoveIntentionGrid(entity, 2, trueWidth(entity), true, true);
    }
}

void sprinterCollision(Entity* thisEntity, Entity* thatEntity)
{
    genericEntityCollision(thisEntity, thatEntity);
}

bool bruteFindClosestExclude(Entity* e)
{
    if(e->weight >= 9) return true;
    if(e->entitySubType == SUBTYPE_GHOST) return true;

    return false;
}

void bruteCleanup(Entity* entity)
{
    Entity* heldEntity = entity->relatedEntities[RELATED_ENTITY_1];
    if(!getEntityByGuid(entity->relatedEntityGuid)) // Doesn't exist any more
        heldEntity = 0;

    if(heldEntity)
    {
        heldEntity->layer = entity->attrs[ATTR_MEMORY_1];
        entity->relatedEntities[RELATED_ENTITY_1] = 0;
        entity->relatedEntityGuid = 0;
        entity->attrs[ATTR_STATE_MACHINE] = 0;
        heldEntity->isEthereal = false;
        return;
    }
}

void bruteAi(Entity* entity)
{
    doOffLeftEdgeCheck(entity);

    const int waitTime = 45;

    // Always manage held entity first in every frame, if any
    Entity* heldEntity = entity->relatedEntities[RELATED_ENTITY_1];
    if(!getEntityByGuid(entity->relatedEntityGuid))
        heldEntity = 0;

    if(!heldEntity)
    {
        entity->attrs[ATTR_STATE_MACHINE] = 0;
    }

    // Possibly release held entity if no longer immobilized (e.g. player teleport)
    if(heldEntity && (!isEntityImmobilized(heldEntity) || (heldEntity->isJumping && heldEntity->jumpTick > heldEntity->jumpDuration / 2)))
    {
        bruteCleanup(entity);
        return;
    }

    // Make sure entity is always held properly
    if(heldEntity)
    {
        if(entity->attrs[ATTR_STATE_MACHINE] < 4)
        {
            heldEntity->x = regressToMean(heldEntity->x, 4, centerX(entity) - trueWidth(entity) / 2);
            heldEntity->y = regressToMean(heldEntity->y, 4, centerY(entity) - 10);
            heldEntity->goalX = heldEntity->x;
            heldEntity->goalY = heldEntity->y;
        }
    }

    if(entity->attrs[ATTR_STATE_MACHINE] == 0)  // Not holding any entity
    {
        if(!isEntityAtGoalLocation(entity)) return; // It's right in the middle of movement, finish moving

        // Always move left if nearly at edge
        if(entity->x <= 32)
        {
            setEntityMoveIntentionGrid(entity, 2, trueWidth(entity), true, true);
            return;
        }

        Entity* chasedEntity = 0;
        if(distanceBetweenEntities(entity, globals()->player) < 120)
        {
            chasedEntity = globals()->player;
        }
        else
        {
            Entity* placeholderArray[1];
            Entity* closestEntity = findClosest(entity, placeholderArray, 0, bruteFindClosestExclude);
            if(closestEntity && distanceBetweenEntities(entity, closestEntity) < 140)
                chasedEntity = closestEntity;
        }

        if(chasedEntity == globals()->player && distanceBetweenEntities(globals()->player, entity) < 44 && ticksSinceEntityLanded(entity) > 20)
        {
            makeEntityJump(entity, globals()->player->x - entity->x, (globals()->player->y + 12) - entity->y, 15, entity->moveSpeed);
        }

        if(chasedEntity)
        {
            entityApproachOrAvoid(entity, chasedEntity);

            if(isTouching(entity, chasedEntity) && !isEntityWaiting(entity))
            {
                if(chasedEntity != globals()->player)
                {
                    immobilizeEntity(chasedEntity, 300);
                    entity->relatedEntities[RELATED_ENTITY_1] = chasedEntity;
                    entity->relatedEntityGuid = chasedEntity->guid;
                    entity->attrs[ATTR_MEMORY_1] = chasedEntity->layer;
                    chasedEntity->layer = 6;
                    chasedEntity->isEthereal = true;
                    entity->attrs[ATTR_STATE_MACHINE] = 1;
                }
                else
                {
                    int throwDx = 240;
                    int throwDy = 0;
                    if(centerX(chasedEntity) > centerX(entity)) throwDx = -throwDx;

                    if(chasedEntity->x < getWorldBoundLeft(chasedEntity->entityType == PLAYER) + 240)
                    {
                        throwDx = 30;
                    }

                    makeEntityJumpTo(chasedEntity, chasedEntity->x - throwDx, chasedEntity->y - throwDy, 140, 60, SOUND_FALL_DAMAGE, entity->maxHp / 8);
                    dealDamageToEntity(chasedEntity, entity->maxHp / 16);
                    playSound(SOUND_BRUTE_STRIKE);
                    immobilizeEntity(chasedEntity, 120);
                    makeEntityWait(entity, waitTime * 4);
                }
            }

            return;
        }

        // Default behaviour, walk left
        setEntityMoveIntentionGrid(entity, 2, trueWidth(entity), true, true);
    }

    if(entity->attrs[ATTR_STATE_MACHINE] == 1) // Holding an entity
    {
        if(heldEntity->entityType == PLAYER)
            setEntityMoveIntentionGrid(entity, 4, trueWidth(entity) * 2, true, true);
        else
            setEntityMoveIntentionGrid(entity, 2, trueWidth(entity) * 2, true, true);

        entity->attrs[ATTR_STATE_MACHINE] = 2;

        return;
    }

    if(entity->attrs[ATTR_STATE_MACHINE] == 2) // Walking with entity then waiting
    {
        if(isEntityAtGoalLocation(entity))
        {
            entity->attrs[ATTR_STATE_MACHINE] = 3;
            makeEntityWait(entity, waitTime);
        }

        return;
    }

    if(entity->attrs[ATTR_STATE_MACHINE] == 3) // Throwing then waiting
    {
        if(isEntityAtGoalLocation(entity))
        {
            makeEntityJump(entity, 0, 0, 10, entity->moveSpeed);

            int throwDx = 240;
            if(rand() % 2) throwDx += 60;

            int throwDy = 0;
            if(heldEntity->entityType == PLAYER) throwDx = -throwDx;

            makeEntityJumpTo(heldEntity, heldEntity->x - throwDx, heldEntity->y - throwDy, 100, 60, SOUND_SOFT_FALL, 2);
            immobilizeEntity(heldEntity, 100);
            entity->animationPatternIndex = 0;
            entity->frameColumn = entity->animationPattern[0];

            entity->attrs[ATTR_STATE_MACHINE] = 4;
        }

        return;
    }

    if(entity->attrs[ATTR_STATE_MACHINE] == 4) // Waiting for thrown entity to land
    {
        if(!heldEntity->isJumping)
        {
            setEntityMoveIntentionGrid(entity, 2, trueWidth(entity) * 4, true, true);
        }

        return;
    }
}

void bruteCollision(Entity* thisEntity, Entity* thatEntity)
{
    genericEntityCollision(thisEntity, thatEntity);
}

void wizardAi(Entity* entity)
{
    doOffLeftEdgeCheck(entity);

    Entity* player = globals()->player;

    makeEntityFaceEntity(entity, player);

    int dxToPlayer = centerX(player) - centerX(entity);
    int dyToPlayer = centerY(player) - (entity->y + trueHeight(entity) / 2 + 3);

    if(entity->attrs[ATTR_STATE_MACHINE] == 0)  // Walk mode
    {
        if(!isEntityAtGoalLocation(entity)) return; // It's right in the middle of movement, finish moving

        // Always move left if nearly at edge
        if(entity->x <= 32)
        {
            setEntityMoveIntentionGrid(entity, 2, trueWidth(entity), true, true);
            return;
        }

        entity->attrs[ATTR_STATE_MACHINE] = 1;
        return;
    }

    if(entity->attrs[ATTR_STATE_MACHINE] == 1) // Teleport mode
    {
        if(!isEntityAtGoalLocation(entity)) // Continue moving
            return;

        const int maxTeleportDistance = 120;

        makeEntityFaceEntity(entity, player);

        int teleportDirection = entity->facing;
        int teleportDistance = maxTeleportDistance;
        int noTeleportTolerance = 10;

        if(entity->facing == 1 || entity->facing == 3)
        {
            if(std::abs(dyToPlayer) < 180)
            {
                teleportDistance = std::abs(dxToPlayer);
                if(dxToPlayer < -noTeleportTolerance)
                    teleportDirection = 2;
                else if(dxToPlayer > noTeleportTolerance)
                    teleportDirection = 4;
                else
                    teleportDirection = 0;
            }
        }
        else
        {
            if(std::abs(dxToPlayer) < 180)
            {
                teleportDistance = std::abs(dyToPlayer);
                if(dyToPlayer < -noTeleportTolerance)
                    teleportDirection = 1;
                else if(dyToPlayer > noTeleportTolerance)
                    teleportDirection = 3;
                else
                    teleportDirection = 0;
            }
        }

        if(teleportDirection != 0)
        {
            entity->facing = teleportDirection;
            castSpell(entity, SPELL_TELEPORT, teleportDistance);
            entity->attrs[ATTR_STATE_MACHINE] = 2;
        }
        else
        {
            entity->attrs[ATTR_STATE_MACHINE] = 3;
        }

        return;
    }

    if(entity->attrs[ATTR_STATE_MACHINE] == 2) // Turn and wait after teleport
    {
        if(!isEntityAtGoalLocation(entity)) // Continue moving
            return;

        makeEntityFaceEntity(entity, player);
        makeEntityWait(entity, 50 + rand() % 20);

        entity->attrs[ATTR_STATE_MACHINE] = 3;
        return;
    }

    if(entity->attrs[ATTR_STATE_MACHINE] == 3) // Cast or move on after teleport
    {
        if(!isEntityAtGoalLocation(entity)) // Continue moving
            return;

        if(distanceBetweenEntities(entity, player) <= 180)
        {
            makeEntityJump(entity, 0, 0, 5, 1);
            castSpell(entity, SPELL_FIREBALL, 0);
            makeEntityWait(entity, 35);
        }

        entity->attrs[ATTR_STATE_MACHINE] = 0;
        return;
    }
}

void wizardCollision(Entity* thisEntity, Entity* thatEntity)
{
    genericEntityCollision(thisEntity, thatEntity);
}

void ghostAi(Entity* entity)
{
    doOffLeftEdgeCheck(entity);

    float dY = 3 * sin(2.0 * PI_VALUE * globals()->worldTick / 150.0f);

    setEntityMoveIntention(entity, -1, (int)dY, false);
    entity->facing = 2;
    resetEntityAnimationToItsFacing(entity);
    entity->moveSpeed = 10;
}

void ghostCollision(Entity* thisEntity, Entity* thatEntity)
{
    genericEntityCollision(thisEntity, thatEntity);
}

void skeletonMageAi(Entity* entity)
{
    doOffLeftEdgeCheck(entity);

    int stateStep = 0;

    // Walking left
    if(entity->attrs[ATTR_STATE_MACHINE] == stateStep++)
    {
        // Always move left if nearly at edge
        if(entity->x <= 32)
        {
            setEntityMoveIntentionGrid(entity, 2, trueWidth(entity), true, true);
            return;
        }

        setEntityMoveIntentionGrid(entity, 2, trueWidth(entity) * 4, true, true);

        entity->attrs[ATTR_STATE_MACHINE]++;
        return;
    }

    // Waiting to finish walking
    if(entity->attrs[ATTR_STATE_MACHINE] == stateStep++)
    {
        if(!isEntityMoving(entity))
        {
            if(entity->facing == 2) entity->attrs[ATTR_STATE_MACHINE]++; // Facing left, we're ready to beam
            else entity->attrs[ATTR_STATE_MACHINE]--; // Not facing left, continue walking
        }
        return;
    }

    // Wait before shake
    if(entity->attrs[ATTR_STATE_MACHINE] == stateStep++)
    {
        makeEntityWait(entity, 5);
        entity->attrs[ATTR_STATE_MACHINE]++;
        return;
    }

    // Shake slightly
    if(entity->attrs[ATTR_STATE_MACHINE] == stateStep++)
    {
        playSoundAtEntityLocation(SOUND_ENEMY_CAST_PREPARE, entity);
        makeEntityShake(entity, 90, 1);
        makeEntityWait(entity, 90);
        entity->attrs[ATTR_STATE_MACHINE]++;
        return;
    }

    // Fire beam and then wait
    if(entity->attrs[ATTR_STATE_MACHINE] == stateStep++)
    {
        makeEntityJump(entity, 0, 0, 6, 4);
        castSpell(entity, SPELL_SCORCHING_LIGHT);
        entity->attrs[ATTR_STATE_MACHINE] = 0;
        makeEntityWait(entity, 90);
        return;
    }
}

void skeletonMageCollision(Entity* thisEntity, Entity* thatEntity)
{
    genericEntityCollision(thisEntity, thatEntity);
}

void playerAdd(int startX, int startY)
{
    globals()->player = newEntity(PLAYER, startX, startY, 150, 4, 4, TEXTURE_INNKEEPER_SPRITESHEET);

    Entity* player = globals()->player;
    player->entitySubType = SUBTYPE_PLAYER;
    player->collisionFunction = 0;
    player->hitBoxClearanceTop = 15;
    player->hitBoxClearanceBottom = 0;

    player->facing = 4;
    resetEntityAnimationToItsFacing(player);

    globals()->playerStats->playerMaxMp = 100;
    globals()->playerStats->playerMp = globals()->playerStats->playerMaxMp; // / 2.0;
    globals()->playerStats->playerDisplayMp = globals()->playerStats->playerMp;

    addEntity(player);
}

void shamblerAdd(int startX, int startY)
{
    Entity* e = newEntity(ENEMY, startX, startY, adjustStrengthForArmyStage(24), 1, 5, TEXTURE_SHAMBLER_SPRITESHEET, shamblerAi, shamblerCollision);

    e->animationPatternIndex = rand() % 4;
    e->animationTick = rand() % 12;
    e->shouldAnimateAtRest = true;
    e->hitBoxClearanceLeft = 2;
    e->hitBoxClearanceTop = 0;
    e->hitBoxClearanceRight = 2;
    e->hitBoxClearanceBottom = 0;
    e->pathfindingClearance = 1;
    e->weight = 2;
    e->entitySubType = SUBTYPE_SHAMBLER;

    addEntity(e);
}

void sprinterAdd(int startX, int startY)
{
    Entity* e = newEntity(ENEMY, startX, startY, adjustStrengthForArmyStage(16), 2, 5, TEXTURE_SPRINTER_SPRITESHEET, sprinterAi, sprinterCollision);

    e->animationPatternIndex = rand() % 4;
    e->animationTick = rand() % 12;
    e->shouldAnimateAtRest = true;
    e->hitBoxClearanceLeft = 3;
    e->hitBoxClearanceTop = 2;
    e->hitBoxClearanceRight = 3;
    e->hitBoxClearanceBottom = 0;
    e->pathfindingClearance = 1;
    e->weight = 1;
    e->entitySubType = SUBTYPE_SPRINTER;

    addEntity(e);
}

void bruteAdd(int startX, int startY)
{
    Entity* e = newEntity(ENEMY, startX, startY, adjustStrengthForArmyStage(110), 2, 5, TEXTURE_BRUTE_SPRITESHEET, bruteAi, bruteCollision);

    e->weight = 9;
    e->animationPatternIndex = rand() % 4;
    e->animationTick = rand() % 12;
    e->shouldAnimateAtRest = false;
    e->hitBoxClearanceLeft = 4;
    e->hitBoxClearanceTop = 10;
    e->hitBoxClearanceRight = 4;
    e->hitBoxClearanceBottom = 0;
    e->pathfindingClearance = 2;
    e->entitySubType = SUBTYPE_BRUTE;
    e->cleanupFunction = bruteCleanup;

    addEntity(e);
}

void wizardAdd(int startX, int startY)
{
    Entity* e = newEntity(ENEMY, startX, startY, adjustStrengthForArmyStage(36), 1, 5, TEXTURE_WIZARD_SPRITESHEET, wizardAi, wizardCollision);

    e->weight = 2;
    e->animationPatternIndex = rand() % 4;
    e->animationTick = rand() % 12;
    e->shouldAnimateAtRest = false;
    e->hitBoxClearanceLeft = 4;
    e->hitBoxClearanceTop = 6;
    e->hitBoxClearanceRight = 4;
    e->hitBoxClearanceBottom = 0;
    e->pathfindingClearance = 1;
    e->entitySubType = SUBTYPE_WIZARD;

    addEntity(e);
}

void ghostAdd(int startX, int startY)
{
    Entity* e = newEntity(ENEMY, startX, startY, adjustStrengthForArmyStage(30), 1, 7, TEXTURE_GHOST_SPRITESHEET, ghostAi, ghostCollision);

    e->weight = 1;
    e->facing = 2;
    e->isGhostly = true;
    e->animationPatternIndex = rand() % 4;
    e->animationTick = rand() % 12;
    e->shouldAnimateAtRest = true;
    e->hitBoxClearanceLeft = 2;
    e->hitBoxClearanceTop = 2;
    e->hitBoxClearanceRight = 2;
    e->hitBoxClearanceBottom = 2;
    e->pathfindingClearance = 1;
    e->entitySubType = SUBTYPE_GHOST;
    e->sprite->setColor(sf::Color(255,255,255,140));

    addEntity(e);
}

void skeletonMageAdd(int startX, int startY)
{
    Entity* e = newEntity(ENEMY, startX, startY, adjustStrengthForArmyStage(28), 1, 5, TEXTURE_SKELETON_MAGE_SPRITESHEET, skeletonMageAi, skeletonMageCollision);

    e->weight = 1;
    e->facing = 2;
    e->animationPatternIndex = rand() % 4;
    e->animationTick = rand() % 12;
    e->shouldAnimateAtRest = false;
    e->hitBoxClearanceLeft = 8;
    e->hitBoxClearanceTop = 1;
    e->hitBoxClearanceRight = 8;
    e->hitBoxClearanceBottom = 1;
    e->pathfindingClearance = 2;
    e->entitySubType = SUBTYPE_SKELETON_MAGE;

    addEntity(e);
}


void randomEnemyAdd(int startX, int startY)
{
    int enemyRoll = rand() % 100 + 1;

    if(enemyRoll <= 40)
    {
        shamblerAdd(startX, startY);
    }
    else if(enemyRoll <= 60)
    {
        sprinterAdd(startX, startY);
    }
    else if(enemyRoll <= 70)
    {
        bruteAdd(startX, startY);
    }
    else if(enemyRoll <= 80)
    {
        wizardAdd(startX, startY);
    }
    else if(enemyRoll <= 90)
    {
        ghostAdd(startX, startY);
    }
    else if(enemyRoll <= 100)
    {
        skeletonMageAdd(startX, startY);
    }
}

void wanderingNpcAdd(int startX, int startY, std::string mapEntityId)
{

    TextureId spritesheet = (TextureId)(std::rand() % 5 + TEXTURE_WOMAN_1_SPRITESHEET);
    Entity* e = newEntity(NPC, startX, startY, 0, 1, 4, spritesheet, wanderingNpcAi, 0);

    e->entitySubType = SUBTYPE_WANDERING_NPC;
    e->dialogPointer = retrieveDialogPack(mapEntityId);
    e->initialDialogPointer = e->dialogPointer;

    addEntity(e);
}

void wanderingNpcAi(Entity* entity)
{
    if(!isEntityAtGoalLocation(entity)) return;

    if(entity->lifeTimer % 120 == 0 && globals()->currentDialogFocusedEntity != entity)
    {
        entityTakeRandomWalkStep(entity);
    }
    else
    {
        resetEntityAnimationToItsFacing(entity);
        entity->animationPatternIndex = 0;
        entity->frameColumn = entity->animationPattern[0];
    }
}

void crateAdd(int startX, int startY)
{
    Entity* entity = newEntity(INANIMATE, startX, startY, 0, 0, 5, TEXTURE_CRATE_SPRITESHEET);
    entity->isAnimated = false;
    entity->spriteWidth = 16;
    entity->spriteHeight = 16;
    entity->weight = 2;
    entity->entitySubType = SUBTYPE_CRATE;
    addEntity(entity);
}

void rockAdd(int startX, int startY)
{
    Entity* entity = newEntity(INANIMATE, startX, startY, 0, 0, 5, TEXTURE_ROCK_SPRITESHEET);
    entity->isAnimated = false;
    entity->spriteWidth = 16;
    entity->spriteHeight = 16;
    entity->entitySubType = SUBTYPE_ROCK;
    entity->weight = 4;
    addEntity(entity);
}

void lanternAdd(int startX, int startY)
{
    Entity* entity = newEntity(INANIMATE, startX, startY, 0, 0, 5, TEXTURE_LANTERN_SPRITESHEET);
    entity->isAnimated = false;
    entity->spriteWidth = 14;
    entity->spriteHeight = 17;
    entity->entitySubType = SUBTYPE_LANTERN;
    entity->weight = 2;
    entity->isEthereal = true;
    entity->layer = 3;

    createLightSource(64.0, 0.9, 0.65, 0.35, entity);
    addEntity(entity);
}

