#ifndef NPCS_H
#define NPCS_H

#include <math.h>
#include <ctime>
#include <cstdlib>

#include "entity.h"
#include "assets.h"
#include "globals.h"
#include "enums.h"

struct Entity; // Forward declaration for entity.h
void castSpell(Entity* caster, SpellName spell, int spellSpecificLogicAttribute);   // Forward declaration for spell.h

// Functions

void doOffLeftEdgeCheck(Entity* entity);

void shamblerAi(Entity* entity);
void shamblerCollision(Entity* thisEntity, Entity* thatEntity);

void sprinterAi(Entity* entity);
void sprinterCollision(Entity* thisEntity, Entity* thatEntity);

void bruteCleanup(Entity* entity);
void bruteAi(Entity* entity);
void bruteCollision(Entity* thisEntity, Entity* thatEntity);

void wizardAi(Entity* entity);
void wizardCollision(Entity* thisEntity, Entity* thatEntity);

void ghostAi(Entity* entity);
void ghostCollision(Entity* thisEntity, Entity* thatEntity);

void skeletonMageAi(Entity* entity);
void skeletonMageCollision(Entity* thisEntity, Entity* thatEntity);

void playerAdd(int startX, int startY);
void shamblerAdd(int startX, int startY);
void sprinterAdd(int startX, int startY);
void bruteAdd(int startX, int startY);
void wizardAdd(int startX, int startY);
void ghostAdd(int startX, int startY);
void skeletonMageAdd(int startX, int startY);
void randomEnemyAdd(int startX, int startY);

void wanderingNpcAdd(int startX, int startY, std::string mapEntityId);

void wanderingNpcAi(Entity* entity);

void crateAdd(int startX, int startY);
void rockAdd(int startX, int startY);
void lanternAdd(int startX, int startY);

#endif // NPCS_H

