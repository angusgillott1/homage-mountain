#include "particle.h"

const int MAX_PARTICLES = 10000;
Particle* allParticles[MAX_PARTICLES] = {0};
int numberOfParticles = 0;

Particle* createParticle(int x, int y, int width, int layer, int xVelocity, int yVelocity, int angularVelocity, int friction, int lifeDuration, sf::Color color, sf::Color outlineColor, sf::Color fade)
{
    //PRINT_DEBUG("Create particle at " << x << ", " << y;

    if(numberOfParticles >= MAX_PARTICLES)
    {
        PRINT_DEBUG("Max particle limit reached");
        return 0;
    }

    Particle* particle = new Particle();
    particle->x = x;
    particle->y = y;
    particle->width = width;
    particle->xVelocity = xVelocity;
    particle->yVelocity = yVelocity;
    particle->angularVelocity = angularVelocity;
    particle->rotation = 0;
    particle->friction = friction;
    particle->lifeDuration = lifeDuration;
    particle->color = color;
    particle->outlineColor = outlineColor;
    particle->fade = fade;
    particle->layer = layer;

    allParticles[numberOfParticles] = particle;
    numberOfParticles++;

    return particle;
    //PRINT_DEBUG("Number of particles: " << numberOfParticles);
}

void destroyParticle(Particle* particle)
{
    int i = 0;

    for(i = 0; i < numberOfParticles; i++)
    {
        if(allParticles[i] == particle)
            break;
    }

    if(i < numberOfParticles)    // means the particle was found
    {
        delete allParticles[i];

        for(i++; i < numberOfParticles; i++)
        {
            allParticles[i - 1] = allParticles[i];
        }

        numberOfParticles--;
    }
}

void stepParticles()
{
    for(int i = 0; i < numberOfParticles; i++)
    {
        Particle* particle = allParticles[i];

        if(particle->lifeTick > 0)  // so that on first tick, it is at starting color and position
        {
            particle->x += particle->xVelocity;
            particle->y += particle->yVelocity;

            if(rand() % 2 < 1)
            {
                particle->xVelocity = regressToMean(particle->xVelocity, particle->friction, 0);
                particle->yVelocity = regressToMean(particle->yVelocity, particle->friction, 0);
            }

            particle->rotation = (particle->rotation + particle->angularVelocity) % 360;

            particle->color -= particle->fade;
            particle->outlineColor -= particle->fade;
        }

        // Behave as collectable exp
        if(particle->isExpDiamond)
        {
            float dx = centerX(globals()->player) - particle->x;
            float dy = centerY(globals()->player) - particle->y;
            float distance = std::sqrt(dx * dx + dy * dy);

            float nominalForce = 6;
            float fullForceDistance = 64;
            float force = nominalForce;

            if(distance > fullForceDistance)
            {
                force *= 1.0 / ((distance / fullForceDistance) * (distance / fullForceDistance));
            }

            if(force > 2.0)
            {
                particle->xVelocity = force * (dx / (std::abs(dx) + std::abs(dy)));
                particle->yVelocity = force * (dy / (std::abs(dx) + std::abs(dy)));
            }

            if(std::abs(dx) < 4 && std::abs(dy) < 4)
            {
                playSound(SOUND_EXP_TINK);
                destroyParticle(particle);
                restorePlayerMana(particle->expValue * 0.75); // Mana restore is currently 75% of exp (which is 100% of health) hardcoded
            }
        }

        particle->lifeTick++;

        if(particle->lifeTick > particle->lifeDuration)
        {
            destroyParticle(particle);
        }
    }
}

void destroyAllParticles()
{
    for(int i = 0; i < numberOfParticles; i++)
    {
        if(allParticles[i] != 0)
        {
            delete allParticles[i];
            allParticles[i] = 0;
        }
    }

    numberOfParticles = 0;
}

void renderParticleLayer(int particleLayer, sf::RenderWindow* window)
{
    sf::RectangleShape rect(sf::Vector2f(1, 1));
    rect.setOutlineColor(sf::Color::Transparent);
    rect.setOutlineThickness(1.0);

    for(int i = 0; i < numberOfParticles; i++)
    {
        Particle* p = allParticles[i];

        if(p->layer != particleLayer) continue;

        int px = p->x - globals()->viewPortX;
        int py = p->y - globals()->viewPortY;

        if(px < 0 || px >= SCREEN_WIDTH || py < 0 || py >= SCREEN_HEIGHT) continue;

        rect.setOutlineColor(p->outlineColor);
        rect.setSize(sf::Vector2f(p->width, p->width));
        rect.setFillColor(p->color);
        rect.setRotation(p->rotation);
        rect.setPosition(px, py);
        rect.setOrigin(p->width / 2, p->width / 2);
        window->draw(rect);
    }
}
