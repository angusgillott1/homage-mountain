#ifndef PARTICLE_H
#define PARTICLE_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <math.h>

#include "globals.h"
#include "stats.h"
#include "enums.h"

struct Particle
{
    int x = 0;
    int y = 0;
    int width = 1;
    int layer = 8;

    int xVelocity = 0;
    int yVelocity = 0;
    int friction = 1;

    int angularVelocity = 0;
    int rotation = 0;

    int lifeTick = 0;
    int lifeDuration = 0;

    bool isExpDiamond = false;
    int expValue = 0;

    sf::Color color = sf::Color::Transparent;
    sf::Color outlineColor = sf::Color::Transparent;
    sf::Color fade = sf::Color::Transparent;
};

Particle* createParticle(int x, int y, int width, int layer, int xVelocity, int yVelocity, int angularVelocity, int friction, int lifeDuration, sf::Color color, sf::Color outlineColor, sf::Color fade);
void removeParticle(Particle* particle);

void stepParticles();

void destroyAllParticles();

void renderParticleLayer(int particleLayer, sf::RenderWindow* window);

#endif // PARTICLE_H

