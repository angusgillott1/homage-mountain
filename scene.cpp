#include "scene.h"
#include "stats.h"

sf::RenderTexture interfaceTexture;
sf::Sprite interfaceSprite;

bool isMenuOpen = false;
bool isOptionsMenuOpen = false;
bool isSpellbookMenuOpen = false;

MenuItem* menuItems[100] = {0};
int numberOfMenuItems = 0;
int menuItemCursor = 0;
int menuBackgroundBlurOpacity = 60;

int numberOfOptionsMenuItems = 0;
MenuItem* optionsMenuItems[100] = {0};
int optionsMenuItemCursor = -1;

int numberOfSpellBookMenuItems = 0;
MenuItem* spellbookMenuItems[100] = {0};
int spellbookMenuItemCursor = -1;
int isSettingGameKey = -1;

int currentScene = SCENE_STARTUP;
int nextScene = SCENE_NO_SCENE; // The scene that will be switched to next after fade transition completes

int sceneTransitionState = TRANSITION_TO; // TRANSITION_FROM means fading down to black because leaving scene; TRANSITION_TO means fading up from black because entering scene
int fadeTransparency = 0; // 0 is fully black
int fadeTransparencyChangePerTick = 0; // gets calculated from fadeDuration in switchToScene. Magnitude only, no + or - implied

int exitBattleConfirmState = 0;

const int textCharacterDelay = 2; // setting
int nextCharacterAtTick = 0;
std::string displayDialogBranches[4];
std::string fullDialogBranches[4];
Entity* dialogSpeaker = 0;
int dialogCursor = 0;

void setupNextScene()
{
    clearMenuItems();

    if(currentScene == SCENE_TITLE)
    {
        playMusic("MUSIC_TITLE_SCREEN");

        addMenuItem("Continue game", doTitleSceneContinueGameMenuOutcome);
        addMenuItem("New game", doTitleSceneNewGameMenuOutcome);
        addMenuItem("Options", doOpenOptionsMenuOutcome);
        addMenuItem("Exit to desktop", doTitleSceneExitMenuOutcome);
    }

    if(currentScene == SCENE_WORLD_MAP)
    {
        playMusic("MUSIC_WORLD_MAP");

        addMenuItem("Make your stand", doWorldMapSelectLevelMenuOutcome);
        addMenuItem("Options", doOpenOptionsMenuOutcome);
        addMenuItem("Exit", doWorldMapSceneExitToTitleMenuOutcome);
    }

    if(currentScene == SCENE_BATTLE_OR_EXPLORE)
    {
        playMusic("MUSIC_BATTLE_1");

        //setupMap(retrieveGameMap("THE_TAVERN_EXPLORE_MAP"), 0);
        //setupMap(retrieveGameMap("SKY_BRIDGE_BATTLE_MAP"), retrieveSpawnMap("ARMY_OF_MUNG_SPAWN_MAP"));
        //setupMap(retrieveGameMap("SLID_BOSS_BATTLE_MAP"), 0);
        //setupMap(retrieveGameMap("INSIDE_OUTSIDE_BATTLE_MAP"), retrieveSpawnMap("ARMY_OF_MUNG_SPAWN_MAP"));
        setupMap(retrieveGameMap("TILESET_DEMO_EXPLORE_MAP"), 0);

        addMenuItem("Continue", doGeneralCloseMenuOutcome);
        //addMenuItem("Spellbook", doOpenSpellbookMenuOutcome);
        addMenuItem("Options", doOpenOptionsMenuOutcome);

        if(getCurrentMap() && getCurrentMap()->mapType == MAP_TYPE_BATTLE_MAP)
        {
            addMenuItem("Give up", doBattleSceneExitMenuOutcome);
        }
    }
}

void switchToScene(int scene, int fadeDuration)
{
    nextScene = scene;
    setWeather(WEATHER_NO_WEATHER);

    if(isMenuOpen)
    {
        fadeTransparency = 255 - menuBackgroundBlurOpacity;
        isMenuOpen = false;
    }

    sceneTransitionState = TRANSITION_FROM;
    fadeTransparencyChangePerTick = 255 / fadeDuration;
}

int getCurrentScene()
{
    return currentScene;
}

int getSceneTransitionState()
{
    return sceneTransitionState;
}

void handleGlobalInput()
{
    // Listen for key binding press

    if(isSettingGameKey != -1)
    {
        for(int i = 0; i < sf::Keyboard::Key::KeyCount; i++)
        {
            if(getSfmlKeyNiceName(i) != "NONE" && isRawSfmlKeyDownThisFrame(i))
            {
                for(int j = KEY_SPELL_1; j <= KEY_SPELL_5; j++)
                {
                    if(mapGameKeyToSfmlKey(j) == i && j != isSettingGameKey)
                    {
                        playSound(SOUND_CAST_FAIL);
                        goto SKIP_FURTHER_INPUT;  // Binding already taken
                    }
                }

                setCustomKeyBinding(isSettingGameKey, i);
                isSettingGameKey = -1;
                playSound(SOUND_MENU_OPEN);
            }
        }

        goto SKIP_FURTHER_INPUT;
    }

    // Menu sliders

    if(optionsMenuItemCursor >= 1 && optionsMenuItemCursor <= 4)
    {
        SystemOption option = (SystemOption)(optionsMenuItemCursor - 1 + OPTION_MASTER_VOLUME);

        if(isKeyDown(KEY_LEFT))
        {
            globals()->systemOptions[option] = clampLow(globals()->systemOptions[option] - 1, 0);
            resetSoundsToVolumes();
        }
        if(isKeyDown(KEY_RIGHT))
        {
            globals()->systemOptions[option] = clampHigh(globals()->systemOptions[option] + 1, 100);
            resetSoundsToVolumes();
        }

        if(isKeyReleasedThisFrame(KEY_LEFT) || isKeyReleasedThisFrame(KEY_RIGHT))
        {
            if(option == OPTION_MASTER_VOLUME || option == OPTION_SOUND_VOLUME) playSound(SOUND_FIREBALL);
            if(option == OPTION_VOICE_VOLUME) playDialogWordSound();
        }
    }

    // Open menu

    if(isKeyDownThisFrame(KEY_ESCAPE) && !isOptionsMenuOpen && !isSpellbookMenuOpen)
    {
        isMenuOpen = !isMenuOpen;
        playSound(SOUND_MENU_OPEN);
        exitBattleConfirmState = 0;
        goto SKIP_FURTHER_INPUT;
    }

    if((getCurrentScene() == SCENE_TITLE || getCurrentScene() == SCENE_WORLD_MAP) && isAnyKeyPressedThisFrame() && !isMenuOpen && !isOptionsMenuOpen && !isSpellbookMenuOpen)
    {
        isMenuOpen = true;
        playSound(SOUND_MENU_OPEN);
        goto SKIP_FURTHER_INPUT;
    }

    if(isKeyDownThisFrame(KEY_ESCAPE) && isMenuOpen && isSpellbookMenuOpen)
    {
        writeSystemOptionsToFile(); // Save options, includes key bindings
        isSpellbookMenuOpen = false;
        spellbookMenuItemCursor = -1;
        playSound(SOUND_MENU_OPEN);
        goto SKIP_FURTHER_INPUT;
    }

    if(isKeyDownThisFrame(KEY_ESCAPE) && isMenuOpen && isOptionsMenuOpen)
    {
        writeSystemOptionsToFile(); // Save options
        isOptionsMenuOpen = false;
        optionsMenuItemCursor = -1;
        playSound(SOUND_MENU_OPEN);
        goto SKIP_FURTHER_INPUT;
    }

    // Cursor movement

    if(isMenuOpen)
    {
        if(isKeyDownThisFrame(KEY_UP))
        {
            shiftMenuCursor(-1);
            exitBattleConfirmState = 0;
        }

        if(isKeyDownThisFrame(KEY_DOWN))
        {
            shiftMenuCursor(1);
            exitBattleConfirmState = 0;
        }

        if(isInteractPressedThisFrame())
            doCurrentMenuCursorOutcome();
    }

    // Dialogue cursor movement

    if(getNumberOfDisplayedDialogBranches() > 1)
    {
        if(isKeyDownThisFrame(KEY_UP))
        {
            shiftDialogCursor(-1);
        }

        if(isKeyDownThisFrame(KEY_DOWN))
        {
            shiftDialogCursor(1);
        }
    }

    SKIP_FURTHER_INPUT:

    return;
}

bool getIsMenuOpen()
{
    return isMenuOpen;
}

void initializeInterface()
{
    interfaceTexture.create(SCREEN_WIDTH, SCREEN_HEIGHT);

    interfaceSprite.setTexture(interfaceTexture.getTexture());
    interfaceSprite.setPosition(0, 0);

    int i = 0;
    {MenuItem* m = new MenuItem(); m->label = "Toggle Full Screen"; m->selectionOutcomeFunction = doToggleFullScreenMenuOutcome; optionsMenuItems[i++] = m;}
    {MenuItem* m = new MenuItem(); m->label = "Master"; m->selectionOutcomeFunction = doDummyMenuOutcome; optionsMenuItems[i++] = m;}
    {MenuItem* m = new MenuItem(); m->label = "Music"; m->selectionOutcomeFunction = doDummyMenuOutcome; optionsMenuItems[i++] = m;}
    {MenuItem* m = new MenuItem(); m->label = "Sound Effects"; m->selectionOutcomeFunction = doDummyMenuOutcome; optionsMenuItems[i++] = m;}
    {MenuItem* m = new MenuItem(); m->label = "Voice"; m->selectionOutcomeFunction = doDummyMenuOutcome; optionsMenuItems[i++] = m;}
    numberOfOptionsMenuItems = i;

    int j = 0;
    {MenuItem* m = new MenuItem(); m->label = getSpellStatsArray()[SPELL_FIREBALL].niceName; m->selectionOutcomeFunction = doSetSpell1KeyMenuOutcome; spellbookMenuItems[j++] = m;}
    {MenuItem* m = new MenuItem(); m->label = getSpellStatsArray()[SPELL_CHAIN_LIGHTNING].niceName; m->selectionOutcomeFunction = doSetSpell2KeyMenuOutcome; spellbookMenuItems[j++] = m;}
    {MenuItem* m = new MenuItem(); m->label = getSpellStatsArray()[SPELL_BLIZZARD].niceName; m->selectionOutcomeFunction = doSetSpell3KeyMenuOutcome; spellbookMenuItems[j++] = m;}
    {MenuItem* m = new MenuItem(); m->label = getSpellStatsArray()[SPELL_SCORCHING_LIGHT].niceName; m->selectionOutcomeFunction = doSetSpell4KeyMenuOutcome; spellbookMenuItems[j++] = m;}
    {MenuItem* m = new MenuItem(); m->label = getSpellStatsArray()[SPELL_TELEPORT].niceName; m->selectionOutcomeFunction = doSetSpell5KeyMenuOutcome; spellbookMenuItems[j++] = m;}
    {MenuItem* m = new MenuItem(); m->label = "Reset Bindings"; m->selectionOutcomeFunction = doResetKeysMenuOutcome; spellbookMenuItems[j++] = m;}
    {MenuItem* m = new MenuItem(); m->label = "Clear Bindings"; m->selectionOutcomeFunction = doClearKeysMenuOutcome; spellbookMenuItems[j++] = m;}
    numberOfSpellBookMenuItems = j;
}

void stepInterface()
{
    if(sceneTransitionState == TRANSITION_FROM)
    {
        fadeTransparency -= fadeTransparencyChangePerTick;

        adjustCurrentMusicVolume(0.95);

        if(fadeTransparency <= 0)
        {
            sceneTransitionState = TRANSITION_TO;
            currentScene = nextScene;
            nextScene = SCENE_NO_SCENE;

            setupNextScene();
        }
    }

    if(sceneTransitionState == TRANSITION_TO)
    {
        fadeTransparency += fadeTransparencyChangePerTick;

        if(fadeTransparency >= 255)
        {
            sceneTransitionState = TRANSITION_NOT_IN_TRANSITION;
        }
    }

    // Count dialog branches
    int nDialogBranches = 0;
    for(int i = 0; i < 4; i++)
    {
        if(fullDialogBranches[i] == "")
            break;
        nDialogBranches++;
    }

    // Figure out which dialog branch is currently being filled
    int dialogBranchIndexToFill = -1;
    for(int i = 0; i < nDialogBranches; i++)
    {
        if(fullDialogBranches[i] != displayDialogBranches[i])
        {
            dialogBranchIndexToFill = i;
            break;
        }
    }

    if(nDialogBranches > 1) // For options sets, fill instantly
    {
        for(int i = 0; i < 4; i++)
            displayDialogBranches[i] = fullDialogBranches[i];
    }
    else if(fullDialogBranches[0] != "" && dialogBranchIndexToFill != -1) // For single line, animate fill
    {
        std::string fullDialogBranchBeingFilled = fullDialogBranches[dialogBranchIndexToFill];
        std::string displayDialogBranchBeingFilled = displayDialogBranches[dialogBranchIndexToFill];

        if(globals()->worldTick >= nextCharacterAtTick)
        {
            char nextChar = fullDialogBranchBeingFilled[displayDialogBranchBeingFilled.length()];

            displayDialogBranches[dialogBranchIndexToFill] += nextChar;
            displayDialogBranchBeingFilled = displayDialogBranches[dialogBranchIndexToFill];

            nextCharacterAtTick = globals()->worldTick + textCharacterDelay;

            // Play voice SE for first character of each word
            if(displayDialogBranchBeingFilled.length() == 1 || (displayDialogBranchBeingFilled.length() > 2 && displayDialogBranchBeingFilled[displayDialogBranchBeingFilled.length() - 2] == ' '))
            {
                playDialogWordSound();
            }
        }
    }
}

void renderInterface(sf::RenderWindow* window)
{
    // setup
    interfaceTexture.clear(sf::Color (0, 0, 0, 0));

    drawText(&interfaceTexture, 0, 0, std::to_string(globals()->inferredFps));

    // Minimap
    GameMap* currentMap = getCurrentMap();
    if(currentMap != 0 && getCurrentScene() == SCENE_BATTLE_OR_EXPLORE)
    {
        const int scale = 2;
        const float opacity = 0.7;

        int width = currentMap->nTilesX * scale;
        int height = currentMap->nTilesY * scale;
        int renderX = SCREEN_WIDTH - width - 8;
        int renderY = 8;

        drawDecoratedInterfaceRectangle(&interfaceTexture, renderX, renderY, width, height, opacity);
        sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
        rectangle.setFillColor(sf::Color (255, 255, 255, (int)(255 * opacity)));

        for(int i = 0; i < currentMap->nStaticCollisionBoxes; i++)
        {
            CollisionBox* cb = currentMap->staticCollisionBoxes[i];
            int w = clampLow(round((cb->width * scale) / 16), scale);
            int h = clampLow(round((cb->height * scale) / 16), scale);
            int x = round((cb->x * scale) / 16) + renderX;
            int y = round((cb->y * scale) / 16) + renderY;

            rectangle.setSize(sf::Vector2f(w, h));
            rectangle.setPosition(sf::Vector2f(x, y));
            interfaceTexture.draw(rectangle);
        }

        Entity** allEntities = getAllEntitiesArray();
        for(int i = 0; i < globals()->numberOfEntities; i++)
        {
            Entity* e = allEntities[i];

            if(e->entityType == PLAYER) rectangle.setFillColor(sf::Color (0, 200, 0, (int)(255 * opacity)));
            if(e->entityType == NPC) rectangle.setFillColor(sf::Color (0, 150, 0, (int)(255 * opacity)));
            if(e->entityType == ENEMY) rectangle.setFillColor(sf::Color (150, 0, 0, (int)(255 * opacity)));
            if(e->entityType == INANIMATE) continue;
            if(e->entityType == SPELL) continue;
            if(e->entityType == COLLECTIBLE) rectangle.setFillColor(sf::Color (0, 255, 0, (int)(255 * opacity)));

            int w = scale;
            int h = scale;
            int x = round((e->x * scale) / 16) + renderX;
            int y = round((e->y * scale) / 16) + renderY;

            rectangle.setSize(sf::Vector2f(w, h));
            rectangle.setPosition(sf::Vector2f(x, y));
            interfaceTexture.draw(rectangle);
        }
    }

    // Dialog Box

    int nDialogBranches = getNumberOfDisplayedDialogBranches();

    if(nDialogBranches > 0)
    {
        int longestOptionCharacterCount = 0;
        for(int i = 0; i < 4; i++)
        {
            if((int)fullDialogBranches[i].length() > longestOptionCharacterCount)
                longestOptionCharacterCount = fullDialogBranches[i].length();
        }

        // Unattached to entity, display at large font and bottom left
        FontType f = LARGE_FONT;
        int w = 12 * longestOptionCharacterCount + 8;
        int h = 22 * nDialogBranches + 6 * (nDialogBranches - 1);
        int xx = 24;
        int yy = SCREEN_HEIGHT - 24 - h;
        int initialXShift = 4;
        int initialYShift = 8;
        int betweenLineYShift = 28;

        // Attached to entity, small font and positioned above entity
        if(dialogSpeaker != 0)
        {
            f = REGULAR_FONT;
            w = 6 * longestOptionCharacterCount + 8;
            h = 12 * nDialogBranches + 4 * (nDialogBranches - 1);
            xx = centerX(dialogSpeaker) - w / 2 - globals()->viewPortX;
            yy = visualTopEdge(dialogSpeaker) - h - 6 - globals()->viewPortY;
            initialYShift = 2;
            betweenLineYShift = 16;
        }

        drawDecoratedInterfaceRectangle(&interfaceTexture, xx, yy, w, h);

        xx += initialXShift;
        yy += initialYShift;

        for(int i = 0; i < nDialogBranches; i++)
        {
            sf::Color textColor = sf::Color(180,180,180,255);
            if(i == getDialogCursor()) textColor = sf::Color(255,255,255,255);

            drawText(&interfaceTexture, xx, yy, displayDialogBranches[i], f, textColor);
            yy += betweenLineYShift;
        }
    }

    // Menu

    if(!isMenuOpen || sceneTransitionState == TRANSITION_FROM)
        goto END_MENU_RENDER;

    // background blur
    {
        sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
        rectangle.setPosition(0, 0);
        rectangle.setFillColor(sf::Color (40, 40, 40, menuBackgroundBlurOpacity));
        interfaceTexture.draw(rectangle);
    }

    if(getCurrentScene() != SCENE_BATTLE_OR_EXPLORE || !getCurrentMap() || getCurrentMap()->mapType != MAP_TYPE_BATTLE_MAP)
        goto END_ENEMY_DESCRIPTIONS_RENDER;

    // enemy list
    {
        int enemyListLeftMargin = 48;
        int enemyListTopMargin = 96;

        int x = enemyListLeftMargin;
        int y = enemyListTopMargin;

        //drawText(&interfaceTexture, x, y - 50, "Foes: ", LARGE_FONT);

        drawDecoratedInterfaceRectangle(&interfaceTexture, enemyListLeftMargin - 18, enemyListTopMargin - 22, 440, 480);
//        {
//            sf::RectangleShape rectangle(sf::Vector2f(440, 480));
//            rectangle.setPosition(enemyListLeftMargin - 18, enemyListTopMargin - 22);
//            rectangle.setFillColor(sf::Color (40, 40, 40, 50));
//            interfaceTexture.draw(rectangle);
//        }

        Entity** allEntities = getAllEntitiesArray();
        std::unordered_set<TextureId> entitySpriteSheetsDisplayed;
        int numberOfEntitiesRendered = 0;
        int spriteHeightOfLargestEnemy = 0;
        int spriteWidthOfLargestEnemy = 0;

        for(int i = 0; i < globals()->numberOfEntities; i++)
        {
            Entity* e = allEntities[i];
            int h = trueHeight(e);
            int w = trueWidth(e);
            if(h > spriteHeightOfLargestEnemy)
                spriteHeightOfLargestEnemy = h;
            if(w > spriteWidthOfLargestEnemy)
                spriteWidthOfLargestEnemy = w;
        }

        for(int i = 0; i < globals()->numberOfEntities; i++)
        {
            Entity* e = allEntities[i];

            if(e->entityType != NPC && e->entityType != ENEMY) continue; // Not an enemy type
            if(entitySpriteSheetsDisplayed.find(e->spritesheetTextureId) != entitySpriteSheetsDisplayed.end()) continue;   // Already rendered this enemy type

            int xx = x + e->hitBoxClearanceLeft + (spriteWidthOfLargestEnemy - e->spriteWidth) / 2;
            int yy = y + numberOfEntitiesRendered * (spriteHeightOfLargestEnemy + 12) + 12 - e->spriteHeight / 2;

            // Final polish x offsets
            if(e->entitySubType == SUBTYPE_BRUTE) xx -= 2;
            if(e->entitySubType == SUBTYPE_SKELETON_MAGE) xx -= 4;
            if(e->entitySubType == SUBTYPE_WIZARD) xx -= 1;

            float oldRotation = e->sprite->getRotation();
            sf::Color oldColor = e->sprite->getColor();

            e->sprite->setRotation(0.0);
            if(e->entitySubType == SUBTYPE_GHOST) e->sprite->setColor(sf::Color(oldColor.r, oldColor.g, oldColor.b, 200));
            e->sprite->setPosition(xx, yy);
            e->sprite->setTextureRect(sf::IntRect(e->spriteWidth * ((globals()->globalTick / 12) % 3), 0, e->spriteWidth, e->spriteHeight));
            interfaceTexture.draw(*(e->sprite));
            e->sprite->setRotation(oldRotation);
            if(e->entitySubType == SUBTYPE_GHOST) e->sprite->setColor(oldColor);

            numberOfEntitiesRendered++;
            entitySpriteSheetsDisplayed.insert(e->spritesheetTextureId);

            drawText(&interfaceTexture, x + spriteWidthOfLargestEnemy + 16, yy + trueHeight(e) / 2, getEntitySubtypeDescription(e->entitySubType));
        }
    }

    {
        sf::Sprite spellIconSprite;
        sf::Sprite arrowSprite;

        int spellListRightMargin = 36;
        int spellListTopMargin = 96;
        int spellListWidth = 600;
        int spellListHeight = 440;
        int spellIconWidth = 16;
        int spellItemBottomMargin = 6;
        int gapBetweenIconAndArrows = 6;
        int gapBetweenArrows = 4;
        int arrowWidth = 10;
        int characterTrueWidth = 5;
        int characterTrueXGap = 1;

        int x = SCREEN_WIDTH - spellListRightMargin - spellListWidth;
        int y = spellListTopMargin;

        sf::Texture* upArrowTexture = retrieveTexture(TEXTURE_UP_ARROW_LARGE);
        sf::Texture* downArrowTexture = retrieveTexture(TEXTURE_DOWN_ARROW_LARGE);
        sf::Texture* leftArrowTexture = retrieveTexture(TEXTURE_LEFT_ARROW_LARGE);
        sf::Texture* rightArrowTexture = retrieveTexture(TEXTURE_RIGHT_ARROW_LARGE);

        drawDecoratedInterfaceRectangle(&interfaceTexture, x - 18, y - 22, spellListWidth, spellListHeight);

        int j = 0;
        for(int i = 0; i < 100; i++)
        {
            if(globals()->allLearnedSpells[i])
            {
                SpellStats spellStats = getSpellStatsArray()[i];
                sf::Texture* spellIconTexture = retrieveTexture(spellStats.spellIcon);
                std::string spellCastLetters = spellStats.castString;
                std::string spellNiceName = spellStats.niceName;

                int xx = x;
                int yy = y;

                spellIconSprite.setTexture(*spellIconTexture);
                spellIconSprite.setPosition(xx, yy);
                interfaceTexture.draw(spellIconSprite);

                xx += spellIconWidth + gapBetweenIconAndArrows;
                drawText(&interfaceTexture, xx, yy + 4, spellNiceName);

                xx += characterTrueWidth * spellNiceName.length() + characterTrueXGap * (spellNiceName.length() - 1) + gapBetweenIconAndArrows * 2;
                yy += 3;

                if(spellStats.niceName != "Teleport")
                {
                    for(int k = 0; k < (int)spellCastLetters.length(); k++)
                    {
                        char d = spellCastLetters[k];
                        sf::Texture* letterTexture = 0;

                        if(d == 'U') letterTexture = upArrowTexture;
                        if(d == 'D') letterTexture = downArrowTexture;
                        if(d == 'L') letterTexture = leftArrowTexture;
                        if(d == 'R') letterTexture = rightArrowTexture;

                        arrowSprite.setTexture(*letterTexture);
                        arrowSprite.setPosition(xx, yy);
                        interfaceTexture.draw(arrowSprite);
                        xx += arrowWidth + gapBetweenArrows;
                    }
                }
                else
                {
                    drawText(&interfaceTexture, xx, yy, "LShift", REGULAR_FONT);

                    // Special case for teleport, because it isn't input by spell strokes (it is a keyboard key)

                    sf::RectangleShape rectangle(sf::Vector2f(41, 12));
                    rectangle.setPosition(xx - 2, yy - 2);
                    rectangle.setFillColor(sf::Color (0,0,0,0));
                    rectangle.setOutlineColor(sf::Color(255,255,255,255));
                    rectangle.setOutlineThickness(1.0f);
                    interfaceTexture.draw(rectangle);
                }

                xx = x + 180;
                drawText(&interfaceTexture, xx, yy, spellStats.spellDescription, REGULAR_FONT);

                y += spellIconWidth + spellItemBottomMargin;
                j++;
            }
        }

        // Render spell instructions

//        x = spellListLeftMargin + secondColumnOffset * 2 + 48;
//        y = spellListTopMargin;
//
//        drawText(&interfaceTexture, x, y + 4, "To cast:", LARGE_FONT); y += 20;
//        drawText(&interfaceTexture, x, y + 4, "1. Press and hold 'z'"); y += 14;
//        drawText(&interfaceTexture, x, y + 4, "2. Press arrow key combo"); y += 14;
//        drawText(&interfaceTexture, x, y + 4, "3. Release 'z'");
    }


    END_ENEMY_DESCRIPTIONS_RENDER:

    // bottom right menu pane with menu items
    if(!isOptionsMenuOpen)
    {
        const int characterTrueWidth = 10;
        const int characterTrueXGap = 2;
        const int textTrueHeight = 12;
        const int menuPaneMargin = 28;
        const int menuPaneOuterPadding = 10;
        const int menuPaneInnerPadding = 12;
        const int longestMenuItemLength = 15;

        int menuPaneWidth = longestMenuItemLength * characterTrueWidth + (longestMenuItemLength - 1) * characterTrueXGap + menuPaneOuterPadding * 2;
        int menuPaneHeight = numberOfMenuItems * textTrueHeight + menuPaneOuterPadding * 2 + (numberOfMenuItems - 1) * menuPaneInnerPadding;

        int x = SCREEN_WIDTH - (menuPaneWidth + menuPaneMargin);
        int y = SCREEN_HEIGHT - (menuPaneHeight + menuPaneMargin);

        drawDecoratedInterfaceRectangle(&interfaceTexture, x, y, menuPaneWidth, menuPaneHeight);

        x += menuPaneOuterPadding;
        y += menuPaneOuterPadding;

        for(int i = 0; i < numberOfMenuItems; i++)
        {
            sf::Color textColor = sf::Color(180,180,180,255);

            if(!isOptionsMenuOpen)
            {
                if(menuItemCursor == i)
                {
                    textColor = sf::Color(255,255,255,255);
                }
            }

            drawText(&interfaceTexture, x, y + 2, menuItems[i]->label, LARGE_FONT, textColor);

            if(menuItems[i]->selectionOutcomeFunction == doBattleSceneExitMenuOutcome && exitBattleConfirmState == 1)
            {
                drawText(&interfaceTexture, x + 100, y + 2, "Are you sure?", REGULAR_FONT);
            }

            y += textTrueHeight + menuPaneInnerPadding;
        }
    }

    // options menu pane
    if(isOptionsMenuOpen)
    {
        const int largeCharacterTrueWidth = 10;
        const int largeCharacterTrueXGap = 2;
        const int smallCharacterTrueWidth = 5;
        const int smallCharacterTrueXGap = 1;
        const int largeTextTrueHeight = 20;
        const int menuPaneMargin = 28;
        const int subMenuLeftPadding = 6;
        const int menuPaneSectionPadding = 60;
        const int menuItemCursorPadding = 6;

        const int menuPaneWidth = 460;
        const int menuPaneHeight = 360;

        int x = (SCREEN_WIDTH - menuPaneWidth) / 2;
        int y = (SCREEN_HEIGHT - menuPaneHeight) / 2;
        int xx;
        int yy;

        // background blur
        {
            sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
            rectangle.setPosition(0, 0);
            rectangle.setFillColor(sf::Color (40, 40, 40, menuBackgroundBlurOpacity));
            interfaceTexture.draw(rectangle);
        }

        drawDecoratedInterfaceRectangle(&interfaceTexture, x, y, menuPaneWidth, menuPaneHeight);

        // Options title
        {
            std::string label = "Options";
            xx = x + menuPaneWidth / 2 - (largeCharacterTrueWidth + largeCharacterTrueXGap) * 6 - 1;
            yy = y + menuPaneMargin;

            drawText(&interfaceTexture, xx, yy, label, LARGE_FONT);

            sf::RectangleShape rectangle(sf::Vector2f((largeCharacterTrueWidth + largeCharacterTrueXGap) * 7 - 6, 2));
            rectangle.setPosition(xx - 1, yy + largeTextTrueHeight - 5);
            rectangle.setFillColor(sf::Color (255, 255, 255, 255));
            interfaceTexture.draw(rectangle);
        }

        // Top close message
        {
            std::string label = "Esc to close";
            xx = x + menuPaneWidth - menuPaneMargin / 2 - (smallCharacterTrueWidth + smallCharacterTrueXGap) * label.length() - 1 - menuItemCursorPadding;

            drawText(&interfaceTexture, xx, yy, label, REGULAR_FONT);
        }

        // Full screen toggle option
        {
            {
                std::string label = "Display";
                xx = x + menuPaneMargin;
                yy += menuPaneMargin + 22;

                drawText(&interfaceTexture, xx, yy, label, LARGE_FONT);
                yy += largeTextTrueHeight / 2;
            }

            {
                int i = 0;

                std::string label = optionsMenuItems[i]->label;
                xx += subMenuLeftPadding;
                yy += menuItemCursorPadding + largeTextTrueHeight;

                sf::Color textColor = sf::Color(180,180,180,255);

                if(optionsMenuItemCursor == i)
                {
                    textColor = sf::Color(255,255,255,255);
                }

                drawText(&interfaceTexture, xx, yy, label, REGULAR_FONT, textColor);

                std::string onOff = "(Off)";
                if(globals()->systemOptions[OPTION_FULL_SCREEN]) onOff = "(On)";
                drawText(&interfaceTexture, xx + 156, yy, onOff, REGULAR_FONT, sf::Color(180,180,180,255));
            }
        }

        // Volume sliders
        {
            {
                std::string label = "Volume";
                xx -= subMenuLeftPadding;
                yy += menuPaneSectionPadding;

                drawText(&interfaceTexture, xx, yy, label, LARGE_FONT);
                yy += largeTextTrueHeight / 2;
            }

            xx += subMenuLeftPadding;
            for(int i = 1; i <= 4; i++)
            {
                std::string label = optionsMenuItems[i]->label;
                yy += menuItemCursorPadding + largeTextTrueHeight;

                sf::Color textColor = sf::Color(180,180,180,255);

                if(optionsMenuItemCursor == i)
                {
                    textColor = sf::Color(255,255,255,255);
                }

                drawText(&interfaceTexture, xx, yy, label, REGULAR_FONT, textColor);

                const int volumeBarWidth = 200;
                const int volumeBarHeight = 3;
                const int volumeBarSliderHeight = 9;
                const int volumeBarSliderWidth = 4;

                xx += 128;
                int c0 = 0; if(i == optionsMenuItemCursor) c0 = 40;
                int c1 = 180 + c0;
                int c2 = 215 + c0;
                int c3 = 130 + c0;
                sf::RectangleShape rectangle(sf::Vector2f(volumeBarWidth, volumeBarHeight));
                rectangle.setPosition(xx, yy + 3);
                rectangle.setFillColor(sf::Color (c1, c1, c1, 255));
                interfaceTexture.draw(rectangle);
                rectangle.setPosition(xx + (int)(((float)(globals()->systemOptions[i - 1 + OPTION_MASTER_VOLUME]) / 100.0) * volumeBarWidth) - volumeBarSliderWidth / 2, yy);
                rectangle.setSize(sf::Vector2f(volumeBarSliderWidth, volumeBarSliderHeight));
                rectangle.setFillColor(sf::Color (c2, c2, c2, 255));
                rectangle.setOutlineColor(sf::Color (c3,c3,c3, 255));
                rectangle.setOutlineThickness(1.0f);
                interfaceTexture.draw(rectangle);
                xx -= 128;
            }
        }
    }

    // Spellbook menu pane
    if(isSpellbookMenuOpen)
    {
        const int largeCharacterTrueWidth = 10;
        const int largeCharacterTrueXGap = 2;
        const int smallCharacterTrueWidth = 5;
        const int smallCharacterTrueXGap = 1;
        const int smallTextTrueHeight = 12;
        const int largeTextTrueHeight = 20;
        const int menuPaneMargin = 28;
        const int menuItemCursorPadding = 6;
        const int spellIconWidth = 16;
        const int gapBetweenIconAndName = 16;
        const int gapBetweenKeyAndDescription = 128;

        const int menuPaneWidth = 720;
        const int menuPaneHeight = 420;

        int x = (SCREEN_WIDTH - menuPaneWidth) / 2;
        int y = (SCREEN_HEIGHT - menuPaneHeight) / 2;
        int xx;
        int yy;

        sf::Sprite spellIconSprite;

        drawDecoratedInterfaceRectangle(&interfaceTexture, x, y, menuPaneWidth, menuPaneHeight);

        // Pane title
        {
            std::string label = "Spellbook";
            xx = x + menuPaneWidth / 2 - (largeCharacterTrueWidth + largeCharacterTrueXGap) * 6 - 1;
            yy = y + menuPaneMargin;

            drawText(&interfaceTexture, xx, yy, label, LARGE_FONT);

            sf::RectangleShape rectangle(sf::Vector2f((largeCharacterTrueWidth + largeCharacterTrueXGap) * 9 - 10, 2));
            rectangle.setPosition(xx - 1, yy + largeTextTrueHeight - 5);
            rectangle.setFillColor(sf::Color (255, 255, 255, 255));
            interfaceTexture.draw(rectangle);
        }

        // Top close message
        {
            std::string label = "Esc to close";
            xx = x + menuPaneWidth - menuPaneMargin / 2 - (smallCharacterTrueWidth + smallCharacterTrueXGap) * label.length() - 1 - menuItemCursorPadding;

            drawText(&interfaceTexture, xx, yy, label, REGULAR_FONT);
        }

        // Spell controls
        {
            xx = x + menuPaneMargin;
            yy += menuPaneMargin + 22;

            const int numSpells = 5;

            for(int i = 0; i < numberOfSpellBookMenuItems; i++)
            {
                sf::Color c = sf::Color(180,180,180,255);

                if(i < numSpells)
                {
                    SpellStats spellStats = getSpellStatsArray()[i + 1];
                    sf::Texture* spellIconTexture = retrieveTexture(spellStats.spellIcon);

                    spellIconSprite.setTexture(*spellIconTexture);
                    spellIconSprite.setPosition(xx, yy - 4);
                    interfaceTexture.draw(spellIconSprite);
                }

                xx += spellIconWidth + gapBetweenIconAndName;

                std::string label = spellbookMenuItems[i]->label;

                if(i == numSpells)
                    yy += 12;   // Offset for "reset bindings" button

                if((spellbookMenuItemCursor == i && i >= numSpells) || i < numSpells)
                {
                    c = sf::Color(255,255,255,255);
                }

                drawText(&interfaceTexture, xx, yy, label, REGULAR_FONT, c);

                if((spellbookMenuItemCursor != i && i < numSpells))
                {
                    c = sf::Color(180,180,180,255);
                }

                if(i < numSpells)
                {
                    xx += 128;
                    int gameKeyIndex = i + KEY_SPELL_1;
                    std::string keyDescription = getSfmlKeyNiceName(mapGameKeyToSfmlKey(gameKeyIndex));
                    if(isSettingGameKey == gameKeyIndex)
                    {
                        keyDescription = "Press a key";
                    }

                    drawText(&interfaceTexture, xx, yy, keyDescription, REGULAR_FONT, c);

                    sf::RectangleShape rectangle(sf::Vector2f((smallCharacterTrueWidth + smallCharacterTrueXGap) * keyDescription.length() + 5, smallTextTrueHeight));
                    rectangle.setPosition(xx - 2, yy - 2);
                    rectangle.setFillColor(sf::Color (0,0,0,0));
                    rectangle.setOutlineColor(c);
                    rectangle.setOutlineThickness(1.0f);
                    interfaceTexture.draw(rectangle);

                    xx += gapBetweenKeyAndDescription;
                    SpellStats spellStats = getSpellStatsArray()[i + 1];
                    std::string description = spellStats.spellDescription;
                    drawText(&interfaceTexture, xx, yy, description, REGULAR_FONT);
                    xx -= gapBetweenKeyAndDescription;
                    xx -= 128;
                }

                xx -= spellIconWidth + gapBetweenIconAndName;
                yy += menuItemCursorPadding + largeTextTrueHeight;
            }
        }
    }

    END_MENU_RENDER:

    // screen fade in/out blur
    if(sceneTransitionState == TRANSITION_FROM || sceneTransitionState == TRANSITION_TO)
    {
        sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
        rectangle.setPosition(0, 0);
        rectangle.setFillColor(sf::Color (0, 0, 0, 255 - fadeTransparency));
        interfaceTexture.draw(rectangle);
    }

    // done
    interfaceTexture.display(); // This must be called otherwise y-axis is flipped. Reason unclear.
    window->draw(interfaceSprite);
}

void shiftMenuCursor(int step)
{
    int m = menuItemCursor;
    int o = optionsMenuItemCursor;
    int s = spellbookMenuItemCursor;

    if(!isOptionsMenuOpen && !isSpellbookMenuOpen)
        menuItemCursor = clampLow(clampHigh(menuItemCursor + step, numberOfMenuItems - 1), 0);
    else if(isOptionsMenuOpen)
        optionsMenuItemCursor = clampLow(clampHigh(optionsMenuItemCursor + step, numberOfOptionsMenuItems - 1), 0);
    else
        spellbookMenuItemCursor = clampLow(clampHigh(spellbookMenuItemCursor + step, numberOfSpellBookMenuItems - 1), 0);

    if(menuItemCursor != m || optionsMenuItemCursor != o || spellbookMenuItemCursor != s)
        playSound(SOUND_MENU_OPEN);
}

void doCurrentMenuCursorOutcome()
{
    void (*selectionOutcomeFunction) () = 0;

    if(isOptionsMenuOpen)
        selectionOutcomeFunction = optionsMenuItems[optionsMenuItemCursor]->selectionOutcomeFunction;
    else if(isSpellbookMenuOpen)
        selectionOutcomeFunction = spellbookMenuItems[spellbookMenuItemCursor]->selectionOutcomeFunction;
    else
        selectionOutcomeFunction = menuItems[menuItemCursor]->selectionOutcomeFunction;

    if(selectionOutcomeFunction != 0)
    {
        selectionOutcomeFunction();
    }
}

void addMenuItem(std::string label, void (*selectionOutcomeFunction) ())
{
    MenuItem* item = new MenuItem();

    item->label = label;
    item->selectionOutcomeFunction = selectionOutcomeFunction;
    menuItems[numberOfMenuItems] = item;

    numberOfMenuItems++;
}

void clearMenuItems()
{
    for(int i = 0; menuItems[i] != 0; i++)
    {
        delete menuItems[i];
        menuItems[i] = 0;
    }

    numberOfMenuItems = 0;
    menuItemCursor = 0;
}

void doDummyMenuOutcome()
{

}

void doTitleSceneNewGameMenuOutcome()
{
    switchToScene(SCENE_WORLD_MAP);
}

void doTitleSceneContinueGameMenuOutcome()
{
    switchToScene(SCENE_WORLD_MAP);
}

void doTitleSceneExitMenuOutcome()
{
    globals()->shouldQuitImmediately = true;
}

void doWorldMapSelectLevelMenuOutcome()
{
    switchToScene(SCENE_BATTLE_OR_EXPLORE);
}

void doWorldMapSceneExitToTitleMenuOutcome()
{
    switchToScene(SCENE_TITLE);
}

void doBattleSceneExitMenuOutcome()
{
    if(exitBattleConfirmState == 0)
    {
        exitBattleConfirmState = 1;
        playSound(SOUND_MENU_OPEN);
    }
    else if(exitBattleConfirmState == 1)
    {
        switchToScene(SCENE_WORLD_MAP);
        exitBattleConfirmState = 0;
    }
}

void doGeneralCloseMenuOutcome()
{
    isMenuOpen = false;
}

void doOpenOptionsMenuOutcome()
{
    optionsMenuItemCursor = 0;
    isOptionsMenuOpen = true;
}

void doOpenSpellbookMenuOutcome()
{
    spellbookMenuItemCursor = 0;
    isSpellbookMenuOpen = true;
}

void doToggleFullScreenMenuOutcome()
{
    globals()->systemOptions[OPTION_FULL_SCREEN] = !globals()->systemOptions[OPTION_FULL_SCREEN];
}

void doSetKeyMenuOutcome(GameKey key)
{
    isSettingGameKey = key;
    playSound(SOUND_MENU_OPEN);
}

void doResetKeysMenuOutcome()
{
    isSettingGameKey = -1;
    playSound(SOUND_MENU_OPEN);

    for(int i = KEY_SPELL_1; i <= KEY_SPELL_5; i++)
    {
        setCustomKeyBinding(i, -1);
    }
}

void doClearKeysMenuOutcome()
{
    isSettingGameKey = -1;
    playSound(SOUND_MENU_OPEN);

    for(int i = KEY_SPELL_1; i <= KEY_SPELL_5; i++)
    {
        setCustomKeyBinding(i, -2);
    }
}

void doSetSpell1KeyMenuOutcome() {doSetKeyMenuOutcome(KEY_SPELL_1);}
void doSetSpell2KeyMenuOutcome() {doSetKeyMenuOutcome(KEY_SPELL_2);}
void doSetSpell3KeyMenuOutcome() {doSetKeyMenuOutcome(KEY_SPELL_3);}
void doSetSpell4KeyMenuOutcome() {doSetKeyMenuOutcome(KEY_SPELL_4);}
void doSetSpell5KeyMenuOutcome() {doSetKeyMenuOutcome(KEY_SPELL_5);}

void renderCurrentScene(sf::RenderWindow* window)
{
    if(currentScene == SCENE_TITLE) renderTitleScene(window);
    if(currentScene == SCENE_WORLD_MAP) renderWorldMapScene(window);
    if(currentScene == SCENE_BATTLE_OR_EXPLORE) renderBattleScene(window);
}

void renderTitleScene(sf::RenderWindow* window)
{
    sf::Sprite backgroundSprite;

    backgroundSprite.setTexture(*(retrieveTexture(TEXTURE_TITLE_SCREEN_BACKGROUND)));
    backgroundSprite.setPosition(0, 0);
    window->draw(backgroundSprite);

    renderInterface(window);
}

void renderWorldMapScene(sf::RenderWindow* window)
{
    sf::Sprite backgroundSprite;

    backgroundSprite.setTexture(*(retrieveTexture(TEXTURE_WORLD_MAP_BACKGROUND)));
    backgroundSprite.setPosition(0, 0);
    window->draw(backgroundSprite);

    renderInterface(window);
}

void renderBattleScene(sf::RenderWindow* window)
{
    globals()->viewPortX = globals()->player->x - SCREEN_WIDTH / 2;
//    globals()->viewPortX = std::max(globals()->viewPortX, 0);
//    globals()->viewPortX = std::min(globals()->viewPortX, globals()->worldWidth - SCREEN_WIDTH);

    globals()->viewPortY = globals()->player->y - SCREEN_HEIGHT / 2;
//    globals()->viewPortY = std::max(globals()->viewPortY, -(SCREEN_HEIGHT / 4));
//    globals()->viewPortY = std::min(globals()->viewPortY, globals()->worldHeight - SCREEN_HEIGHT);
//    globals()->viewPortX = 0;
//    globals()->viewPortY = 0;

    renderMapWithEntities(window);

    if(getCurrentMap() && getCurrentMap()->mapType == MAP_TYPE_BATTLE_MAP)
    {
        renderPlayerStatBars(window);
        renderSpellInput(window);
    }

    renderFloatingTexts(window);
    renderInterface(window);
    //renderStaticCollisionOverlay(window);
}

void drawDecoratedInterfaceRectangle(sf::RenderTarget* renderTarget, int x, int y, int width, int height, float opacity)
{
    sf::Color baseMenuPaneColor = sf::Color(100, 100, 160, (int)(255 * opacity));
    sf::Color borderColorBright = sf::Color(130, 130, 190, (int)(255 * opacity));
    //sf::Color borderColorDull = sf::Color(110, 110, 170, (int)(255* opacity));

    sf::ConvexShape roundedRectangle;

    roundedRectangle.setPointCount(8);

    roundedRectangle.setPoint(0, sf::Vector2f(1, -1));
    roundedRectangle.setPoint(1, sf::Vector2f(width - 1, -1));
    roundedRectangle.setPoint(2, sf::Vector2f(width + 1, 1));
    roundedRectangle.setPoint(3, sf::Vector2f(width + 1, height - 1));
    roundedRectangle.setPoint(4, sf::Vector2f(width - 1, height + 1));
    roundedRectangle.setPoint(5, sf::Vector2f(1, height + 1));
    roundedRectangle.setPoint(6, sf::Vector2f(-1, height - 1));
    roundedRectangle.setPoint(7, sf::Vector2f(-1, 1));

    // Drop shadow
    if(opacity == 1.0)
    {
        roundedRectangle.setFillColor(sf::Color(100,100,100,50));
        roundedRectangle.setPosition(x + clampLow(height / 30, 2), y + (clampLow(height / 30, 2)));
        renderTarget->draw(roundedRectangle);
    }

    // Front rectangle
    roundedRectangle.setFillColor(baseMenuPaneColor);
    roundedRectangle.setOutlineThickness(1.0f);
    roundedRectangle.setOutlineColor(borderColorBright);
    roundedRectangle.setPosition(x, y);
    renderTarget->draw(roundedRectangle);
}

void displayDialog(std::string branches[], Entity* speaker)
{
    dialogSpeaker = speaker;

    for(int i = 0; i < 4; i++)
    {
        fullDialogBranches[i] = branches[i];
        displayDialogBranches[i] = "";
    }

    dialogCursor = 0;
}

void closeDialogDisplay()
{
    dialogSpeaker = 0;

    for(int i = 0; i < 4; i++)
    {
        fullDialogBranches[i] = "";
        displayDialogBranches[i] = "";
    }
}

int getDialogCursor()
{
    return dialogCursor;
}

void shiftDialogCursor(int step)
{
    int prev = dialogCursor;
    dialogCursor = clampLow(clampHigh(dialogCursor + step, getNumberOfDisplayedDialogBranches() - 1), 0);

    if(dialogCursor != prev)
        playSound(SOUND_MENU_OPEN);
}

int getNumberOfDisplayedDialogBranches()
{
    int nDisplayedDialogBranches = 0;
    for(int i = 0; i < 4; i++)
    {
        if(fullDialogBranches[i] == "")
            break;

        nDisplayedDialogBranches++;
    }

    return nDisplayedDialogBranches;
}
