#ifndef SCENE_H
#define SCENE_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <math.h>

#include "globals.h"
#include "spell.h"
#include "assets.h"
#include "utils.h"
#include "enums.h"
#include "input.h"

//---------
// Scene management
//---------

void switchToScene(int scene, int fadeDuration = 30);
void setupNextScene();
int getCurrentScene();
int getSceneTransitionState();

//----------
// Menu interface
//----------

struct MenuItem
{
    std::string label = "";
    void (*selectionOutcomeFunction) () = 0;
};

void handleGlobalInput();

bool getIsMenuOpen();

void initializeInterface();
void stepInterface();
void renderInterface(sf::RenderWindow* window);

void shiftMenuCursor(int step);

void doCurrentMenuCursorOutcome();

void addMenuItem(std::string label, void (*selectionOutcomeFunction) ());
void clearMenuItems();

//---------
// Menu outcome functions
//---------

void doDummyMenuOutcome();

void doTitleSceneNewGameMenuOutcome();
void doTitleSceneContinueGameMenuOutcome();
void doTitleSceneExitMenuOutcome();

void doWorldMapSelectLevelMenuOutcome();
void doWorldMapSceneExitToTitleMenuOutcome();

void doBattleSceneExitMenuOutcome();
void doGeneralCloseMenuOutcome();

void doOpenOptionsMenuOutcome();

void doOpenSpellbookMenuOutcome();

void doToggleFullScreenMenuOutcome();

void doSetKeyMenuOutcome();
void doResetKeysMenuOutcome();
void doClearKeysMenuOutcome();
void doSetSpell1KeyMenuOutcome();
void doSetSpell2KeyMenuOutcome();
void doSetSpell3KeyMenuOutcome();
void doSetSpell4KeyMenuOutcome();
void doSetSpell5KeyMenuOutcome();
void doSetSpell6KeyMenuOutcome();
void doSetSpell7KeyMenuOutcome();
void doSetSpell8KeyMenuOutcome();
void doSetSpell9KeyMenuOutcome();

//-----------
// Scene renders
//-----------

void renderCurrentScene(sf::RenderWindow* window);
void renderTitleScene(sf::RenderWindow* window);
void renderWorldMapScene(sf::RenderWindow* window);
void renderBattleScene(sf::RenderWindow* window);

void drawDecoratedInterfaceRectangle(sf::RenderTarget* renderTarget, int x, int y, int width, int height, float opacity = 1.0);

//-----------
// Dialogue box management
//-----------

void displayDialog(std::string branches[], Entity* speaker);
void closeDialogDisplay();
int getDialogCursor();
void shiftDialogCursor(int step);
int getNumberOfDisplayedDialogBranches();

#endif
