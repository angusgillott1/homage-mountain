#include "spell.h"

//---------------
// Local globals
//---------------

sf::Texture spellInputTexture;
sf::Sprite spellInputSprite;

sf::Image emptySpellInputImage;

const int MAX_SPELL_STROKES = 8;
int spellStrokesLength = 0;
int spellStrokes[MAX_SPELL_STROKES] = {0};

const int CAST_ARROW_SIZE = 5;

SpellStats spellStats[100];

const int SPELL_COOLDOWN = 5;
int spellCooldowns[100] = {0};

SpellStats* getSpellStatsArray()
{
    return spellStats;
}

SpellName translateStrokesToSpell(int strokes[], int numStrokes)
{
    if(numStrokes == 0)
        return SPELL_NOSPELL;

    std::string spellString = "";

    for(int i = 0; i < numStrokes; i++)
    {
        char stroke = strokes[i];
        if(stroke == 1) spellString += "U";
        if(stroke == 2) spellString += "L";
        if(stroke == 3) spellString += "D";
        if(stroke == 4) spellString += "R";
        if(stroke > 4 || stroke <= 0) spellString += "N";
    }

    for(SpellName i = (SpellName)(SPELL_NOSPELL + 1); i != SPELL_ENUM_END; i = (SpellName)(i+1))
    {
        if(spellString == spellStats[i].castString)
            return i;
    }

    return SPELL_NOSPELL;
}

void addSpellStroke(int dir)
{
    if(spellStrokesLength < MAX_SPELL_STROKES)
    {
        spellStrokes[spellStrokesLength] = dir;
        makeEntityFlash(globals()->player, 10, sf::Color(140 + rand() % 20, 100 + rand() % 20, 255, 200), sf::Color(0,0,0,28));
        spellStrokesLength++;
    }
}

void dischargeSpellInput()
{
    SpellName spell = translateStrokesToSpell(spellStrokes, spellStrokesLength);

    if(globals()->allLearnedSpells[spell])
    {
        incantSpell(spell);
    }

    spellStrokesLength = 0;
    spellInputTexture.loadFromImage(emptySpellInputImage);
}

void incantSpell(SpellName spell)
{
    if(spell == SPELL_NOSPELL)
        return;

    int cost = spellStats[spell].mpCost;

    int spellCooldownArrayIndex = (int)spell - (int)SPELL_TELEPORT;

    if(spellCooldowns[spellCooldownArrayIndex] > 0)
        return;

    if(cost <= globals()->playerStats->playerMp && !(isEntityImmobilized(globals()->player) && spell != SPELL_TELEPORT))
    {
        castSpell(globals()->player, spell);
        spellCooldowns[spellCooldownArrayIndex] = SPELL_COOLDOWN;
        globals()->playerStats->playerMp -= cost;
    }
    else
    {
        playSoundAtEntityLocation(SOUND_CAST_FAIL);
        makeEntityFlash(globals()->player, 30, sf::Color(180,180,180), sf::Color(0,0,0,10));
    }
}

int spellStrokesInBuffer()
{
    return spellStrokesLength;
}

void renderSpellInput(sf::RenderWindow* window)
{
    const int X_OFFSET = 7;
    const int X_GAP = 1;
    const int Y_OFFSET = 3 + CAST_ARROW_SIZE;

    for(int i = 0; i < spellStrokesLength; i++)
    {
        TextureId textureId = TEXTURE_NONE;
        int dir = spellStrokes[i];

        if(dir == 1)
            textureId = TEXTURE_UP_ARROW;
        if(dir == 2)
            textureId = TEXTURE_LEFT_ARROW;
        if(dir == 3)
            textureId = TEXTURE_DOWN_ARROW;
        if(dir == 4)
            textureId = TEXTURE_RIGHT_ARROW;

        spellInputTexture.update(*retrieveTexture(textureId), i * (CAST_ARROW_SIZE + X_GAP), 0);
    }

    int x = globals()->player->x - globals()->player->hitBoxClearanceLeft - X_OFFSET - globals()->viewPortX;
    int y = globals()->player->y - globals()->player->hitBoxClearanceTop - Y_OFFSET - globals()->viewPortY;
    spellInputSprite.setPosition(x, y);
    window->draw(spellInputSprite);
}

void initializeSpellInputGraphics()
{
    emptySpellInputImage.loadFromFile("assets/Graphics/Sprites/SpellBlank.png");
    spellInputTexture.create((CAST_ARROW_SIZE + 1) * MAX_SPELL_STROKES, CAST_ARROW_SIZE);
    spellInputSprite.setTexture(spellInputTexture);
}

void initializeSpells()
{
    initializeOneSpell(SPELL_TELEPORT, TEXTURE_TELEPORT_SPELL_ICON, "DDDDDDDD", "Teleport", "Warp a short distance forward.", 0, 0, 6, teleportAI);
    initializeOneSpell(SPELL_FIREBALL, TEXTURE_FIREBALL_SPELL_ICON, "UU", "Fireball", "Hurl a handful of fire.", 9, 6, 5, fireballAI, fireballCollision);
    initializeOneSpell(SPELL_CHAIN_LIGHTNING, TEXTURE_CHAIN_LIGHTNING_SPELL_ICON, "UL", "Chain Lightning", "Shoot a lightning bolt that forks between enemies.", 15, 18, 7, chainLightningAI);
    initializeOneSpell(SPELL_BLIZZARD, TEXTURE_BLIZZARD_SPELL_ICON, "DD", "Blizzard", "Create a blast of ice shards.", 2, 24, 3, blizzardAI);
    initializeOneSpell(SPELL_SCORCHING_LIGHT, TEXTURE_SCORCHING_LIGHT_SPELL_ICON, "RL", "Scorching Light", "Shoot a beam of light that is stronger at greater distance.", 35, 12, 3, scorchingLightAI);
    initializeOneSpell(SPELL_WARMTH, TEXTURE_WARMTH_SPELL_ICON, "ULR", "Warmth", "Recover a small amount of health over time.", 20, 6, 6, warmthAI);
    initializeOneSpell(SPELL_SHOCKWAVE, TEXTURE_SHOCKWAVE_SPELL_ICON, "UD", "Shockwave", "Slam the ground with a strike that knocks down enemies.", 6, 24, 3, shockwaveAI);
    initializeOneSpell(SPELL_JESTERS_BLADE, TEXTURE_JESTERS_BLADE_SPELL_ICON, "ULUL", "Jester's Blade", "Conjure a sword and control it from afar.", 2, 7, 7, jestersBladeAI, jestersBladeCollision);
    initializeOneSpell(SPELL_HOMING_FIREBALLS, TEXTURE_HOMING_FIREBALLS_SPELL_ICON, "RUU", "Homing Fireballs", "Fireballs that continually seek enemies.", 9, 80, 3, homingFireballsAI);

    int availableSpells[] = { SPELL_TELEPORT, SPELL_FIREBALL, SPELL_CHAIN_LIGHTNING, SPELL_BLIZZARD, SPELL_SCORCHING_LIGHT, SPELL_WARMTH, SPELL_SHOCKWAVE, SPELL_JESTERS_BLADE, SPELL_HOMING_FIREBALLS };

    for(int i = 0; i < 9; i++) globals()->allLearnedSpells[availableSpells[i]] = true;
}

void initializeOneSpell(SpellName spell, TextureId spellIcon, std::string spellString, std::string niceName, std::string spellDescription, int baseDamage, int mpCost, int layer, void (*aiFunction) (Entity*), void (*collisionFunction)(Entity* thisGuy, Entity* thatGuy))
{
    SpellStats* sp = &spellStats[spell];

    sp->spellIcon = spellIcon;
    sp->castString = spellString;
    sp->niceName = niceName;
    sp->spellDescription = spellDescription;
    sp->baseDamage = baseDamage;
    sp->mpCost = mpCost;
    sp->layer = layer;
    sp->aiFunction = aiFunction;
    sp->collisionFunction = collisionFunction;
}

void stepSpells()
{
    for(int j = 0; j < 100; j++)
    {
        spellCooldowns[j] = (int)regressToMean(spellCooldowns[j], 1, 0);
    }
}

void castSpell(Entity* caster, SpellName spell, int spellSpecificLogicAttribute)
{
    Entity* e = newEntity(SPELL, 0, 0, 0, 0, 4);

    e->attrs[ATTR_SPELL] = spell;
    e->attrs[ATTR_SPELL_SPECIFIC_LOGIC] = spellSpecificLogicAttribute;
    e->relatedEntities[RELATED_ENTITY_CASTER] = caster;
    e->aiFunction = spellStats[spell].aiFunction;
    e->collisionFunction = spellStats[spell].collisionFunction;

    e->isEthereal = true;
    e->shouldNotBeRendered = true;

    if(spell == SPELL_FIREBALL)
    {
        e->isEthereal = false;
        e->shouldNotBeRendered = false;
        e->moveSpeed = 7;
        animateEntity(e, TEXTURE_FIREBALL_ANIMATION, 4);
    }

    if(spell == SPELL_JESTERS_BLADE)
    {
        e->isEthereal = false;
        e->moveSpeed = 9;
        e->spriteWidth = 16;
        e->spriteHeight = 16;
        animateEntity(e, TEXTURE_JESTERS_BLADE_ANIMATION, 3);
    }

    teleportEntity(e, centerX(caster) - trueWidth(e) / 2, centerY(caster) - trueHeight(e) / 2);
    e->facing = caster->facing;

    addEntity(e);
}

void destroyPreviousCastsOfThisSpell(Entity* caster, Entity* thisSpell)
{
    Entity** allEntities = getAllEntitiesArray();

    for(int i = 0; i < globals()->numberOfEntities; i++)
    {
        Entity* e = allEntities[i];

        if(e->entityType != SPELL || e->relatedEntities[RELATED_ENTITY_CASTER] != caster || e == thisSpell || e->attrs[ATTR_SPELL] != thisSpell->attrs[ATTR_SPELL])
            continue;

        e->markedForDestruction = true;
    }
}

void spawnFireballParticles(Entity* e)
{
    for(int k = 0; k < 10; k++)
    {
        int r = 255;
        int g = rand() % 150;
        int b = rand() % 50;
        int a = 255;
        int w = 1;
        if(rand() % 3 == 0) w = 2;
        createParticle(centerX(e), centerY(e), w, e->layer, rand() % 3 - 1, rand() % 3 - 1, rand() % 3, 1, 20, sf::Color(r, g, b, a), sf::Color::Transparent, sf::Color(r / 20, g / 20, b / 20, a / 20));
    }
}

void fireballAI(Entity* e)
{
    if(e->lifeTimer == 1)
    {
        playSoundAtEntityLocation(SOUND_FIREBALL, e);
    }

    if(!isEntityMoving(e))
    {
        setEntityMoveIntentionGrid(e, e->facing, e->moveSpeed * 2);
        if(leftEdge(e) <= getWorldBoundLeft() + 1 || rightEdge(e) >= getWorldBoundRight() - 1 || bottomEdge(e) >= getWorldBoundBottom() - 1 || topEdge(e) <= getWorldBoundTop() + 1)
            e->markedForDestruction = true;
    }

    if(isEntityMoving(e))
        spawnFireballParticles(e);
}

void chainLightningAI(Entity* e)
{
    Entity* caster = e->relatedEntities[RELATED_ENTITY_CASTER];

    if(e->lifeTimer == 1)
    {
        Entity* fakeEntity = new Entity;

        int cdx = 0;
        int cdy = 0;

        if(caster->facing == 1)
            cdy -= trueHeight(caster) * 2;
        if(caster->facing == 2)
            cdx -= trueWidth(caster) * 2;
        if(caster->facing == 3)
            cdy += trueHeight(caster) * 2;
        if(caster->facing == 4)
            cdx += trueWidth(caster) * 2;

        fakeEntity->x = caster->x + cdx;
        fakeEntity->y = caster->y + cdy;

        Entity* entitiesIgnored[11];
        Entity* entitiesHit[10];
        long long int entityDamages[10];
        int nHit = 0;

        entitiesIgnored[0] = caster;

        int dmg = spellStats[SPELL_CHAIN_LIGHTNING].baseDamage;
        dmg = adjustForBonusPower(dmg, caster);

        Entity* chainAt = fakeEntity;
        for(int i = 0; i < 10; i++)
        {
            entitiesIgnored[i + 1] = chainAt;
            Entity* chainNext = findClosest(chainAt, entitiesIgnored, nHit + 2);

            if(chainNext != 0)
            {
                int dx = chainAt->x - chainNext->x;
                int dy = chainAt->y - chainNext->y;
                if(dx * dx + dy * dy > 10000)
                    break;

                entitiesHit[i] = chainNext;
                entityDamages[i] = dmg;
                nHit++;

                dmg = (int)(dmg * 0.9);

                if(dmg == 0)
                    break;

                chainAt = chainNext;
            }
        }

        playSoundAtEntityLocation(SOUND_THUNDER, e);
        makeEntityFlash(caster, 100, sf::Color(255,255,255,255), sf::Color(0,0,0,20));

        int renderXFrom = centerX(caster);
        int renderYFrom = centerY(caster);

        for(int i = 0; i < nHit; i++)
        {
            Entity* eHit = entitiesHit[i];

            makeEntityFlash(eHit, 100, sf::Color(255,255,255,255), sf::Color(0,0,0,20));
            dealDamageToEntity(eHit, entityDamages[i]);

            int renderXTo = centerX(eHit);
            int renderYTo = centerY(eHit);

            long long int power = spellStats[SPELL_CHAIN_LIGHTNING].baseDamage;
            power = adjustForBonusPower(power, caster);

            // TODO why?
            for(int k = 0; k < power * 0.5 * pow(0.5, i); k++)
            {
                int rx = rand() % 8 - 4;
                int ry = rand() % 8 - 4;

                sf::Vertex line[] = {sf::Vertex(sf::Vector2f(renderXFrom, renderYFrom)), sf::Vertex(sf::Vector2f(renderXTo + rx, renderYTo + ry))};
                getSupplementaryRenderTexture(e->layer)->draw(line, 2, sf::Lines);
            }

            renderXFrom = renderXTo;
            renderYFrom = renderYTo;
        }

        if(nHit == 0)   // Still do some animation, if no enemies hit
        {
            int renderXTo = renderXFrom + cdx;
            int renderYTo = renderYFrom + cdy;
            int rgx = rand() % 10 - 5;
            int rgy = rand() % 10 - 5;

            for(int k = 0; k < 4; k++)
            {
                int rx = rand() % 8 - 4;
                int ry = rand() % 8 - 4;

                sf::Vertex line[] = {sf::Vertex(sf::Vector2f(renderXFrom, renderYFrom)), sf::Vertex(sf::Vector2f(renderXTo + rx + rgx, renderYTo + ry + rgy))};
                getSupplementaryRenderTexture(e->layer)->draw(line, 2, sf::Lines);
            }
        }

        delete fakeEntity;
    }

    if(e->lifeTimer == 4)
    {
        e->markedForDestruction = true;
    }
}

void blizzardAI(Entity* e)
{
    Entity* caster = e->relatedEntities[RELATED_ENTITY_CASTER];

    if(e->lifeTimer == 1)
    {
        destroyPreviousCastsOfThisSpell(caster, e);
        e->x = centerX(caster) - e->hitBoxClearanceLeft - trueWidth(e) / 2;
        e->y = centerY(caster) - e->hitBoxClearanceTop - trueHeight(e) / 2 - caster->jumpCurrentHeight;

        makeEntityFlash(globals()->player, 30, sf::Color(190, 190, 250, 200), sf::Color(0,0,0,5));
        playSound(SOUND_ICE_SHARD);

        for(int i = 0; i < 30; i++)
        {
            TextureId spritesheetTextureId = TEXTURE_NONE;
            int r = rand() % 3;
            if(r == 0) spritesheetTextureId = TEXTURE_ICE_SHARD_LARGE_SPRITESHEET;
            if(r == 1) spritesheetTextureId = TEXTURE_ICE_SHARD_MEDIUM_SPRITESHEET;
            if(r == 2) spritesheetTextureId = TEXTURE_ICE_SHARD_SMALL_SPRITESHEET;

            int largeVelocityMagnitude = 5 + rand() % 3;
            int smallVelocityMagnitude = rand() % 7 - 3;
            int xVelocity = 0;
            int yVelocity = 0;

            if(caster->facing == 1)
            {
                yVelocity = -largeVelocityMagnitude;
                xVelocity = smallVelocityMagnitude;
            }
            if(caster->facing == 2)
            {
                xVelocity = -largeVelocityMagnitude;
                yVelocity = smallVelocityMagnitude;
            }
            if(caster->facing == 3)
            {
                yVelocity = largeVelocityMagnitude;
                xVelocity = smallVelocityMagnitude;
            }
            if(caster->facing == 4)
            {
                xVelocity = largeVelocityMagnitude;
                yVelocity = smallVelocityMagnitude;
            }

            Entity* spawnedShard = newEntity(SPELL, centerX(caster), centerY(caster), 0, 7, 3, spritesheetTextureId, iceShardAI, iceShardCollision);
            spawnedShard->attrs[SPELL] = SPELL_ICE_SHARD;
            spawnedShard->attrs[ATTR_X] = xVelocity;
            spawnedShard->attrs[ATTR_Y] = yVelocity;
            spawnedShard->relatedEntities[RELATED_ENTITY_CASTER] = caster;
            spawnedShard->facing = caster->facing;
            spawnedShard->isAnimated = false;

            if(r == 0)
            {
                spawnedShard->spriteWidth = 9;
                spawnedShard->spriteHeight = 9;
            }
            if(r == 1)
            {
                spawnedShard->spriteWidth = 6;
                spawnedShard->spriteHeight = 6;
            }
            if(r == 2)
            {
                spawnedShard->spriteWidth = 3;
                spawnedShard->spriteHeight = 3;
            }
            addEntity(spawnedShard);
        }
    }

    if(e->lifeTimer == 20)
    {
        e->markedForDestruction = true;
    }
}

void iceShardAI(Entity* e)
{
    if(leftEdge(e) <= getWorldBoundLeft() + 1 || rightEdge(e) >= getWorldBoundRight() - 1 || bottomEdge(e) >= getWorldBoundBottom() - 1 || topEdge(e) <= getWorldBoundTop() + 1)
        e->markedForDestruction = true;

    e->goalX += e->attrs[ATTR_X];
    e->goalY += e->attrs[ATTR_Y];

    if(e->lifeTimer % 3 == 0 && rand() % 3 < 2)
    {
        e->attrs[ATTR_X] = regressToMean(e->attrs[ATTR_X], 1, 0);
        e->attrs[ATTR_Y] = regressToMean(e->attrs[ATTR_Y], 1, 0);
    }

    if(e->attrs[ATTR_X] == 0 && e->attrs[ATTR_Y] == 0)
    {
        e->attrs[ATTR_SPELL_SPECIFIC_LOGIC]++;
        if(e->attrs[ATTR_SPELL_SPECIFIC_LOGIC] > 30) e->markedForDestruction = true;
    }
}

void scorchingLightAI(Entity* e)
{
    Entity* caster = e->relatedEntities[RELATED_ENTITY_CASTER];
    Entity** allEntities = getAllEntitiesArray();
    int totalEntities = globals()->numberOfEntities;

    bool castingAsSkeletonMage = caster->entityType != PLAYER;

    const int peakBeamWidth = 3;
    const int spellLifetime = 30;

    // Initial flash and sound effect
    if(e->lifeTimer == 1)
    {
        sf::Color flashColor = sf::Color(255,255,0,230);
        if(castingAsSkeletonMage) flashColor = sf::Color(100,0,200,230);

        makeEntityFlash(caster, 60, flashColor, sf::Color(0,0,0,10));
        teleportEntity(e, e->x, e->y);
        playSoundAtEntityLocation(SOUND_BLOOD_PACT, e);
    }

    // Set size of collision box based on screen dimensions, static collisions, and hit enemies, on first frame
    if(e->lifeTimer == 1)
    {
        CollisionBox collisionBox;
        const int castOffset = 8;
        int facing = caster->facing;

        // Set maximum size collision box based on screen dimensions
        collisionBox.width = peakBeamWidth / 4;
        collisionBox.height = peakBeamWidth / 4;
        collisionBox.x = centerX(caster) - collisionBox.height / 2;
        collisionBox.y = centerY(caster) - collisionBox.width / 2;

        if(facing == 1)
        {
            collisionBox.y -= castOffset;
            collisionBox.height += collisionBox.y - getWorldBoundTop();
            collisionBox.y = getWorldBoundTop();
        }
        if(facing == 2)
        {
            collisionBox.x -= castOffset;
            collisionBox.width += collisionBox.x - getWorldBoundLeft();
            collisionBox.x = getWorldBoundLeft();
        }
        if(facing == 3)
        {
            collisionBox.y += castOffset;
            collisionBox.height += getWorldBoundBottom() - collisionBox.y;
        }
        if(facing == 4)
        {
            collisionBox.x += castOffset;
            collisionBox.width += getWorldBoundRight() - collisionBox.x;
        }

        // Set length to first static collision
        while(!isRegionFreeOfStaticCollisions(collisionBox.x, collisionBox.y, collisionBox.width, collisionBox.height, e->entityType))
        {
            if(facing == 1) {collisionBox.height -= 1; collisionBox.y += 1;}
            if(facing == 3) {collisionBox.height -= 1; }
            if(facing == 2) {collisionBox.width -= 1; collisionBox.x += 1;}
            if(facing == 4) {collisionBox.width -= 1; }
        }

        // Find closest enemy
        Entity* closestEntity = 0;

        for(int i = 0; i < totalEntities; i++)
        {
            Entity* otherEntity = allEntities[i];

            if(otherEntity == caster || (!castingAsSkeletonMage && otherEntity->entityType != ENEMY) || (castingAsSkeletonMage && otherEntity->entityType != ENEMY && otherEntity->entityType != PLAYER))
                continue;

            if(!isCollidingOnX(otherEntity, &collisionBox) || !isCollidingOnY(otherEntity, &collisionBox))
                continue;

            if(
               (closestEntity == 0) ||
               (facing == 1 && bottomEdge(otherEntity) > bottomEdge(closestEntity)) ||
               (facing == 3 && topEdge(otherEntity) < topEdge(closestEntity)) ||
               (facing == 2 && rightEdge(otherEntity) > rightEdge(closestEntity)) ||
               (facing == 4 && leftEdge(otherEntity) < leftEdge(closestEntity))
            )
            {
                closestEntity = otherEntity;
                continue;
            }
        }

        // Set box to connect with first hit enemy, and damage that enemy
        if(closestEntity != 0)
        {
            int x = collisionBox.x;
            int y = collisionBox.y;
            int w = collisionBox.width;
            int h = collisionBox.height;

            if(facing == 1)
            {
                y = bottomEdge(closestEntity);
                h = (collisionBox.y + collisionBox.height) - bottomEdge(closestEntity);
            }
            if(facing == 2)
            {
                x = rightEdge(closestEntity);
                w = (collisionBox.x + collisionBox.width) - rightEdge(closestEntity);
            }
            if(facing == 3)
            {
                h = topEdge(closestEntity) - collisionBox.y;
            }
            if(facing == 4)
            {
                w = leftEdge(closestEntity) - collisionBox.x;
            }

            collisionBox.x = x;
            collisionBox.y = y;
            collisionBox.width = w;
            collisionBox.height = h;

            // Damage enemy
            long long int power = spellStats[SPELL_SCORCHING_LIGHT].baseDamage;

            if(caster->entityType == PLAYER)
            {
                float distanceAdjustment = ((float)collisionBox.width / ((float)getWorldBoundRight() - (float)getWorldBoundLeft()));
                if(facing == 1 || facing == 3)
                    distanceAdjustment = ((float)collisionBox.height / ((float)getWorldBoundRight() - (float)getWorldBoundLeft()));

                power = adjustForBonusPower(power, caster);
                power = (long long int)((float)power * distanceAdjustment) + 5;
            }
            else
            {
                power /= 4; // So that enemies get a quarter of max damage regardless of distance
            }

            dealDamageToEntity(closestEntity, power);
        }

        // Store for next frame
        e->attrs[ATTR_X] = collisionBox.x;
        e->attrs[ATTR_Y] = collisionBox.y;
        e->attrs[ATTR_MEMORY_1] = collisionBox.width;
        e->attrs[ATTR_MEMORY_2] = collisionBox.height;
        e->attrs[ATTR_MEMORY_3] = caster->facing; // Initial facing
    }

    // Do the beam each frame
    if(e->lifeTimer % 1 == 0)
    {
        int drawX = e->attrs[ATTR_X];
        int drawY = e->attrs[ATTR_Y];
        int drawWidth = e->attrs[ATTR_MEMORY_1];
        int drawHeight = e->attrs[ATTR_MEMORY_2];
        int facing = e->attrs[ATTR_MEMORY_3];
        int drawLayer = 6;

        int beamWidth = (int)(peakBeamWidth * (sin(PI_VALUE * (e->lifeTimer / (float)spellLifetime))));

        if(facing == 1 || facing == 3)
        {
            drawWidth = beamWidth;
            drawX -= beamWidth / 3;
        }
        if(facing == 2 || facing == 4)
        {
            drawHeight = beamWidth;
            drawY -= beamWidth / 3;
        }

        if(castingAsSkeletonMage)
        {
            if(facing == 1)
            {
                drawX -= 3;
            }
            if(facing == 2)
            {
                drawWidth -= 6;
                drawY += 5;
            }
            if(facing == 3)
            {
                drawX -= 2;
                drawY += 3;
                drawHeight -= 3;
            }
            if(facing == 4)
            {
                drawX += 8;
                drawWidth -= 8;
                drawY += 5;
            }
        }

        if(facing == 1)
            drawLayer = 3;

        // Draw beam
        sf::RectangleShape rectangle(sf::Vector2f(drawWidth, drawHeight));
        rectangle.setPosition(drawX, drawY);
        rectangle.setFillColor(sf::Color(255, 255, 255, 255));
        rectangle.setOutlineColor(sf::Color(255, 255, 200, 255));

        if(castingAsSkeletonMage)
        {
            rectangle.setFillColor(sf::Color(100, 0, 200, 200));
            rectangle.setOutlineColor(sf::Color(39, 0, 77, 200));
        }

        rectangle.setOutlineThickness(1.0f);
        getSupplementaryRenderTexture(drawLayer)->draw(rectangle);

        // For the first half of the spell, show burn at both ends of beam
        if(e->lifeTimer < spellLifetime / 2)
        {
            int particleXEnemy;
            int particleYEnemy;
            int particleXCaster;
            int particleYCaster;

            if(facing == 1)
            {
                particleXEnemy = drawX + drawWidth / 2;
                particleXCaster = particleXEnemy;
                particleYEnemy = drawY;
                particleYCaster = particleYEnemy + drawHeight;
            }
            if(facing == 3)
            {
                particleXEnemy = drawX + drawWidth / 2;
                particleXCaster = particleXEnemy;
                particleYEnemy = drawY + drawHeight;
                particleYCaster = particleYEnemy - drawHeight;
            }
            if(facing == 2)
            {
                particleYEnemy = drawY + drawHeight / 2;
                particleYCaster = particleYEnemy;
                particleXEnemy = drawX;
                particleXCaster = particleXEnemy + drawWidth;
            }
            if(facing == 4)
            {
                particleYEnemy = drawY + drawHeight / 2;
                particleYCaster = particleYEnemy;
                particleXEnemy = drawX + drawWidth;
                particleXCaster = particleXEnemy - drawWidth;
            }

            for(int k = 0; k < 20; k++)
            {
                int r = 255;
                int g = rand() % 150 + 100;
                int b = rand() % 50 + 50;
                int a = 255;
                int w = 2;
                createParticle(particleXEnemy + rand() % 7 - 3, particleYEnemy + rand() % 7 - 3, w, 7, rand() % 5 - 2, rand() % 5 - 2, 0, 1, 10, sf::Color(r, g, b, a), sf::Color::Transparent, sf::Color(r / 20, g / 20, b / 20, a / 20));
                createParticle(particleXCaster + rand() % 7 - 3, particleYCaster + rand() % 7 - 3, w, drawLayer, rand() % 5 - 2, rand() % 5 - 2, 0, 1, 10, sf::Color(r, g, b, a), sf::Color::Transparent, sf::Color(r / 20, g / 20, b / 20, a / 20));
            }
        }
    }

    // Extinguish spell
    if(e->lifeTimer == spellLifetime)
    {
        e->markedForDestruction = true;
    }
}

void teleportAI(Entity* e)
{
    Entity* caster = e->relatedEntities[RELATED_ENTITY_CASTER];

    if(isEntityImmobilized(caster))
    {
        deImmobilizeEntity(caster);
        caster->jumpLandDamage = 0;
        caster->jumpLandSoundId = SOUND_NONE;
        caster->isJumping = false;
        caster->jumpCurrentHeight = 0;
    }

    if(e->lifeTimer == 1)
    {
        int distance = 160;
        if(!(caster == globals()->player) && e->attrs[ATTR_SPELL_SPECIFIC_LOGIC] != 0)
            distance = e->attrs[ATTR_SPELL_SPECIFIC_LOGIC];

        int x = caster->x;
        int y = caster->y;

        int facing = caster->facing;
        if(caster == globals()->player && e->attrs[ATTR_SPELL_SPECIFIC_LOGIC] != 0)
            facing = e->attrs[ATTR_SPELL_SPECIFIC_LOGIC];

        if(facing == 4)
            x += distance;
        if(facing == 2)
            x -= distance;
        if(facing == 1)
            y -= distance;
        if(facing == 3)
            y += distance;

        int loopPanicTick = 0;
        while(!isRegionFreeOfStaticCollisions(x, y, trueWidth(caster), trueHeight(caster), caster->entityType))
        {
            x = regressToMean(x, 1, caster->x);
            y = regressToMean(y, 1, caster->y);

            if(loopPanicTick++ > 1000)
            {
                PRINT_DEBUG("ERROR: Loop panic condition.");
                e->markedForDestruction = true;
                return;
            }
        }

        // Ghost of caster left behind
        teleportEntity(e, caster->x, caster->y);
        e->facing = facing;
        e->shouldNotBeRendered = false;
        e->frameColumn = caster->frameColumn;
        e->frameRow = caster->frameRow;
        e->animationPatternIndex = caster->animationPatternIndex;
        e->spriteWidth = caster->spriteWidth;
        e->spriteHeight = caster->spriteHeight;
        e->hitBoxClearanceLeft = caster->hitBoxClearanceLeft;
        e->hitBoxClearanceTop = caster->hitBoxClearanceTop;
        e->hitBoxClearanceRight = caster->hitBoxClearanceRight;
        e->hitBoxClearanceBottom = caster->hitBoxClearanceBottom;
        e->flashSprite->setTexture(*retrieveTextureMask(caster->spritesheetTextureId));
        makeEntityFlash(e, 30, sf::Color(182,0,255,120), sf::Color(0,0,0,4));

        // Actual teleport of caster
        teleportEntity(caster, x, y);
        warpIntoBounds(caster);
        //makeEntityFlash(caster, 30, sf::Color(182,0,255,150), sf::Color(0,0,0,5));
        playSoundAtEntityLocation(SOUND_WARP, e);
        createAnimationAtPoint(TEXTURE_TELEPORT_ANIMATION, 1, centerX(caster), bottomEdge(caster), caster->layer - 1);
    }

    if(e->lifeTimer == 30)
    {
        e->markedForDestruction = true;
    }
}

void shockwaveAI(Entity* e)
{
    PRINT_DEBUG("Shockwave AI!");
}

void homingFireballsAI(Entity* e)
{
    Entity* caster = e->relatedEntities[RELATED_ENTITY_CASTER];

    if(e->lifeTimer == 1)
    {
        destroyPreviousCastsOfThisSpell(caster, e);
        e->x = centerX(caster) - e->hitBoxClearanceLeft - trueWidth(e) / 2;
        e->y = centerY(caster) - e->hitBoxClearanceTop - trueHeight(e) / 2 - caster->jumpCurrentHeight;
    }

    if(e->lifeTimer % 20 == 1)
    {
        makeEntityFlash(globals()->player, 30, sf::Color(150, 30, 50, 200), sf::Color(5, 5, 5, 10));

        Entity* spawnedFireball = newEntity(SPELL, centerX(caster), centerY(caster), 0, 7, 6, TEXTURE_NONE, homingFireballAI, fireballCollision);
        spawnedFireball->attrs[ATTR_SPELL] = SPELL_FIREBALL;
        spawnedFireball->attrs[ATTR_X] = 0;
        spawnedFireball->attrs[ATTR_Y] = 0;
        spawnedFireball->relatedEntities[RELATED_ENTITY_CASTER] = caster;
        spawnedFireball->hitBoxClearanceLeft = 6;
        spawnedFireball->hitBoxClearanceTop = 6;
        spawnedFireball->hitBoxClearanceRight = 6;
        spawnedFireball->hitBoxClearanceBottom = 6;
        spawnedFireball->facing = caster->facing;

        animateEntity(spawnedFireball, TEXTURE_FIREBALL_ANIMATION, 4);
        addEntity(spawnedFireball);
    }

    if(e->lifeTimer == 500)
    {
        e->markedForDestruction = true;
    }
}

void homingFireballAI(Entity* e)
{
    if(e->lifeTimer == 1)
    {
        playSound(SOUND_FIREBALL, 0.2);
    }

    if(leftEdge(e) <= getWorldBoundLeft() + 1 || rightEdge(e) >= getWorldBoundRight() - 1 || bottomEdge(e) >= getWorldBoundBottom() - 1 || topEdge(e) <= getWorldBoundTop() + 1)
        e->markedForDestruction = true;

    Entity* ignoredEntities[1];
    ignoredEntities[0] = e->relatedEntities[RELATED_ENTITY_CASTER]; // caster
    Entity* closestEntity = findClosest(e, ignoredEntities, 1);

    if(closestEntity)
    {
        int maxSpeed = 7;
        int dx = centerX(closestEntity) - centerX(e);
        int dy = centerY(closestEntity) - centerY(e);

        if(std::abs(dx) > std::abs(dy))
        {
            if(dx >= 0) e->attrs[ATTR_X] = clampHigh(e->attrs[ATTR_X] + 1, maxSpeed);
            else e->attrs[ATTR_X] = clampLow(e->attrs[ATTR_X] - 1, -maxSpeed);
        }
        else
        {
            if(dy >= 0) e->attrs[ATTR_Y] = clampHigh(e->attrs[ATTR_Y] + 1, maxSpeed);
            else e->attrs[ATTR_Y] = clampLow(e->attrs[ATTR_Y] - 1, -maxSpeed);
        }
    }

    e->goalX += e->attrs[ATTR_X];
    e->goalY += e->attrs[ATTR_Y];

    if(rand() % 10 == 0) e->goalX += rand() % 2 - 1;
    if(rand() % 10 == 0) e->goalY += rand() % 2 - 1;

    if(isEntityMoving(e))
       spawnFireballParticles(e);
}

void jestersBladeAI(Entity* e)
{
    Entity* caster = e->relatedEntities[RELATED_ENTITY_CASTER];

    if(e->lifeTimer == 1)
    {
        destroyPreviousCastsOfThisSpell(caster, e);
    }

    if(e->lifeTimer == 2)
    {
        globals()->isPlayerMoveSelfDisabled = true;
    }

    if(e->lifeTimer <= 3)
    {
        playSoundAtEntityLocation(SOUND_BLADE_STRIKE, e);
    }

    if(e->lifeTimer % 10 == 0)
    {
        playSoundAtEntityLocation(SOUND_BLADE_SWOOSH, e);
        makeEntityFlash(caster, 10, sf::Color(255,255,255,100), sf::Color(0,0,0,10));
    }

    int maintenanceCostPerSecond = spellStats[e->attrs[ATTR_SPELL]].mpCost / 2;
    int frameModulusToChargeOn = 60 / maintenanceCostPerSecond;

    if(frameModulusToChargeOn < 1)
        frameModulusToChargeOn = 1;

    if(e->lifeTimer % frameModulusToChargeOn == 0)
    {
        if(globals()->playerStats->playerMp < 1.0)
        {
            e->markedForDestruction = true;
            globals()->isPlayerMoveSelfDisabled = false;
        }
        else {
            globals()->playerStats->playerMp -= 1.0;
        }
    }

    if(isKeyDownThisFrame(KEY_CAST))
    {
        e->markedForDestruction = true;
        globals()->isPlayerMoveSelfDisabled = false;
    }

    if(!e->isJumping)
    {
        makeEntityMoveAccordingToCurrentInput(e, false, false, true);
    }
}

void warmthAI(Entity* e)
{
    Entity* caster = e->relatedEntities[RELATED_ENTITY_CASTER];

    const int frameTicksPerHealthTick = 5;

    int power = spellStats[SPELL_WARMTH].baseDamage;
    power = adjustForBonusPower(power, caster);

    int totalLifeTime = power * frameTicksPerHealthTick;

    if(e->lifeTimer == 1)
    {
        makeEntityFlash(caster, 100, sf::Color(255,255,50,255), sf::Color(0,0,0,(255 / totalLifeTime) + 1));
        teleportEntity(e, centerX(caster) - 24, centerY(caster) - 36);
        playSoundAtEntityLocation(SOUND_WARMTH, e);
        animateEntity(e, TEXTURE_WARMTH_ANIMATION, 4);
    }

    if(e->lifeTimer % frameTicksPerHealthTick == 1)
    {
        healEntity(caster, 1);
    }

    if(e->lifeTimer == 44)
    {
        e->shouldNotBeRendered = true;
    }

    if(e->lifeTimer == totalLifeTime)
    {
        e->markedForDestruction = true;
    }
}

void fireballCollision(Entity* spell, Entity* collidee)
{
    if(collidee == spell->relatedEntities[RELATED_ENTITY_CASTER] || (collidee && (collidee->entityType == SPELL || collidee->entityType == COLLECTIBLE)))
        return;

    if(spell->markedForDestruction)
        return;

    if(collidee != 0)
    {
        long long int power = spellStats[SPELL_FIREBALL].baseDamage;
        power = adjustForBonusPower(power, spell->relatedEntities[RELATED_ENTITY_CASTER]);

        dealDamageToEntity(collidee, power);
    }

    createAnimationAtPoint(TEXTURE_EXPLOSION_ANIMATION, 3, centerX(spell), centerY(spell), 7);
    playSoundAtEntityLocation(SOUND_FIREBALL_EXPLODE, spell);
    spell->markedForDestruction = true;
}

void iceShardCollision(Entity* spell, Entity* collidee)
{
    if(collidee == spell->relatedEntities[RELATED_ENTITY_CASTER] || (collidee && (collidee->entityType == SPELL || collidee->entityType == COLLECTIBLE)))
        return;

    if(spell->markedForDestruction)
        return;

    if(collidee != 0)
    {
        long long int power = spellStats[SPELL_BLIZZARD].baseDamage;
        power = adjustForBonusPower(power, spell->relatedEntities[RELATED_ENTITY_CASTER]);

        dealDamageToEntity(collidee, power);
    }

    playSoundAtEntityLocation(SOUND_ICE_SHARD_SMASH, spell);
    spell->markedForDestruction = true;
}

void jestersBladeCollision(Entity* spell, Entity* collidee)
{
    PRINT_DEBUG("Collision!");
    if(collidee == spell->relatedEntities[RELATED_ENTITY_CASTER])
        return;

    if(spell->markedForDestruction)
        return;

    bool enemyWasDamaged = false;

    if(collidee != 0)
    {
        int power = spellStats[SPELL_JESTERS_BLADE].baseDamage;
        power = adjustForBonusPower(power, spell->relatedEntities[RELATED_ENTITY_CASTER]);

        enemyWasDamaged = dealDamageToEntity(collidee, power);
        if(enemyWasDamaged) collidee->invincibilityCooldown = 10;
    }

    if(collidee == 0 || collidee->entityType == INANIMATE)
    {
        makeEntityJump(spell, -spell->impliedXVelocity * 3, -spell->impliedYVelocity * 3, 12, spell->moveSpeed * 2);
        playSoundAtEntityLocation(SOUND_JESTERS_BLADE_BOUNCE, spell);
    }

    if(enemyWasDamaged)
    {
        makeEntityFlash(collidee, 20, sf::Color(255,0,0,255), sf::Color(0,0,0,25));
        playSoundAtEntityLocation(SOUND_BLADE_STRIKE, spell);
    }
}

