#ifndef SPELL_H
#define SPELL_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <algorithm>

#include "input.h"
#include "entity.h"
#include "assets.h"
#include "globals.h"
#include "stats.h"
#include "enums.h"

struct SpellStats
{
    int baseDamage = 0;
    int mpCost = 0;
    int layer = 5;
    std::string castString = "";
    TextureId spellIcon = TEXTURE_NONE;
    std::string niceName = "";
    std::string spellDescription = "";

    void (*aiFunction) (Entity*) = 0;
    void (*collisionFunction)(Entity* thisGuy, Entity* thatGuy) = 0;
};

SpellStats* getSpellStatsArray();

SpellName translateStrokesToSpell(int strokes[], int numStrokes);
void addSpellStroke(int dir);
void dischargeSpellInput();
void incantSpell(SpellName spell, bool bankInsteadOfCast);  // Brings a spell into existence as a pre-cast, either to cast or bank it
void incantRebank();

int spellStrokesInBuffer();
void renderSpellInput(sf::RenderWindow* window);
void initializeSpellInputGraphics();

void initializeSpells();
void initializeOneSpell(SpellName spell, TextureId spellIcon, std::string spellString, std::string niceName, std::string spellDescription, int baseDamage, int mpCost, int layer, void (*aiFunction) (Entity*) = 0, void (*collisionFunction)(Entity* thisGuy, Entity* thatGuy) = 0);

void stepSpells();

void incantSpell(SpellName spell);  // Brings a spell into existence as a pre-cast, which may go ahead as a casted spell
void castSpell(Entity* caster, SpellName spell, int spellSpecificLogicAttribute = 0);   // Actually activate a spell. Returns the spell (as an entity)
void destroyPreviousCastsOfThisSpell(Entity* caster, Entity* thisSpell);

void fireballAI(Entity* e);
void chainLightningAI(Entity* e);
void blizzardAI(Entity* e);
void iceShardAI(Entity* e);
void scorchingLightAI(Entity* e);
void teleportAI(Entity* e);
void shockwaveAI(Entity* e);
void warmthAI(Entity* e);
void homingFireballsAI(Entity* e);
void homingFireballAI(Entity* e);
void jestersBladeAI(Entity* e);


void fireballCollision(Entity* spell, Entity* collidee);
void iceShardCollision(Entity* spell, Entity* collidee);
void jestersBladeCollision(Entity* spell, Entity* collidee);

#endif
