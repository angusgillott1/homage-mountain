#include "stats.h"

//--------------
// Local globals
//--------------

const float PLAYER_BASE_HP = 150;
const float PLAYER_BASE_MP = 100;

const int baseSecondsToFillMana = 10; // setting
const float baseTicksToFillMana = baseSecondsToFillMana * FRAME_RATE;
const float baseProportionOfTotalManaRecoveredPerTick = 1.0 / baseTicksToFillMana;
const float baseMpRegenPerFrame = PLAYER_BASE_MP * baseProportionOfTotalManaRecoveredPerTick;

//-------------
// Functions
//-------------

void initializeStats()
{
    globals()->playerStats = new PlayerStats();
}

void stepStats()
{
    //PRINT_DEBUG("Restoring mana based on max mp: " << globals()->player->maxMp << " (" << globals()->player->maxMp * baseProportionOfTotalManaRecoveredPerTick * FRAME_RATE << " /s)");
    restorePlayerMana(globals()->playerStats->playerMaxMp * baseProportionOfTotalManaRecoveredPerTick);
}

void renderPlayerStatBars(sf::RenderWindow* window)
{
    Entity* e = globals()->player;

    if(e->maxHp <= 0)
        return;

    sf::RenderTexture healthBarTexture;
    healthBarTexture.create(600, 29);

    sf::Sprite healthBarSprite;
    healthBarSprite.setTexture(healthBarTexture.getTexture());

    healthBarTexture.clear(sf::Color (0, 0, 0, 0));

    float barWidth = 240.0;
    float barHeight = 7.0;

    // HP bar
    sf::RectangleShape outerRectangle(sf::Vector2f(barWidth, barHeight));
    outerRectangle.setOutlineColor(sf::Color (0, 0, 0, 255));
    outerRectangle.setOutlineThickness(-1.0f);
    outerRectangle.setFillColor(sf::Color (0, 0, 0, 0));
    outerRectangle.setPosition(0, 1);
    healthBarTexture.draw(outerRectangle);

    sf::RectangleShape innerRectangle(sf::Vector2f(std::max(floor((barWidth - 2) * ((float)e->displayHp / e->maxHp)), (float)1.0), (barHeight - 2)));
    innerRectangle.setFillColor(sf::Color (20, 255, 20, 255));
    innerRectangle.setPosition(1, 2);
    innerRectangle.setOutlineColor(sf::Color (20, 220, 20, 220));
    innerRectangle.setOutlineThickness(-1.0f);
    healthBarTexture.draw(innerRectangle);

    // HP summary text
    std::string hpSummary = std::to_string((int)globals()->player->hp) + "/" + std::to_string((int)globals()->player->maxHp);
    {
        sf::RectangleShape dropShadow(sf::Vector2f(hpSummary.length() * 6 + 1, 9));
        dropShadow.setFillColor(sf::Color (0, 0, 0, 30));
        dropShadow.setPosition(outerRectangle.getSize().x + 3, 0);
        healthBarTexture.draw(dropShadow);
    }
    drawText(&healthBarTexture, 1 + outerRectangle.getSize().x + 3, 1, hpSummary);

    // MP bar
    outerRectangle.setSize(sf::Vector2f(barWidth, barHeight));
    outerRectangle.setPosition(0, 12);
    healthBarTexture.draw(outerRectangle);

    innerRectangle.setPosition(1, 13);
    innerRectangle.setSize(sf::Vector2f(std::max(floor((barWidth - 2) * (globals()->playerStats->playerDisplayMp / globals()->playerStats->playerMaxMp)), (float)1.0), (barHeight - 2)));
    innerRectangle.setFillColor(sf::Color (72, 66, 245, 255));
    innerRectangle.setOutlineColor(sf::Color (52, 46, 225, 235));
    innerRectangle.setOutlineThickness(-1.0f);
    healthBarTexture.draw(innerRectangle);

    // MP summary text
    std::string mpSummary = toScientificNotation(globals()->playerStats->playerMp, 0) + "/" + toScientificNotation(globals()->playerStats->playerMaxMp, 0);
    {
        sf::RectangleShape dropShadow(sf::Vector2f(mpSummary.length() * 6 + 1, 9));
        dropShadow.setFillColor(sf::Color (0, 0, 0, 30));
        dropShadow.setPosition(outerRectangle.getSize().x + 3, 11);
        healthBarTexture.draw(dropShadow);
    }
    drawText(&healthBarTexture, 1 + outerRectangle.getSize().x + 3, 12, mpSummary);

    // Bonus power text
    if(calculateBonusPowerMultiplier() > 1.0)
    {
        std::string powerBonusSummary = "x " + toScientificNotation(calculateBonusPowerMultiplier());
        drawText(&healthBarTexture, 1, 22, powerBonusSummary, REGULAR_FONT, sf::Color(255,255,120,255));
    }

    // Display health bar

    healthBarTexture.display();  // Forces it to be the right y-axis orientation, something related to OpenGL
    healthBarSprite.setPosition(4, 3);
    window->draw(healthBarSprite);

    // Draw Level nice name
    {
        int xx = 800; int yy = 12;

        drawText(window, xx, yy, getLevelNiceName(), LARGE_FONT, sf::Color(255,255,255,255));
    }

    // Draw Modifier description
    {
        int xx = 840; int yy = 28;

        drawText(window, xx, yy, getArmyStageModifierDescription(), REGULAR_FONT, sf::Color(255,255,50,255));
    }
}

float calculateBonusPowerMultiplier()
{
    return 1.0;
}

long long int adjustForBonusPower(int power, Entity* caster)
{
    if(caster->entityType != PLAYER) return power;

    return (long long int)(power * calculateBonusPowerMultiplier());
}

void restorePlayerMana(float amount)
{
    globals()->playerStats->playerMp += amount;

    float overflowAmount = globals()->playerStats->playerMp - globals()->playerStats->playerMaxMp;

    if(overflowAmount > 0)
    {
        globals()->playerStats->playerMp = globals()->playerStats->playerMaxMp;
    }
}

void spawnExpPacket(int x, int y, int exp)
{
    if(exp <= 0)
        return;

    while(exp > 0)
    {
        int largestPowerOfTwo = (int)std::floor(std::log2(exp));

        int powTwo = 0;
        if(largestPowerOfTwo > 0)
            powTwo = rand() % largestPowerOfTwo;

        int eatExp = (int)pow(2, powTwo);

        Particle* p = createParticle(rand() % 32 - 16 + x, rand() % 32 - 16 + y, powTwo + 1, 5, rand() % 3 - 1, rand() % 3 - 1, 3, 1, 1200, sf::Color(31, 117, 255, 220), sf::Color(15, 80, 184, 220), sf::Color::Transparent);
        if(p)   // p may be 0 if max particles reached
        {
            p->isExpDiamond = true;
            p->expValue = eatExp;
        }

        exp -= eatExp;
    }
}
