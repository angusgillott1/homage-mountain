#ifndef STATS_H
#define STATS_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <math.h>

#include "globals.h"
#include "entity.h"
#include "enums.h"

struct PlayerStats
{
    float playerMp = 0;
    float playerDisplayMp = 0;
    float playerMaxMp = 0;
};

void initializeStats();
void stepStats();

void renderPlayerStatBars(sf::RenderWindow* window);

float calculateBonusPowerMultiplier();
long long int adjustForBonusPower(int power, Entity* caster); // Adjusts a power value according to bonus power

void restorePlayerMana(float amount);

void spawnExpPacket(int x, int y, int exp);

#endif // STATS_H
