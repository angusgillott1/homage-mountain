#include "utils.h"

sf::Clock totalClock;
sf::Clock intervalOneClock;
sf::Clock intervalTwoClock;

long long int intervalOneSum = 0;
long long int intervalTwoSum = 0;

int intervalOneTicks = 0;
int intervalTwoTicks = 0;

bool writeStringToFile(std::string str, std::string filename)
{
    std::ofstream outFile(filename);

    if (outFile.is_open())
    {
        outFile << str;
        outFile.close();
        return true;
    }

    return false;
}

std::string readStringFromFile(std::string filename)
{
    std::ifstream inFile(filename);
    std::string str;

    if (inFile.is_open())
    {
        std::getline(inFile, str, '\0');
        inFile.close();
        return str;
    }

    return "NO_FILE_FOUND";
}

int regressToMean(int value, int step, int mean)
{
    int dxToMean = mean - value;

    if(dxToMean == 0)
        return value;

    if(dxToMean > 0)
    {
        value += step;

        if(value > mean)
            value = mean;
    }
    else
    {
        value -= step;

        if(value < mean)
            value = mean;
    }

    return value;
}

float regressToMean(float value, float step, float mean)
{
    float dxToMean = mean - value;

    if(dxToMean == 0.0)
        return value;

    if(dxToMean > 0.0)
    {
        value += step;

        if(value > mean)
            value = mean;
    }
    else
    {
        value -= step;

        if(value < mean)
            value = mean;
    }

    return value;
}

int clampHigh(int value, int clamp)
{
    return std::min(value, clamp);
}

int clampLow(int value, int clamp)
{
    return std::max(value, clamp);
}

int clampMagnitude(int value, int clamp)
{
    PRINT_DEBUG("Warning. Clamping with negative clamp: " << clamp);

    int clampedMagnitude = std::min(std::abs(value), std::abs(clamp));

    if(value < 0) return -clampedMagnitude;
    else return clampedMagnitude;
}

float distance(float x1, float y1, float x2, float y2)
{
    float dx = x2 - x1;
    float dy = y2 - y1;
    return std::sqrt(dx * dx + dy * dy);
}

std::string toScientificNotation(float value, int decimalPlaces)
{
    float v = value;
    int e = 0;
    for(e = 0; v >= 10.0; e++)
    {
        v /= 10.0;
    }

    if(value <= 1000 && decimalPlaces == 0)
        return std::to_string((int)value);

    if(e <= 3)
    {
        int substrLength = decimalPlaces + 2 + e;
        if(decimalPlaces == 0) substrLength -= 1;

        return std::to_string(value).substr(0, substrLength);
    }
    else
        return std::to_string(v).substr(0, 2 + 2) + "E" + std::to_string(e);
}

void startProfiling()
{
    PRINT_DEBUG("\nStart profiling...");
    totalClock.restart();
    intervalOneSum = 0;
    intervalTwoSum = 0;
    intervalOneTicks = 0;
    intervalTwoTicks = 0;
}

void reportProfiling()
{
    long long int totalClockMicroseconds = totalClock.getElapsedTime().asMicroseconds();
    double totalClockWallTimeMs = (double)totalClockMicroseconds / 1000.0;

    double intervalOnePercentage = ((double)intervalOneSum / (double)totalClockMicroseconds) * 100.0;
    double intervalOneWallTimePerCall = (double)intervalOneSum / (double)intervalOneTicks;

    double intervalTwoPercentage = ((double)intervalTwoSum / (double)totalClockMicroseconds) * 100.0;
    double intervalTwoWallTimePerCall = (double)intervalTwoSum / (double)intervalTwoTicks;

    PRINT_DEBUG("\nProfiling time elapsed: " << totalClockWallTimeMs << "ms");
    PRINT_DEBUG("Interval One: " << intervalOnePercentage << "% (" << intervalOneWallTimePerCall  << " microseconds per call)");
    PRINT_DEBUG("Interval Two: " << intervalTwoPercentage << "% (" << intervalTwoWallTimePerCall  << " microseconds per call)");
}

void pf1s()
{
    intervalOneClock.restart();
}

void pf1e()
{
    intervalOneSum += intervalOneClock.getElapsedTime().asMicroseconds();
    intervalOneTicks++;
}

void pf2s()
{
    intervalTwoClock.restart();
}

void pf2e()
{
    intervalTwoSum += intervalTwoClock.getElapsedTime().asMicroseconds();
    intervalTwoTicks++;
}

bool isDebugMode()
{
    #ifdef __OPTIMIZE__
        return false;
    #endif

    return true;
}
