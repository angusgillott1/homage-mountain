#ifndef UTILS_H
#define UTILS_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <SFML/OpenGL.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

#define PI_VALUE 3.141592654

bool writeStringToFile(std::string str, std::string filename);

std::string readStringFromFile(std::string filename);

int regressToMean(int value, int step = 1, int mean = 0);

float regressToMean(float value, float step = 1.0, float mean = 0.0);

int clampHigh(int value, int clamp);

int clampLow(int value, int clamp);

int clampMagnitude(int value, int clamp);

float distance(float x1, float y1, float x2, float y2);

std::string toScientificNotation(float value, int decimalPlaces = 2);

void startProfiling();

void reportProfiling();

void pf1s(); // profile interval one start

void pf1e(); // profile interval one end

void pf2s(); // profile interval two start

void pf2e(); // profile interval two end

bool isDebugMode();

#define PRINT_DEBUG(x) do { if (isDebugMode()) { std::cout << '\n' << x; } } while (0)

#endif // UTILS_H
