#include "weather.h"

//--------
// Local globals
//--------

// Weather

const int MAX_RAIN_PARTICLES = 10000;
RainParticle* allRainParticles[MAX_RAIN_PARTICLES] = {0};
int numberOfRainParticles = 0;

int currentWeather = WEATHER_NO_WEATHER;
float currentWeatherIntensity = 0;
float displayWeatherIntensity = 0;

int lightningStrikeAtTick = 0;

// Lighting

int numberOfLightSources = 0;
const int MAX_NUMBER_LIGHT_SOURCES = 50;

float lightSourcesX[MAX_NUMBER_LIGHT_SOURCES] = {0}; // X position
float lightSourcesY[MAX_NUMBER_LIGHT_SOURCES] = {0};   // Y position
float lightSourcesR[MAX_NUMBER_LIGHT_SOURCES] = {0}; // Red channel
float lightSourcesG[MAX_NUMBER_LIGHT_SOURCES] = {0}; // Green channel
float lightSourcesB[MAX_NUMBER_LIGHT_SOURCES] = {0}; // Blue channel
float lightSourcesM[MAX_NUMBER_LIGHT_SOURCES] = {0};   // The strength of the light, relative to 1.0.
float lightSourcesS[MAX_NUMBER_LIGHT_SOURCES] = {0}; // Sin oscillation offset
unsigned long int lightSourcesPinnedToEntities[MAX_NUMBER_LIGHT_SOURCES] = {0}; // The entity to which the light source is "pinned" (the "source" of the light source)

sf::Shader* shader = 0;
sf::RenderTexture* lightOverlayTexture = 0;

//-------
// Functions
//-------

void initializeWeather()
{
    // Allocate textures and shader

    if (!sf::Shader::isAvailable())
        PRINT_DEBUG("WARNING: Shaders are unavailable.");

    lightOverlayTexture = new sf::RenderTexture();
    if (!lightOverlayTexture->create(SCREEN_WIDTH, SCREEN_HEIGHT))
        PRINT_DEBUG("WARNING: Failed to create darkness overlay texture.");

    shader = new sf::Shader();
    if (!shader->loadFromFile("assets/Shaders/shader.frag", sf::Shader::Fragment))
        PRINT_DEBUG("WARNING: Shader load failed.");
}

RainParticle* createRainParticle(short int x, short int y, short int length, short int xDrift, short int driftFailRoll, short int driftSkip, short int speed, short int lifeDuration, sf::Color color, sf::Color fade)
{
    if(numberOfRainParticles >= MAX_RAIN_PARTICLES)
    {
        //PRINT_DEBUG("Max rain particle limit reached");
        return 0;
    }

    RainParticle* particle = new RainParticle();
    particle->x = x;
    particle->y = y;
    particle->length = length;
    particle->xDrift = xDrift;
    particle->driftFailRoll = driftFailRoll;
    particle->driftSkip = driftSkip;
    particle->speed = speed;
    particle->lifeDuration = lifeDuration;
    particle->color = color;
    particle->fade = fade;

    allRainParticles[numberOfRainParticles] = particle;
    numberOfRainParticles++;

    return particle;
    //PRINT_DEBUG("Number of rain particles: " << numberOfRainParticles);
}

void destroyRainParticle(RainParticle* rainParticle)
{
    int i = 0;

    for(i = 0; i < numberOfRainParticles; i++)
    {
        if(allRainParticles[i] == rainParticle)
            break;
    }

    if(i < numberOfRainParticles)    // means the rainParticle was found
    {
        delete allRainParticles[i];

        for(i++; i < numberOfRainParticles; i++)
        {
            allRainParticles[i - 1] = allRainParticles[i];
        }

        numberOfRainParticles--;
    }
}

void destroyAllRainParticles()
{
    for(int i = 0; i < numberOfRainParticles; i++)
    {
        delete allRainParticles[i];
    }

    numberOfRainParticles = 0;
}

void stepWeather()
{
    displayWeatherIntensity = regressToMean(displayWeatherIntensity, 0.05, currentWeatherIntensity);

    if(currentWeatherIntensity == 0.0 && displayWeatherIntensity == 0.0)
    {
        currentWeather = WEATHER_NO_WEATHER;
        stopAmbient();
    }

    if(currentWeather == WEATHER_RAIN)
    {
        const int dropRate = 2 * displayWeatherIntensity;

        for(int i = 0; i < dropRate; i++)
        {
            createRainParticle(rand() % (getWorldBoundRight() + 100) - 100, rand() % (getWorldBoundBottom() + 100) - 100, 12 + displayWeatherIntensity, -1 - (displayWeatherIntensity / 5), 0, 1, 22, 15, sf::Color(200, 200, 220, 200), sf::Color(0,0,0,5));
        }
    }

    if(currentWeather == WEATHER_STORM)
    {
        if((rand() % 700 == 0 && globals()->worldTick - lightningStrikeAtTick > 300))
        {
            playSound(SOUND_STORM_THUNDER);
            lightningStrikeAtTick = globals()->worldTick;
        }

        const int dropRate = 6 * displayWeatherIntensity;

        for(int i = 0; i < dropRate; i++)
        {
            createRainParticle(rand() % (getWorldBoundRight() + 100) - 100, rand() % (getWorldBoundBottom() + 100) - 100, 18 + displayWeatherIntensity, -1 - (displayWeatherIntensity / 5), 10, 1, 26, 15, sf::Color(200, 200, 220, 200), sf::Color(0,0,0,5));
        }
    }

    if(currentWeather == WEATHER_SNOW)
    {
        const int dropRate = 2 * displayWeatherIntensity;

        for(int i = 0; i < dropRate; i++)
        {
            createRainParticle(rand() % (getWorldBoundRight() + 100) - 100, rand() % (getWorldBoundBottom() + 100) - 100, 6, rand() % 3 - 2, 10, 5 - (displayWeatherIntensity / 3), 1, 45, sf::Color(240, 240, 240, 200), sf::Color(0,0,0,1));
        }
    }

    if(currentWeather == WEATHER_BLIZZARD)
    {
        const int dropRate = 4 * displayWeatherIntensity;

        for(int i = 0; i < dropRate; i++)
        {
            createRainParticle(rand() % (getWorldBoundRight() + 100) - 100, rand() % (getWorldBoundBottom() + 100) - 100, 6, rand() % 7 - 7, 10, 3 - (displayWeatherIntensity / 4), 3, 45, sf::Color(230, 230, 230, 200), sf::Color(0,0,0,1));
        }
    }

    // Manage particles
    for(int i = 0; i < numberOfRainParticles; i++)
    {
        RainParticle* particle = allRainParticles[i];

        if(particle->lifeTick > 0)  // so that on first tick, it is at starting color and position
        {
            particle->y += particle->speed;
            particle->color -= particle->fade;
        }

        if((particle->driftFailRoll == 0 || rand() % particle->driftFailRoll > 0) && globals()->worldTick % particle->driftSkip == 0)
        {
            particle->x += particle->xDrift;
        }
        particle->lifeTick++;

        if(particle->lifeTick > particle->lifeDuration)
            destroyRainParticle(particle);
    }
}

void renderWeather(sf::RenderWindow* window)
{
    if(currentWeather == WEATHER_RAIN)
    {
        // Overcast screen layer
        sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
        rectangle.setPosition(0, 0);
        rectangle.setFillColor(sf::Color(0,0,0, 5 * displayWeatherIntensity));
        window->draw(rectangle);

        // Rain
        sf::Vertex vertices[2];

        for(int i = 0; i < numberOfRainParticles; i++)
        {
            RainParticle* p = allRainParticles[i];

            vertices[0].position = sf::Vector2f(p->x - globals()->viewPortX, p->y - globals()->viewPortY);
            vertices[1].position = sf::Vector2f(p->x + p->xDrift - globals()->viewPortX, p->y + p->length - globals()->viewPortY);
            vertices[0].color  = p->color;
            vertices[1].color = p->color;

            window->draw(vertices, 2, sf::Lines);
        }
    }

    if(currentWeather == WEATHER_STORM)
    {
        int darknessLevel = 8 * displayWeatherIntensity;
        int additionalBrightness = 0;
        int ticksSinceLightning = globals()->worldTick - lightningStrikeAtTick;
        if(ticksSinceLightning * 4 < darknessLevel)
        {
            darknessLevel -= darknessLevel - (ticksSinceLightning * 4);
            additionalBrightness = 20 - ticksSinceLightning;
        }

        // Overcast screen layer
        sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
        rectangle.setPosition(0, 0);
        rectangle.setFillColor(sf::Color(additionalBrightness, additionalBrightness, additionalBrightness, darknessLevel));
        window->draw(rectangle);

        // Rain
        sf::Vertex vertices[2];

        for(int i = 0; i < numberOfRainParticles; i++)
        {
            RainParticle* p = allRainParticles[i];

            vertices[0].position = sf::Vector2f(p->x - globals()->viewPortX, p->y - globals()->viewPortY);
            vertices[1].position = sf::Vector2f(p->x + p->xDrift - globals()->viewPortX, p->y + p->length - globals()->viewPortY);
            vertices[0].color  = p->color;
            vertices[1].color = p->color;

            window->draw(vertices, 2, sf::Lines);
        }
    }

    if(currentWeather == WEATHER_SNOW)
    {
        // Overcast screen layer
        sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
        rectangle.setPosition(0, 0);
        rectangle.setFillColor(sf::Color(255,255,255, 6 * displayWeatherIntensity));
        window->draw(rectangle);

        // Rain
        sf::Vertex vertices[6];

        for(int i = 0; i < numberOfRainParticles; i++)
        {
            RainParticle* p = allRainParticles[i];

            vertices[0].position = sf::Vector2f(p->x - globals()->viewPortX, p->y - globals()->viewPortY);
            vertices[1].position = sf::Vector2f(p->x - globals()->viewPortX, p->y + p->length - globals()->viewPortY);

            vertices[2].position = sf::Vector2f(p->x - (4) - globals()->viewPortX, p->y + (1) - globals()->viewPortY);
            vertices[3].position = sf::Vector2f(p->x + (3) - globals()->viewPortX, p->y + (5) - globals()->viewPortY);

            vertices[4].position = sf::Vector2f(p->x - (4) - globals()->viewPortX, p->y + (5) - globals()->viewPortY);
            vertices[5].position = sf::Vector2f(p->x + (3) - globals()->viewPortX, p->y + (1) - globals()->viewPortY);

            for(int j = 0; j < 6; j++)
                vertices[j].color = p->color;

            window->draw(vertices, 6, sf::Lines);
        }
    }

    if(currentWeather == WEATHER_BLIZZARD)
    {
        // Overcast screen layer
        sf::RectangleShape rectangle(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
        rectangle.setPosition(0, 0);
        rectangle.setFillColor(sf::Color(255,255,255, 10 * displayWeatherIntensity));
        window->draw(rectangle);

        // Rain
        sf::Vertex vertices[6];

        for(int i = 0; i < numberOfRainParticles; i++)
        {
            RainParticle* p = allRainParticles[i];

            vertices[0].position = sf::Vector2f(p->x - globals()->viewPortX, p->y - globals()->viewPortY);
            vertices[1].position = sf::Vector2f(p->x - globals()->viewPortX, p->y + p->length - globals()->viewPortY);

            vertices[2].position = sf::Vector2f(p->x - (4) - globals()->viewPortX, p->y + (1) - globals()->viewPortY);
            vertices[3].position = sf::Vector2f(p->x + (3) - globals()->viewPortX, p->y + (5) - globals()->viewPortY);

            vertices[4].position = sf::Vector2f(p->x - (4) - globals()->viewPortX, p->y + (5) - globals()->viewPortY);
            vertices[5].position = sf::Vector2f(p->x + (3) - globals()->viewPortX, p->y + (1) - globals()->viewPortY);

            for(int j = 0; j < 6; j++)
                vertices[j].color = p->color;

            window->draw(vertices, 6, sf::Lines);
        }
    }
}

void setWeather(int weatherType, int intensity)
{
    if(weatherType == WEATHER_NO_WEATHER)
    {
        PRINT_DEBUG("Drop weather intensity to 0");
        currentWeatherIntensity = 0.0;
    }
    else
    {
        currentWeather = weatherType;
        currentWeatherIntensity = intensity;
    }

    if(currentWeather == WEATHER_RAIN)
    {
        playAmbient("BGS_RAIN");
    }

    if(currentWeather == WEATHER_STORM)
    {
        playAmbient("BGS_STORM");
    }

    if(currentWeather == WEATHER_SNOW)
    {
        stopAmbient();
    }

     if(currentWeather == WEATHER_BLIZZARD)
    {
        playAmbient("BGS_BLIZZARD");
    }
}

sf::RenderTexture* getLightOverlayTexturePointer()
{
    return lightOverlayTexture;
}

void createLightSource(float magnitude, float r, float g, float b, Entity* pinnedToEntity)
{
    int i = numberOfLightSources;

    lightSourcesX[i] = 0; // Set in tick update
    lightSourcesY[i] = 0; // Set in tick update
    lightSourcesR[i] = r;
    lightSourcesG[i] = g;
    lightSourcesB[i] = b;
    lightSourcesM[i] = magnitude;
    lightSourcesS[i] = rand() % 1000;
    lightSourcesPinnedToEntities[i] = pinnedToEntity->guid;

    numberOfLightSources++;
    PRINT_DEBUG("Number of light sources: " << numberOfLightSources);
}

void destroyLightSource(int index)
{
    if (index < 0 || index >= numberOfLightSources)
    {
        PRINT_DEBUG("Invalid index to destroy light source: " << index);
        return;
    }

    // Teleport the last element to the deleted index
    lightSourcesX[index] = lightSourcesX[numberOfLightSources - 1];
    lightSourcesY[index] = lightSourcesY[numberOfLightSources - 1];
    lightSourcesR[index] = lightSourcesR[numberOfLightSources - 1];
    lightSourcesG[index] = lightSourcesG[numberOfLightSources - 1];
    lightSourcesB[index] = lightSourcesB[numberOfLightSources - 1];
    lightSourcesM[index] = lightSourcesM[numberOfLightSources - 1];
    lightSourcesS[index] = lightSourcesS[numberOfLightSources - 1];
    lightSourcesPinnedToEntities[index] = lightSourcesPinnedToEntities[numberOfLightSources - 1];

    // Decrease the number of light sources
    numberOfLightSources--;

    PRINT_DEBUG("Number of light sources after destruction: " << numberOfLightSources);
}

void renderDarkness(sf::RenderWindow* window)
{
    // Step lighting

    for(int i = 0; i < numberOfLightSources; i++)
    {
//        lightSourcesX[i] += lightSourcesXVelocities[i];
//        lightSourcesY[i] += lightSourcesYVelocities[i];

//        if(lightSourcesX[i] > SCREEN_X)
//        {
//            lightSourcesXVelocities[i] = -lightSourcesXVelocities[i];
//            lightSourcesX[i] = SCREEN_X;
//        }
//        if(lightSourcesX[i] < 0)
//        {
//            lightSourcesXVelocities[i] = -lightSourcesXVelocities[i];
//            lightSourcesX[i] = 0;
//        }
//        if(lightSourcesY[i] > SCREEN_Y)
//        {
//            lightSourcesYVelocities[i] = -lightSourcesYVelocities[i];
//            lightSourcesY[i] = SCREEN_Y;
//        }
//        if(lightSourcesY[i] < 0)
//        {
//            lightSourcesYVelocities[i] = -lightSourcesYVelocities[i];
//            lightSourcesY[i] = 0;
//        }

        Entity* pinnedEntity = getEntityByGuid(lightSourcesPinnedToEntities[i]);
        if(pinnedEntity == 0)
        {
            destroyLightSource(i);  // pinned entity is gone
        }
        else
        {
            lightSourcesX[i] = centerX(pinnedEntity);
            lightSourcesY[i] = centerY(pinnedEntity);
            lightSourcesS[i]++;

            if(pinnedEntity == globals()->player)
            {
                lightSourcesY[i] -= 3;
            }
        }
    }

    // Render

    lightOverlayTexture->setActive();
    lightOverlayTexture->clear(sf::Color(0, 0, 0, 0));

    shader->setUniform("numberOfLightSources", numberOfLightSources);
    shader->setUniform("screenHeight", SCREEN_HEIGHT);
    shader->setUniformArray("lightSourcesX", lightSourcesX, numberOfLightSources);
    shader->setUniformArray("lightSourcesY", lightSourcesY, numberOfLightSources);
    shader->setUniformArray("lightSourcesR", lightSourcesR, numberOfLightSources);
    shader->setUniformArray("lightSourcesG", lightSourcesG, numberOfLightSources);
    shader->setUniformArray("lightSourcesB", lightSourcesB, numberOfLightSources);
    shader->setUniformArray("lightSourcesM", lightSourcesM, numberOfLightSources);
    shader->setUniformArray("lightSourcesS", lightSourcesS, numberOfLightSources);

    {
        sf::Texture texture;
        texture.create(SCREEN_WIDTH, SCREEN_HEIGHT);
        sf::Sprite sprite(texture);

        lightOverlayTexture->draw(sprite, shader);
        lightOverlayTexture->display();
    }

    {
        sf::RenderStates states;
        states.blendMode = sf::BlendAdd;
        window->draw(sf::Sprite(lightOverlayTexture->getTexture()), states);
    }
}
