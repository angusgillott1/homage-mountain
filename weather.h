#ifndef WEATHER_H
#define WEATHER_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <math.h>
#include <cstring> // For memmove

#include "globals.h"
#include "stats.h"
#include "assets.h"
#include "utils.h"
#include "enums.h"

struct Entity; // Forward declaration for entity.h

struct RainParticle
{
    short int x = 0;
    short int y = 0;
    short int length = 0;
    short int xDrift = 0;
    short int driftFailRoll = 0;
    short int driftSkip = 1; // Effective drift speed is divided by this, by moving only on modulus of number. Allows speeds slower than 60px/s
    short int speed = 0;

    short int lifeTick = 0;
    short int lifeDuration = 0;

    sf::Color color = sf::Color::Transparent;
    sf::Color fade = sf::Color::Transparent;
};

void initializeWeather();

RainParticle* createRainParticle(short int x, short int y, short int length, short int xDrift, short int driftFailRoll, short int driftSkip, short int speed, short int lifeDuration, sf::Color color, sf::Color fade);
void destroyRainParticle(RainParticle* rainParticle);
void destroyAllRainParticles();

void stepWeather();
void renderWeather(sf::RenderWindow* window);
void setWeather(int weatherType, int intensity = 5);
sf::RenderTexture* getLightOverlayTexturePointer();

void createLightSource(float magnitude, float r, float g, float b, Entity* pinnedToEntity);
void destroyLightSource(int index);
void renderDarkness(sf::RenderWindow* window);


#endif // WEATHER_H

